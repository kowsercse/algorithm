package com.kowsercse.p1257;

import java.util.Arrays;
import java.util.stream.Stream;

class Solution {
  int N;
  int[] target;
  Integer[] tree;
  private int treeSize;

  public int minNumberOperations(int[] target) {
    this.N = target.length;
    this.target = target;
    buildSegmentTree();

    return countIncrement(0, N - 1, 0);
  }

  int countIncrement(int start, int end, int base) {
    if (start > end || start < 0 || end >= N) return 0;

    int minIndex = rmq(start, end);

    return target[minIndex] - base
        + countIncrement(start, minIndex - 1, target[minIndex])
        + countIncrement(minIndex + 1, end, target[minIndex]);
  }

  int rmq(int start, int end) {
    return rmq(1, 0, treeSize / 2 - 1, start, end);
  }

  Integer rmq(int index, int left, int right, int start, int end) {
    int mid = (left + right) / 2;

    if (start <= left && end >= right) {
      return tree[index];
    }
    if (start > right || end < left) {
      return null;
    }

    var leftMin = rmq(index * 2, left, mid, start, end);
    var rightMin = rmq(index * 2 + 1, mid + 1, right, start, end);

    if (leftMin != null && rightMin != null) {
      return target[leftMin] < target[rightMin] ? leftMin : rightMin;
    }
    return leftMin != null ? leftMin : rightMin;
  }

  void buildSegmentTree() {
    treeSize = calculateTreeSize();
    tree = new Integer[treeSize];
    for (int i = 0; i < N; i++) {
      tree[treeSize / 2 + i] = i;
    }

    buildTree(1, 0, treeSize / 2 - 1);

    System.out.println(Arrays.toString(tree));
    System.out.println(Stream.of(tree).map(i -> i == null ? "null" : target[i]).toList());
  }

  private void buildTree(int index, int left, int right) {
    if (index >= treeSize / 2 || left > right || left < 0 || right >= treeSize / 2) return;

    var mid = (left + right) / 2;
    buildTree(index * 2, left, mid);
    buildTree(index * 2 + 1, mid + 1, right);

    var leftTargetIndex = tree[index * 2];
    var rightTargetIndex = tree[index * 2 + 1];
    if (leftTargetIndex != null && rightTargetIndex != null) {
      tree[index] = target[leftTargetIndex] < target[rightTargetIndex] ? leftTargetIndex : rightTargetIndex;
    } else {
      tree[index] = leftTargetIndex == null ? rightTargetIndex : leftTargetIndex;
    }
  }

  private int calculateTreeSize() {
    int n = N;
    int treeSize = 1;
    while (n != 0) {
      n >>= 1;
      treeSize <<= 1;
    }
    if (treeSize != N * 2) {
      treeSize <<= 1;
    }
    return treeSize;
  }
}
