package com.kowsercse.p1257;

import java.util.Arrays;
import java.util.stream.Stream;

public class Solution2 {
  int N;
  int[] target;
  Integer[] tree;

  public int minNumberOperations(int[] target) {
    this.N = target.length;
    this.target = target;
    buildSegmentTree();

    System.out.println(rmq(5, 7));
//    return 0;
    return countIncrement(0, N - 1, 0);
  }

  int countIncrement(int start, int end, int base) {
    if (start > end || start < 0 || end >= N) return 0;

    int minIndex = rmq(start, end);

    return target[minIndex] - base
        + countIncrement(start, minIndex - 1, target[minIndex])
        + countIncrement(minIndex + 1, end, target[minIndex]);
  }

  Integer rmq(int start, int end) {
    if (start > end || start < 0 || end >= N) return null;

    var from = start + N;
    var to = end + N + 1;

    Integer minIndex = null;
    while (from < to) {
      if (from % 2 == 1) {
        minIndex = minIndex(minIndex, tree[from]);
        from++;
      }
      if (to % 2 == 1) {
        to--;
        minIndex = minIndex(minIndex, tree[to]);
      }

      from /= 2;
      to /= 2;
    }

    return minIndex;
  }

  private Integer minIndex(Integer left, Integer right) {
    if (left != null && right != null) {
      return target[left] < target[right] ? left : right;
    }
    return left != null ? left : right;
  }

  void buildSegmentTree() {
    tree = new Integer[2 * N];
    for (int i = 0; i < N; i++) {
      tree[N + i] = i;
    }

    for (int i = N - 1; i > 0; i--) {
      var leftIndex = tree[2 * i];
      var rightIndex = tree[2 * i + 1];
      if (leftIndex != null && rightIndex != null) {
        tree[i] = target[leftIndex] < target[rightIndex] ? leftIndex : rightIndex;
      } else {
        tree[i] = leftIndex != null ? leftIndex : rightIndex;
      }
    }

    System.out.println(Arrays.toString(tree));
    System.out.println(Stream.of(tree).map(i -> i == null ? "null" : target[i]).toList());
  }
}