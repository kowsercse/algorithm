package com.kowsercse.p68;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

class Solution {
  String emptyString;

  public List<String> fullJustify(String[] words, int maxWidth) {
    initializeString(maxWidth);

    var lineLength = words[0].length();
    var selectedWords = new LinkedList<>(List.of(words[0]));
    var result = new LinkedList<String>();

    for (var i = 1; i < words.length; i++) {
      var word = words[i];
      if (lineLength + 1 + word.length() <= maxWidth) {
        lineLength += 1 + word.length();
        selectedWords.add(word);
      } else {
        result.add(toLine(selectedWords, maxWidth));
        lineLength = word.length();
        selectedWords = new LinkedList<>(List.of(word));
      }
    }

    if (!selectedWords.isEmpty()) {
      String line = String.join(" ", selectedWords);
      result.add(line);
    }

    return result;
  }

  private String toLine(LinkedList<String> selectedWords, int maxWidth) {
    if (selectedWords.size() == 1) {
      return selectedWords.get(0);
    }

    int totalLength = selectedWords.stream().mapToInt(String::length).sum();
    int availableSpace = maxWidth - totalLength;
    int defaultSpace = availableSpace / (selectedWords.size() - 1);
    int extraSpace = availableSpace % (selectedWords.size() - 1);

    var builder = new StringBuilder(selectedWords.remove());
    while (!selectedWords.isEmpty()) {
      if (extraSpace > 0) {
        builder.append(emptyString, 0, 1);
        extraSpace--;
      }
      builder.append(emptyString, 0, defaultSpace);

      var selectedWord = selectedWords.removeFirst();
      builder.append(selectedWord);
    }

    return builder.toString();
  }

  private void initializeString(int maxWidth) {
    "".toCharArray();
    HashMap<String, List<String>> map = new HashMap<>();
    map.putIfAbsent(" ", new ArrayList<>());
    char[] chars = new char[maxWidth];
    Arrays.fill(chars, ' ');
    emptyString = new String(chars);
  }

  public static void main(String[] args) {
    System.out.println(String.join(" ", List.of("a", "b", "c")));
    Solution solution = new Solution();
    for (String line : solution.fullJustify(new String[]{"this", "is", "a", "test"}, 8)) {
      System.out.println(line);
    }
    for (String line : solution.fullJustify(new String[]{"Science", "is", "what", "we", "understand", "well", "enough", "to", "explain", "to", "a", "computer.", "Art", "is", "everything", "else", "we", "do"}, 20)) {
      System.out.println(line);
    }
  }
}

/*
"Science  is  what we",
"understand      well",
"enough to explain to",
"a  computer.  Art is",
"everything  else  we",
"do                  "
 */