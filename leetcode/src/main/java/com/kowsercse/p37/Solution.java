package com.kowsercse.p37;

public class Solution {
  boolean[][] rowBit;
  boolean[][] colBit;
  boolean[][][] boardBit = new boolean[3][3][];
  char[][] board;

  public void solveSudoku(char[][] board) {
    initialize(board);
    solve(0, 0);
  }

  private void initialize(char[][] board) {
    this.rowBit = new boolean[9][10];
    this.colBit = new boolean[9][10];
    this.boardBit = new boolean[3][3][10];
    this.board = board;
    for (int row = 0; row < 9; row++) {
      for (int col = 0; col < 9; col++) {
        if (board[row][col] == '.') {
          continue;
        }
        int digit = board[row][col] - '0';
        rowBit[row][digit] = true;
        colBit[col][digit] = true;
        boardBit[row / 3][col / 3][digit] = true;
      }
    }
  }

  void solve(int row, int col) {
    if (row == 8 && col == 8) {
      return;
    }
    for (int digit = 1; digit < 10; digit++) {
      if (rowBit[row][digit]) {
        continue;
      }
      if (colBit[col][digit] || board[row][col] != '.' || boardBit[row / 3][col / 3][digit]) {
        continue;
      }

      rowBit[row][digit] = true;
      colBit[col][digit] = true;
      boardBit[row / 3][col / 3][digit] = true;

      var temp = board[row][col];
      board[row][col] = (char) ('0' + digit);


      if (col == 8) {
        solve(row + 1, 0);
      } else {
        solve(row, col + 1);
      }

      board[row][col] = temp;

      boardBit[row / 3][col / 3][digit] = false;
      colBit[col][digit] = false;
      rowBit[row][digit] = false;
    }
  }
}
