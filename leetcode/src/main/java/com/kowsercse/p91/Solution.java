package com.kowsercse.p91;

class Solution {
  public int numDecodings(String s) {
    if (s.charAt(0) == '0') return 0;
    var dp = new int[s.length() + 1];
    dp[0] = 1;
    dp[1] = isValid(s.charAt(0)) ? 1 : 0;

    for (int i = 2; i <= s.length(); i++) {
      dp[i] =
          (isValid(s.charAt(i - 1)) ? dp[i - 1] : 0)
              + (isValid(s.charAt(i - 2), s.charAt(i - 1)) ? dp[i - 2] : 0);
    }
    return dp[s.length()];
  }

  boolean isValid(char c) {
    return c >= '1' && c <= '9';
  }

  boolean isValid(char c1, char c2) {
    if (c1 == '1') {
      return true;
    } else if (c1 == '2') {
      return c2 >= '0' && c2 <= '6';
    }

    return false;
  }
}