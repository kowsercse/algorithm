package com.kowsercse.p227;

import java.util.Map;
import java.util.Stack;
import java.util.function.BiFunction;

class Solution {
  Map<String, Integer> map = Map.ofEntries(
      Map.entry("apple", 1),
      Map.entry("banana", 2),
      Map.entry("cherry", 3),
      Map.entry("date", 4)
  );

  Map<Character, BiFunction<Integer, Integer, Integer>> OPERATORS = Map.ofEntries(
      Map.entry('+', Integer::sum),
      Map.entry('-', (left, right) -> left - right),
      Map.entry('*', (left, right) -> left * right),
      Map.entry('/', (left, right) -> left / right)
  );

}