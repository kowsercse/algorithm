package com.kowsercse.p95;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Definition for a binary tree node.
 * <pre> {@code
 * class TreeNode {
 *   int val;
 *   TreeNode left;
 *   TreeNode right;
 *
 *   TreeNode(int val) {
 *     this(val, null, null);
 *   }
 *   TreeNode(int val, TreeNode left, TreeNode right) {
 *     this.val = val;
 *     this.left = left;
 *     this.right = right;
 *   }
 * }</pre>
 */
class Solution {

  Map<Pair<Integer, Integer>, List<TreeNode>> map = new HashMap<>();

  public List<TreeNode> generateTrees(int n) {
    return generateTrees(1, n);
  }

  List<TreeNode> generateTrees(int l, int r) {
    if (l > r) {
      return List.of();
    }
    var pair = new Pair<>(l, r);
    if (map.containsKey(pair)) {
      return map.get(pair);
    }
    if (l == r) {
      map.put(pair, List.of(new TreeNode(l)));
      return map.get(pair);
    }

    var list = new LinkedList<TreeNode>();
    for (int i = l; i <= r; i++) {
      var leftSubTrees = generateTrees(l, i - 1);
      var rightSubTrees = generateTrees(i + 1, r);
      if (leftSubTrees.isEmpty()) {
        for (var right : rightSubTrees) {
          list.add(new TreeNode(i, null, right));
        }
      }
      if (rightSubTrees.isEmpty()) {
        for (var left : leftSubTrees) {
          list.add(new TreeNode(i, left, null));
        }
      }
      for (var left : leftSubTrees) {
        for (var right : rightSubTrees) {
          list.add(new TreeNode(i, left, right));
        }
      }
    }

    map.put(pair, list);
    return list;
  }
}

class TreeNode {
  int val;
  TreeNode left;
  TreeNode right;

  TreeNode(int val) {
    this(val, null, null);
  }

  TreeNode(int val, TreeNode left, TreeNode right) {
    this.val = val;
    this.left = left;
    this.right = right;
  }
}

class Pair<Left, Right> {

  Left left;
  Right right;

  public Pair(Left left, Right right) {
    this.left = left;
    this.right = right;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Pair<?, ?> pair = (Pair<?, ?>) o;
    return Objects.equals(left, pair.left) && Objects.equals(right, pair.right);
  }

  @Override
  public int hashCode() {
    return Objects.hash(left, right);
  }
}