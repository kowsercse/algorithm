package com.kowsercse.p400;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.StringJoiner;
import java.util.TreeMap;

class LFUCache {
  private final int capacity;
  Map<Integer, Entry> cache = new HashMap<>();
  TreeMap<Integer, LinkedHashSet<Integer>> keysByFrequency = new TreeMap<>();

  public LFUCache(int capacity) {
    this.capacity = capacity;
  }

  public int get(int key) {
    System.out.printf("cache.get(%d);\n", key);
    Entry entry = cache.get(key);
    if (entry == null) {
      return -1;
    }

    LinkedHashSet<Integer> keys = keysByFrequency.get(entry.getCount());
    if (keys != null) {
      keys.remove(key);
      if (keys.isEmpty()) {
        keysByFrequency.remove(entry.getCount());
      }
    }

    entry.incCount();
    keysByFrequency.computeIfAbsent(entry.getCount(), k -> new LinkedHashSet<>()).addLast(key);

    return entry.getValue();
  }

  public void put(int key, int value) {
    System.out.printf("cache.put(%d, %d);\n", key, value);
    Entry entry = cache.get(key);

    if (entry == null) {
      if (cache.size() == capacity) {
        var frequencyEntry = keysByFrequency.firstEntry();
        int keyToRemove = frequencyEntry.getValue().removeFirst();
        cache.remove(keyToRemove);
        if (frequencyEntry.getValue().isEmpty()) {
          keysByFrequency.remove(frequencyEntry.getKey());
        }
      }

      entry = new Entry();
      entry.incCount();
      entry.setValue(value);

      cache.put(key, entry);
      keysByFrequency.computeIfAbsent(entry.getCount(), k -> new LinkedHashSet<>()).addLast(key);
    } else {
      entry.setValue(value);

      LinkedHashSet<Integer> keys = keysByFrequency.get(entry.getCount());
      if (keys != null) {
        keys.remove(key);
        if (keys.isEmpty()) {
          keysByFrequency.remove(entry.getCount());
        }
      }
      entry.incCount();
      keysByFrequency.computeIfAbsent(entry.getCount(), k -> new LinkedHashSet<>()).addLast(key);
    }
  }

  void print() {
    System.out.println(cache);
    System.out.println(keysByFrequency);
    System.out.println("==========");
  }

  static class Entry {
    private int value;
    private int count = 0;

    public void setValue(int value) {
      this.value = value;
    }

    int getValue() {
      return value;
    }

    public void incCount() {
      count++;
    }

    public int getCount() {
      return count;
    }

    @Override
    public String toString() {
      return new StringJoiner(", ", "[", "]")
          .add("value=" + value)
          .add("count=" + count)
          .toString();
    }
  }
}

/*
- 0
  - 1
    - 3
    - 4
  - 2
    - 5
    - 6

 */