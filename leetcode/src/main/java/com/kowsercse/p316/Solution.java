package com.kowsercse.p316;

import java.util.Stack;

class Solution {
  public String removeDuplicateLetters(String s) {
    var in = s.toCharArray();
    var seen = new boolean[256];
    var lastIndex = new Integer[256];
    var stack = new Stack<Character>();

    for (int i = 0; i < in.length; i++) lastIndex[in[i]] = i;
    for (int i = 0; i < in.length; i++) {
      var c = in[i];
      while (!seen[c] && !stack.isEmpty() && c < stack.peek() && lastIndex[stack.peek()] > i) {
        seen[stack.pop()] = false;
      }
      if (!seen[c]) {
        seen[c] = true;
        stack.push(c);
      }
    }

    var result = new StringBuilder();
    for (char c : stack) result.append(c);
    return result.toString();
  }
}
