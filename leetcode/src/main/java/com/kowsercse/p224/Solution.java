package com.kowsercse.p224;

import java.util.List;
import java.util.Optional;
import java.util.Stack;
import java.util.stream.Collectors;

class Solution {

  int i;
  String s;

  public int calculate(String s) {
    i = 0;
    this.s = s;
    return calculate();
  }

  int calculate() {
    int result = 0;
    int operator = 1;

    while (i < s.length()) {
      char c = s.charAt(i);

      if (c == '(') {
        i++;
        result += calculate() * operator;
      } else if (c == ')') {
        i++;
      } else if (c == '+') {
        i++;
        operator = 1;
      } else if (c == '-') {
        i++;
        operator = -1;
      } else if (Character.isDigit(c)) {
        int number = 0;
        while (i < s.length() && Character.isDigit(s.charAt(i))) {
          number = number * 10 + s.charAt(i) - '0';
          i++;
        }
        result = result + number * operator;
      } else {
        i++;
      }
    }

    return result;
  }
}
