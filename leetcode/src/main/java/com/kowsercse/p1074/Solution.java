package com.kowsercse.p1074;

import java.util.Arrays;

class Solution {

  public int numSubmatrixSumTarget(int[][] matrix, int target) {
    var count = 0;
    var R = matrix.length;
    var C = matrix[0].length;
    var ps1d /* prefixSum1D */ = new int[R + 1][C + 1];
    var ps2d /* prefixSum2D */ = new int[R + 1][C + 1];

    for (int r = 1; r <= R; r++) {
      for (int c = 1; c <= C; c++) {
        ps1d[r][c] = ps1d[r][c - 1] + matrix[r - 1][c - 1];
      }
    }

    for (int c = 1; c <= C; c++) {
      for (int r = 1; r <= R; r++) {
        ps2d[r][c] = ps2d[r - 1][c] + ps1d[r][c];
      }
    }

    printArray(matrix);
    printArray(ps1d);
    print();
    printArray(ps2d);

    for (int rStart = 1; rStart <= R; rStart++) {
      for (int rEnd = rStart; rEnd <= R; rEnd++) {
        for (int cStart = 1; cStart <= C; cStart++) {
          for (int cEnd = cStart; cEnd <= C; cEnd++) {
            var sum =
                ps2d[rEnd][cEnd] - ps2d[rStart - 1][cEnd] - ps2d[rEnd][cStart - 1] + ps2d[rStart - 1][cStart - 1];
            if (sum == target) {
              count++;
            }
          }
        }
      }
    }

    return count;
  }

  void printArray(int[][] array) {
    for (int[] entry : array) {
      print(entry);
    }
  }

  void print(int[] args) {
    System.out.println(Arrays.toString(args));
  }

  void print(Object... args) {
    System.out.println(Arrays.toString(args));
  }
}