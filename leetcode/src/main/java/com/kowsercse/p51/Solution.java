package com.kowsercse.p51;


import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.IntStream;

class Solution {
  char[][] board;
  int size;
  List<List<String>> results = new LinkedList<>();
  Set<Integer> cols = new HashSet<>();
  Set<Integer> diagonals = new HashSet<>();
  Set<Integer> antiDiagonals = new HashSet<>();

  public List<List<String>> solveNQueens(int n) {
    this.size = n;
    board = new char[n][n];
    for (int r = 0; r < size; r++) {
      Arrays.fill(board[r], '.');
    }

    backtrack(0);

    return results;
  }

  void backtrack(int row) {
    if (row == size) {
      results.add(toResult());
      return;
    }

    for (var c = 0; c < size; c++) {
      if (cols.contains(c)) continue;
      if (diagonals.contains(row + c)) continue;
      if (antiDiagonals.contains(row - c)) continue;

      cols.add(c);
      diagonals.add(row + c);
      antiDiagonals.add(row - c);
      board[row][c] = 'Q';

      backtrack(row + 1);

      board[row][c] = '.';
      cols.remove(c);
      diagonals.remove(row + c);
      antiDiagonals.remove(row - c);
    }
  }

  List<String> toResult() {
    var list = new LinkedList<String>();
    for (int r = 0; r < size; r++) list.add(new String(board[r]));
    return list;
  }
}
