package com.kowsercse.p212;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

class Solution {
  int[][] dirs = new int[][]{{0, 1}, {0, -1}, {1, 0}, {-1, 0}};
  int R, C;
  char[][] board;
  Trie trie;
  List<String> result;

  public List<String> findWords(char[][] board, String[] words) {
    this.trie = Trie.of(words);
    this.board = board;
    this.R = board.length;
    this.C = board[0].length;
    this.result = new LinkedList<>();

    for (int r = 0; r < R; r++) {
      for (int c = 0; c < C; c++) {
        backtrack(r, c);
      }
    }

    return result;
  }

  void backtrack(int r, int c) {
    backtrack(trie.root.children.get(board[r][c]), r, c, new boolean[R][C]);
  }

  void backtrack(TrieNode node, int r, int c, boolean[][] visited) {
    if (node == null) return;
    if (visited[r][c]) return;

    visited[r][c] = true;

    if (node.word != null) {
      result.add(node.word);
    }

    for (var dir : dirs) {
      int nextR = r + dir[0], nextC = c + dir[1];
      if (nextR < 0 || nextR == R || nextC < 0 || nextC == C) continue;
      var child = node.children.get(board[nextR][nextC]);
      backtrack(child, nextR, nextC, visited);
    }

    visited[r][c] = false;
  }
}

class Trie {

  TrieNode root = new TrieNode();

  void insert(String word) {
    var node = root;
    for (char c : word.toCharArray()) {
      node = node.children.computeIfAbsent(c, k -> new TrieNode());
      node.c = c;
    }
    node.word = word;
  }

  static Trie of(String[] words) {
    var trie = new Trie();
    Arrays.stream(words).forEach(trie::insert);
    return trie;
  }
}

class TrieNode {
  Map<Character, TrieNode> children = new HashMap<>();
  char c;
  String word;

  public String toString() {
    return "" + c;
  }
}
