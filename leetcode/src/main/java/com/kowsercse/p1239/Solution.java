package com.kowsercse.p1239;

import java.util.List;
import java.util.Stack;

class Solution {

  int max = 0;
  int length;
  String[] in;
  Boolean[][] overlap;

  public int maxLength(List<String> arr) {
    in = arr.stream().filter(entry -> !this.hasDuplicate(entry)).toArray(String[]::new);
    length = in.length;
    overlap = new Boolean[length][length];

    backtrack(new Stack<Integer>(), 0, 0);

    return max;
  }

  void backtrack(Stack<Integer> stack, int runningLength, int index) {
    if (index >= length) {
      return;
    }

    backtrack(stack, runningLength, index + 1);
    if (hasOverlap(stack, index)) {
      return;
    }
    var currentLength = runningLength + in[index].length();
    max = Math.max(max, currentLength);
    stack.push(index);
    backtrack(stack, currentLength, index + 1);
    stack.pop();
  }

  private boolean hasOverlap(Stack<Integer> stack, int index) {
    for (var previous : stack) {
      if (hasOverlap(previous, index)) {
        return true;
      }
    }
    return false;
  }

  boolean hasOverlap(int left, int right) {
    if (overlap[left][right] != null) {
      return overlap[left][right];
    }

    var foundChars = new boolean[256];
    for (var c : in[left].toCharArray()) {
      foundChars[c] = true;
    }
    for (var c : in[right].toCharArray()) {
      if (foundChars[c]) {
        return overlap[left][right] = overlap[right][left] = true;
      }
    }

    return false;
  }

  boolean hasDuplicate(String in) {
    var foundChars = new boolean[256];
    for (var c : in.toCharArray()) {
      if (foundChars[c]) {
        return true;
      }
      foundChars[c] = true;
    }

    return false;
  }
}