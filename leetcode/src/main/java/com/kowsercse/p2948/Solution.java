package com.kowsercse.p2948;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.TreeSet;
import java.util.stream.IntStream;

class Solution {

  public int[] lexicographicallySmallestArray(int[] nums, int limit) {
    int[] numsSorted = new int[nums.length];
    for (int i = 0; i < nums.length; i++) numsSorted[i] = nums[i];
    Arrays.sort(numsSorted);

    int currGroup = 0;
    HashMap<Integer, Integer> numToGroup = new HashMap<>();
    numToGroup.put(numsSorted[0], currGroup);

    HashMap<Integer, LinkedList<Integer>> groupToList = new HashMap<>();
    groupToList.put(
        currGroup,
        new LinkedList<Integer>(Arrays.asList(numsSorted[0]))
    );

    for (int i = 1; i < nums.length; i++) {
      if (Math.abs(numsSorted[i] - numsSorted[i - 1]) > limit) {
        // new group
        currGroup++;
      }

      // assign current element to group
      numToGroup.put(numsSorted[i], currGroup);

      // add element to sorted group list
      if (!groupToList.containsKey(currGroup)) {
        groupToList.put(currGroup, new LinkedList<Integer>());
      }
      groupToList.get(currGroup).add(numsSorted[i]);
    }

    // iterate through input and overwrite each element with the next element in its corresponding group
    for (int i = 0; i < nums.length; i++) {
      int num = nums[i];
      int group = numToGroup.get(num);
      nums[i] = groupToList.get(group).pop();
    }

    return nums;
  }
}

class Solution2 {

  public int[] lexicographicallySmallestArray(int[] nums, int limit) {
    var n = nums.length;
    var set = new TreeSet<Integer>((left, right) -> nums[left] == nums[right] ? left - right : nums[left] - nums[right]);
    IntStream.range(0, n).forEach(set::add);

    for (var i = 0; i < n; i++) {
      set.remove(i);
      var I = set.floor(i);

      while (I != null && nums[i] > nums[I] && nums[i] - nums[I] <= limit) {
        set.remove(I);

        int temp = nums[i];
        nums[i] = nums[I];
        nums[I] = temp;

        set.add(I);
        I = set.floor(i);
      }
    }

    return nums;
  }
}