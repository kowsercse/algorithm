package com.kowsercse.p1257;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SolutionTest {

  @Test
  public void test() {
    Solution solution = new Solution();
    System.out.println(solution.minNumberOperations(new int[]{1, 1, 1, 1, 2, 3, 2, 1}));
    System.out.println(solution.minNumberOperations(new int[]{1, 2, 3, 4, 5}));
  }
}