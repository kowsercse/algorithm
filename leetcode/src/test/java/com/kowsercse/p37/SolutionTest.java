package com.kowsercse.p37;

import com.google.gson.Gson;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Array;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class SolutionTest {
  Gson gson = new Gson();
  String args = """
      [["5","3",".",".","7",".",".",".","."],["6",".",".","1","9","5",".",".","."],[".","9","8",".",".",".",".","6","."],["8",".",".",".","6",".",".",".","3"],["4",".",".","8",".","3",".",".","1"],["7",".",".",".","2",".",".",".","6"],[".","6",".",".",".",".","2","8","."],[".",".",".","4","1","9",".",".","5"],[".",".",".",".","8",".",".","7","9"]]
      """;
  char[][] board = gson.fromJson(args, char[][].class);

  @Test
  void test() {
    for (char[] chars : board) {
      System.out.println(Arrays.toString(chars));
    }
    System.out.println("===");
    Solution solution = new Solution();
    solution.solveSudoku(board);
    for (char[] chars : board) {
      System.out.println(Arrays.toString(chars));
    }
  }
}