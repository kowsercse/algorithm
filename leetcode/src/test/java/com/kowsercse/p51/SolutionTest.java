package com.kowsercse.p51;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;
import org.junit.jupiter.api.Test;

class SolutionTest {

  @Test
  public void test() {
    Solution solution = new Solution();
    for (List<String> solveNQueen : solution.solveNQueens(4)) {
      System.out.println();
      solveNQueen.forEach(System.out::println);
      System.out.println();
    }
  }
}