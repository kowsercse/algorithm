package com.kowsercse.p316;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.function.Function;
import org.junit.jupiter.api.Test;

public class SolutionTest {

  Solution solution = new Solution();

  @Test
  public void testX() {
    assertEquals("acb", solution.removeDuplicateLetters("ccacbb"));
    assertEquals("acdb", solution.removeDuplicateLetters("cbacdcbc"));
  }

  @Test
  public void x() {
    var queue = new PriorityQueue<>(
        Comparator.comparing(
            (Function<int[], Integer>) entry -> entry[0]).thenComparing(entry -> entry[0]));
  }
}
