package com.kowsercse.p2948;

import com.google.gson.Gson;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;

class SolutionTest {
  Solution solution = new Solution();
  Gson gson = new Gson();

  @Test
  void test() throws URISyntaxException, IOException {
    URI uri = getClass().getResource("/p2948.txt").toURI();
    var lines = Files.readAllLines(Path.of(uri));
    int[] nums = gson.fromJson(lines.get(0), int[].class);
    System.out.println(nums.length);
    int limit = gson.fromJson(lines.get(1), int.class);
    var result = solution.lexicographicallySmallestArray(nums, limit);
  }
}