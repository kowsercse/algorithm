//package com.kowsercse.p576;
//
//class Solution {
//
//  public int findPaths(int m, int n, int maxMove, int startRow, int startColumn) {
//    var dp = new int[m + 2][n + 2];
//    var C = 1000000000 + 7;
//
//    for (int move = 1; move <= maxMove; move++) {
//      var current = new int[m + 2][n + 2];
//      for (int r = 1; r <= m; r++) {
//        for (int c = 1; c <= n; c++) {
//          current[r][c] = dp[r - 1][c];
//        }
//        current[r][c] = (current[r][c] + dp[r + 1][c]) % C;
//        current[r][c] = (current[r][c] + dp[r][c - 1]) % C;
//        current[r][c] = (current[r][c] + dp[r][c + 1]) % C;
//      }
//      dp = current;
//    }
//
//    return dp[startRow + 1][startColumn + 1];
//  }
//}