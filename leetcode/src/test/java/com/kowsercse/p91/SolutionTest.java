package com.kowsercse.p91;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class SolutionTest {

  private final Solution solution = new Solution();

  @Test
  public void test() {
    assertEquals(1, solution.numDecodings("102"));
  }
}