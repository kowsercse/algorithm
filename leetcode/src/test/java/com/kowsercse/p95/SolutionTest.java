package com.kowsercse.p95;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;
import org.junit.jupiter.api.Test;

class SolutionTest {

  Solution solution = new Solution();

  @Test
  public void test() {
    List<TreeNode> treeNodes = solution.generateTrees(3);
    System.out.println(treeNodes.size());
  }

}