package com.kowsercse.p212;

import com.google.gson.Gson;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class SolutionTest {
  Gson gson = new Gson();
  Solution solution = new Solution();

  @Test
  void test() throws IOException, URISyntaxException {
    URI uri = getClass().getResource("/212.txt").toURI();
    var lines = Files.readAllLines(Path.of(uri));
    List<String> words = solution.findWords(gson.fromJson(lines.get(0), char[][].class), gson.fromJson(lines.get(1), String[].class));
    System.out.println(words);
  }

}