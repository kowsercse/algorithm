package com.kowsercse.p1239;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;
import org.junit.jupiter.api.Test;

class SolutionTest {

  private final Solution solution = new Solution();

  @Test
  public void test() {
    int actual = solution.maxLength(Arrays.asList("abcdefghijklm", "opqrstuvwxyza", "nopqrstuvwxyz"));
    assertEquals(26, actual);
  }

  @Test
  public void testAnother() {
    System.out.println(1 << ('a' - 96));
    System.out.println(1 << ('b' - 96));
    System.out.println(1 << ('c' - 96));
  }
}