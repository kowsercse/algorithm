package com.kowsercse.util;

import static java.util.stream.Collectors.toList;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.io.FileUtils;
import org.apache.http.HttpHeaders;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

public class SkeletonBuilder implements Closeable {

  public static final String RESOURCE_ROOT =
      "/world/code/algorithm/online-judge/src/test/resources/";
  private static final Pattern INPUT_ID_PATTERN = Pattern.compile("data-id=\"(?<inputId>[\\d]*)\"");

  private final CloseableHttpClient httpClient = HttpClients.createDefault();
  private final Gson gson = new Gson();

  public void build(Class<?> clazz) {
    final String canonicalName = clazz.getCanonicalName().replace('.', '/');
    final String[] nameAndPackages = canonicalName.split("/");
    final File writeDirectory =
        new File(RESOURCE_ROOT, "/" + canonicalName.substring(0, canonicalName.lastIndexOf('/')));
    writeDirectory.mkdirs();

    try {
      final String problemId = nameAndPackages[nameAndPackages.length - 1].substring(1);
      FileUtils.writeStringToFile(
          new File(writeDirectory, problemId + ".in"), "", Charset.defaultCharset());
      FileUtils.writeStringToFile(
          new File(writeDirectory, problemId + ".out"), "", Charset.defaultCharset());

      buildInternal(writeDirectory, nameAndPackages[2], problemId);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private void buildInternal(File writeDirectory, String judgeId, String problemId)
      throws IOException {
    final String content = getPageContents(judgeId, problemId);
    List<Input> inputs =
        getInputIds(content).stream()
            .map(inputId -> getLikeCount(content, inputId))
            .sorted()
            .collect(toList());

    int i = 1;
    for (Input input : inputs) {
      FileUtils.writeStringToFile(
          new File(writeDirectory, problemId + "-" + i + ".in"),
          getInputContent(input.inputId()),
          Charset.defaultCharset());
      FileUtils.writeStringToFile(
          new File(writeDirectory, problemId + "-" + i + ".out"), "", Charset.defaultCharset());
      i++;
    }
  }

  private String getInputContent(String inputId) throws IOException {
    HttpPost postRequest =
        new HttpPost("https://www.udebug.com/udebug-custom-get-selected-input-ajax");
    postRequest.addHeader(HttpHeaders.ACCEPT, "application/json");
    postRequest.addHeader(
        HttpHeaders.USER_AGENT,
        "Mozilla/5.0 (X11; Linux i586; rv:31.0) Gecko/20100101 Firefox/72.0");

    List<NameValuePair> params = new LinkedList<>();
    params.add(new BasicNameValuePair("input_nid", inputId));
    postRequest.setEntity(new UrlEncodedFormEntity(params));

    try (final CloseableHttpResponse response = httpClient.execute(postRequest)) {
      final String inputContent = EntityUtils.toString(response.getEntity());
      return gson.fromJson(inputContent, JsonObject.class).get("input_value").getAsString();
    }
  }

  private String getPageContents(String judgeId, String problemId) throws IOException {
    HttpGet getRequest =
        new HttpGet(String.format("https://www.udebug.com/%s/%s", judgeId, problemId));
    getRequest.addHeader(HttpHeaders.ACCEPT, "text/html");
    getRequest.addHeader(
        HttpHeaders.USER_AGENT,
        "Mozilla/5.0 (X11; Linux i586; rv:31.0) Gecko/20100101 Firefox/72.0");
    try (final CloseableHttpResponse response = httpClient.execute(getRequest)) {
      return EntityUtils.toString(response.getEntity());
    }
  }

  private Input getLikeCount(String content, String inputId) {
    final Pattern likeCountPattern =
        Pattern.compile("like_count" + inputId + "\">(?<likeCount>[\\d]*)<");
    final Matcher matcher = likeCountPattern.matcher(content);
    if (!matcher.find()) {
      return new Input(inputId, 0);
    }

    return new Input(inputId, Integer.parseInt(matcher.group("likeCount")));
  }

  private Set<String> getInputIds(String content) {
    final Matcher matcher = INPUT_ID_PATTERN.matcher(content);

    final Set<String> inputIds = new HashSet<>();
    while (matcher.find()) {
      inputIds.add(matcher.group("inputId"));
    }
    return inputIds;
  }

  @Override
  public void close() throws IOException {
    httpClient.close();
  }

  public boolean isCreated(Class<?> clazz) {
    final String canonicalName = clazz.getCanonicalName().replace('.', '/');
    final String[] nameAndPackages = canonicalName.split("/");
    final File writeDirectory =
        new File(RESOURCE_ROOT, "/" + canonicalName.substring(0, canonicalName.lastIndexOf('/')));
    return new File(
            writeDirectory, nameAndPackages[nameAndPackages.length - 1].substring(1) + ".in")
        .isFile();
  }
}

record Input(String inputId, int likeCount) implements Comparable<Input> {
  @Override
  public int compareTo(Input other) {
    if (likeCount != other.likeCount) {
      return Integer.compare(other.likeCount, likeCount);
    }

    return inputId.compareTo(other.inputId);
  }
}
