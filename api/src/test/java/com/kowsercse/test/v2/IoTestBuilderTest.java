package com.kowsercse.test.v2;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

@ExtendWith(HelloExtension.class)
class IoTestBuilderTest {

  @Test
  void testFileIoTestBuilder() {
    IoTest ioTest = new IoTestBuilder(Dummy.class).build();
    assertEquals(Dummy.class, ioTest.clazz());
    assertEquals("Dummy.in", ioTest.input());
    assertEquals("Dummy.out", ioTest.output());
  }
}