package com.kowsercse.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import static org.junit.jupiter.api.DynamicTest.dynamicTest;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.nio.charset.Charset;
import java.time.Duration;
import java.time.LocalTime;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.junit.jupiter.api.DynamicNode;

public class InputOutputTestBuilder {

  public static final String INPUT_EXTENSION = ".in";
  public static final String OUTPUT_EXTENSION = ".out";
  public static final InputStream systemIn = System.in;
  public static final PrintStream systemOut = System.out;
  public static final Object[] MAIN_METHOD_ARGUMENTS = {null};

  private final Class<?> clazz;
  private final List<InputOutput> inputOutputs = new LinkedList<>();
  private ByteArrayOutputStream outputStream;
  private final Method mainMethod;
  private boolean statisticsEnabled;

  public InputOutputTestBuilder(Class<?> clazz) {
    this.clazz = clazz;
    this.mainMethod = getMainMethod(clazz);
  }

  private Method getMainMethod(Class<?> clazz) {
    try {
      return clazz.getDeclaredMethod("main", String[].class);
    } catch (NoSuchMethodException e) {
      throw new IllegalStateException("Method: " + clazz.getName() + ".main is not defined", e);
    }
  }

  public Stream<DynamicNode> build() {
    return inputOutputs.stream()
        .map(
            inputOutput ->
                dynamicTest(
                    inputOutput.toString(),
                    () -> {
                      try {
                        runTest(inputOutput);
                      } catch (RuntimeException ex) {
                        fail(ex);
                      }
                    }));
  }

  private void runTest(InputOutput inputOutput)
      throws IllegalAccessException, InvocationTargetException, IOException {
    try (InputStream input = clazz.getResourceAsStream(inputOutput.input())) {
      setSystemIO(input);
      LocalTime start = LocalTime.now();
      try {
        mainMethod.invoke(null, MAIN_METHOD_ARGUMENTS);
      } finally {
        LocalTime end = LocalTime.now();
        resetSystemIO();
        printStatistics(start, end);
        System.out.println(outputStream.toString(Charset.defaultCharset()));
      }
    }

    if (inputOutput.output() == null) {
      return;
    }

    try (final InputStream output = clazz.getResourceAsStream(inputOutput.output())) {
      assertExpected(output);
    }
  }

  private void printStatistics(LocalTime start, LocalTime end) {
    if (statisticsEnabled) {
      System.err.println(Duration.between(start, end).toMillis());
      System.err.flush();
    }
  }

  private void resetSystemIO() {
    System.setIn(systemIn);
    System.setOut(systemOut);
  }

  protected void setSystemIO(final InputStream inputStream) {
    System.setIn(inputStream);

    outputStream = new ByteArrayOutputStream();
    System.setOut(new PrintStream(outputStream));
  }

  protected void assertExpected(final InputStream inputStream) throws IOException {
    assertEquals(IOUtils.toString(inputStream), outputStream.toString());
  }

  public InputOutputTestBuilder addInputOutput(String input, String output) {
    inputOutputs.add(new InputOutput(input, output));
    return this;
  }

  public InputOutputTestBuilder withStatistics() {
    statisticsEnabled = true;
    return this;
  }

  public static Stream<DynamicNode> of(Class<?> clazz, String separator) {
    final InputOutputTestBuilder inputOutputTestBuilder = new InputOutputTestBuilder(clazz);
    String problemNumber = clazz.getSimpleName().replace("P", "");

    int i = 1;
    String fileNameWithoutExtension = problemNumber;
    URL inputResource = clazz.getResource(fileNameWithoutExtension + INPUT_EXTENSION);
    while (inputResource != null) {
      inputOutputTestBuilder.addInputOutput(
          fileNameWithoutExtension + INPUT_EXTENSION, fileNameWithoutExtension + OUTPUT_EXTENSION);
      inputResource = clazz.getResource(problemNumber + separator + i + INPUT_EXTENSION);
      fileNameWithoutExtension = problemNumber + separator + i++;
    }

    return inputOutputTestBuilder.build();
  }
}
