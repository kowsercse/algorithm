package com.kowsercse.test;

public record InputOutput(String input, String output) {

  @Override
  public String toString() {
    return input + " -> " + output;
  }
}
