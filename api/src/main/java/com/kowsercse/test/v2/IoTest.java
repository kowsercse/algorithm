package com.kowsercse.test.v2;

public record IoTest(Class<?> clazz, String input, String output) {

}