package com.kowsercse.test.v2;

public class IoTestBuilder {

  public static final String IO_FILE_NAME_FORMAT = "%s.%s";
  private String inputExtension = "in";
  private String outputExtension = "out";
  private final Class<?> clazz;

  public IoTestBuilder(Class<?> clazz) {
    this.clazz = clazz;
  }

  public IoTest build() {
    String simpleName = clazz.getSimpleName();
    return new IoTest(clazz, IO_FILE_NAME_FORMAT.formatted(simpleName, inputExtension),
        IO_FILE_NAME_FORMAT.formatted(simpleName, outputExtension));
  }

  public IoTestBuilder setInputExtension(String inputExtension) {
    this.inputExtension = inputExtension;
    return this;
  }

  public String getInputExtension() {
    return inputExtension;
  }

  public IoTestBuilder setOutputExtension(String outputExtension) {
    this.outputExtension = outputExtension;
    return this;
  }

  public String getOutputExtension() {
    return outputExtension;
  }
}
