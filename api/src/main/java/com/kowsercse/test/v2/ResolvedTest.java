package com.kowsercse.test.v2;

import java.io.File;

public record ResolvedTest(String name, Class<?> clazz, File input, File recordedOutput) {

}
