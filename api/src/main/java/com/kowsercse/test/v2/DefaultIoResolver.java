package com.kowsercse.test.v2;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import org.junit.jupiter.api.DynamicTest;

public class DefaultIoResolver {

  public ResolvedTest resolve(IoTest ioTest) throws URISyntaxException {
    return new ResolvedTest(
        getSimpleName(ioTest.clazz()),
        ioTest.clazz(),
        getFile(ioTest.input()),
        getFile(ioTest.output()));
  }

  private String getSimpleName(Class<?> clazz) {
    return clazz.getSimpleName();
  }

  private File getFile(String ioTest) throws URISyntaxException {
    return new File(getClass().getResource(ioTest).toURI());
  }
}
