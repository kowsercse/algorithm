package com.kowsercse.test.v2;

import static org.junit.jupiter.api.Assertions.fail;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.nio.charset.Charset;
import java.time.LocalTime;
import org.junit.jupiter.api.DynamicNode;
import org.junit.jupiter.api.DynamicTest;

public class DynamicNodeFactory {

  DynamicNode of(ResolvedTest resolvedTest) {

    return DynamicTest.dynamicTest(
        resolvedTest.name(),
        () -> {
          try {
//            runTest(resolvedTest);
          } catch (RuntimeException ex) {
            fail(ex);
          }
        });
  }

//  private void runTest(ResolvedTest resolvedTest)
//      throws IllegalAccessException, InvocationTargetException, IOException {
//    try (InputStream input = new FileInputStream(resolvedTest.input())) {
//      setSystemIo(input);
//      LocalTime start = LocalTime.now();
//      try {
//        mainMethod.invoke(null, MAIN_METHOD_ARGUMENTS);
//      } finally {
//        LocalTime end = LocalTime.now();
//        resetSystemIo();
//        printStatistics(start, end);
//        System.out.println(outputStream.toString(Charset.defaultCharset()));
//      }
//    }
//
//    if (inputOutput.output() == null) {
//      return;
//    }
//
//    try (final InputStream output = clazz.getResourceAsStream(inputOutput.output())) {
//      assertExpected(output);
//    }
//  }
}
