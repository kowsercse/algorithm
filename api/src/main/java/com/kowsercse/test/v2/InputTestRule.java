package com.kowsercse.test.v2;

import java.io.InputStream;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

public class InputTestRule implements TestRule {

  private final InputStream systemInputStream = System.in;

  @Override
  public Statement apply(Statement base, Description description) {
    System.out.println("Hello");
    return new Statement() {
      @Override
      public void evaluate() throws Throwable {
        try (InputStream inputStream = newIo(description)) {
          System.setIn(inputStream);
          base.evaluate();
        } finally {
          System.setIn(systemInputStream);
        }
      }
    };
  }

  private InputStream newIo(Description description) {
    return systemInputStream;
  }
}
