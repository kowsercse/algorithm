package com.kowsercse;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.nio.charset.Charset;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.junit.After;
import org.junit.Before;

public abstract class IOTestCase {

  private PrintStream sysOut;
  private ByteArrayOutputStream outContent;

  @Before
  public void beforeEach() {
    sysOut = System.out;
    outContent = new ByteArrayOutputStream();
    System.setOut(new PrintStream(outContent));
  }

  @After
  public void afterEach() {
    System.setOut(sysOut);
  }

  protected void setInput(final String resourceName) {
    final InputStream inputStream = getClass().getResourceAsStream(resourceName);
    System.setIn(inputStream);
  }

  protected void assertExpected(final String expectedOutput) {
    final String expected = getExpectedOutput(expectedOutput);
    assertEquals(expected, outContent.toString(Charset.defaultCharset()));
  }

  private String getExpectedOutput(final String expectedOutput) {
    final InputStream expectedOutputStream = this.getClass().getResourceAsStream(expectedOutput);
    try {
      return IOUtils.toString(expectedOutputStream, Charset.defaultCharset());
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }
}
