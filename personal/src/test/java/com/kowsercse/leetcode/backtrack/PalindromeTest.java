package com.kowsercse.leetcode.backtrack;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class PalindromeTest {

  @Test
  public void test() {
    Palindrome palindrome = new Palindrome();
    int minCut = palindrome.minCut("abaca");
    assertEquals(2, minCut);
  }
}
