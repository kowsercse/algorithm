package com.kowsercse.leetcode.dynamic;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class InterleavingTest {

  private final Interleaving interleaving = new Interleaving();

  @Test
  public void testInterleaving() {
//    assertFalse(interleaving.isInterleave("", "", "a"));
    assertTrue(interleaving.isInterleave("", "", ""));
    assertTrue(interleaving.isInterleave("a", "", "a"));
    assertTrue(interleaving.isInterleave("", "b", "b"));
    assertTrue(interleaving.isInterleave("a", "b", "ab"));
    assertTrue(interleaving.isInterleave("a", "b", "ba"));
    assertTrue(interleaving.isInterleave("aa", "b", "aab"));
    assertTrue(interleaving.isInterleave("aa", "b", "aba"));
    assertTrue(interleaving.isInterleave("aa", "b", "baa"));
    assertTrue(interleaving.isInterleave("aa", "bb", "abab"));
    assertTrue(interleaving.isInterleave("aa", "bb", "aabb"));
    assertFalse(interleaving.isInterleave("ac", "b", "abd"));
    assertFalse(interleaving.isInterleave("db", "b", "cbb"));
  }

}