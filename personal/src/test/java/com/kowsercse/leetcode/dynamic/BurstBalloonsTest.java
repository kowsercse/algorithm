package com.kowsercse.leetcode.dynamic;

import static org.junit.jupiter.api.Assertions.*;

import com.kowsercse.leetcode.dynamic.BurstBalloons.Solution;
import org.junit.jupiter.api.Test;

class BurstBalloonsTest {

  @Test
  public void testBurstBalloons() {
    Solution solution = new Solution();
    assertEquals(167, solution.maxCoins(new int[]{3, 1, 5, 8}));
  }

}