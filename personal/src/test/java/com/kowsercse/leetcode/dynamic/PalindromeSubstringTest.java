package com.kowsercse.leetcode.dynamic;


import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class PalindromeSubstringTest {

  @Test
  void testLongestPalindrome() {
    PalindromeSubstring palindromeSubstring = new PalindromeSubstring();
    assertEquals("ccc", palindromeSubstring.longestPalindrome("ccc"));
  }


}