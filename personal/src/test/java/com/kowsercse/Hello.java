package com.kowsercse;

import java.util.Collection;
import java.util.List;
import org.junit.jupiter.api.Test;

public class Hello {

  @Test
  void testLSB() {
    int value = 0b0011100;
    //    value = 3;

    System.out.println(value);
    System.out.println(Integer.toBinaryString(value));
    System.out.println(Integer.toBinaryString(-value));
    System.out.println(value & (-value));
    System.out.println(value | (-value));
  }

  @Test
  public void takeWhile() {
    List<List<String>> list =
        List.of(List.<String>of("1", "2", "6"), List.<String>of("3", "4", "5", "6", "7"));

    list.stream()
        .flatMap(Collection::stream)
        .takeWhile(i -> !i.equals("4"))
        .forEach(System.out::println);
  }

  private static <T> T process(T input) {
    System.out.println("Processing...");
    return input;
  }
}
