package com.kowsercse.array;

import static com.google.common.truth.Truth.assertThat;

import java.util.Arrays;
import java.util.List;
import org.junit.Ignore;
import org.junit.Test;

public class ProblemsTest {

  @Test
  public void sumOfTwoNumber() {}

  @Test
  public void gameOfLife() {
    boolean[] actual =
        Problems.gameOfLife(new boolean[] {false, true, false, true, true, false, false, true}, 10);
    for (boolean i : actual) {
      System.out.println(i);
    }
  }

  @Test
  public void testApplyPermutation() {
    List<Character> input = Arrays.asList('a', 'b', 'c', 'd');
    Problems.applyPermutation(input, Arrays.asList(3, 2, 1, 0));
    assertThat(input).containsExactly('d', 'c', 'b', 'a').inOrder();
    input = Arrays.asList('a', 'b', 'c', 'd');
    Problems.applyPermutation(input, Arrays.asList(1, 2, 3, 0));
    assertThat(input).containsExactly('d', 'a', 'b', 'c').inOrder();
  }

  @Test
  @Ignore
  // TODO fix it
  public void testNextPermutation2() {
    List<Character> input;
    input = Arrays.asList('a', 'b', 'c', 'd', 'e', 'f', 'g');
    Problems.nextPermutation(input, Arrays.asList(6, 5, 4, 3, 2, 1, 0));
    System.out.println(input);
    List<Character> expected = Arrays.asList('a', 'b', 'c', 'd', 'e', 'f', 'g');
    Problems.applyPermutation(expected, Arrays.asList(6, 5, 4, 3, 2, 1, 0));
    System.out.println(expected);
    assertThat(input).containsExactlyElementsIn(expected).inOrder();
  }

  @Test
  public void testNextPermutation0() {
    List<Character> input;
    input = Arrays.asList('a', 'b', 'c', 'd', 'e', 'f', 'g');
    Problems.nextPermutation(input, Arrays.asList(0, 1, 2, 3, 4, 5, 6));
    System.out.println(input);
    List<Character> expected = Arrays.asList('a', 'b', 'c', 'd', 'e', 'f', 'g');
    Problems.applyPermutation(expected, Arrays.asList(0, 1, 2, 3, 4, 6, 5));
    System.out.println(expected);
    assertThat(input).containsExactlyElementsIn(expected).inOrder();
  }

  @Test
  public void testNextPermutation() {
    List<Character> input;
    input = Arrays.asList('a', 'b', 'c', 'd', 'e', 'f', 'g');
    Problems.nextPermutation(input, Arrays.asList(6, 2, 1, 5, 4, 3, 0));
    System.out.println(input);
    List<Character> expected = Arrays.asList('a', 'b', 'c', 'd', 'e', 'f', 'g');
    Problems.applyPermutation(expected, Arrays.asList(6, 2, 3, 0, 1, 4, 5));
    System.out.println(expected);
    assertThat(input).containsExactlyElementsIn(expected).inOrder();
  }
}
