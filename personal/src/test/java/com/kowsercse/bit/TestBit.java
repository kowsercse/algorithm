package com.kowsercse.bit;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestBit {

  @Test
  public void test1() {
    int BIT_0 = 1;
    int BIT_1 = 1 << 1;
    int BIT_2 = 1 << 2;

    assertEquals(0, 8 & BIT_1);
    assertEquals(10, 8 | BIT_1);
  }

  @Test
  public void test2() {
    final long l = 6L;
    System.out.println((l >> 64) & 1);
    System.out.println((l >> 65) & 1);
    System.out.println((l >> 66) & 1);
    System.out.println((l >> 67) & 1);

    System.out.println((l >> 0) & 1);
    System.out.println((l >> 1) & 1);
    System.out.println((l >> 2) & 1);
    System.out.println((l >> 3) & 1);
  }

  public void test3() {
    System.out.println(get(64));
    System.out.println(get(65));
    System.out.println(get(66));
    System.out.println(get(67));
  }

  @Test
  public void test4() {
    set(65, 1);
    System.out.println(get(65));
    System.out.println(bits[1]);
  }

  private void set(int i, int bit) {
    bits[i / 64] = bits[i / 64] | (bit << (i % 64));
  }

  private long get(int i) {
    return (bits[i / 64] >>> (i % 64)) & 1;
  }

  private long[] bits = new long[100];
}
