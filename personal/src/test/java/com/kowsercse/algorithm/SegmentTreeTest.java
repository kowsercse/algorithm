package com.kowsercse.algorithm;

import static com.google.common.truth.Truth.assertThat;

import com.google.common.collect.ImmutableMap;
import com.google.common.truth.Correspondence;
import java.util.List;
import org.junit.jupiter.api.Test;

class SegmentTreeTest {

  @Test
  void test() {
    SegmentTree segmentTree = new SegmentTree();
    assertThat(segmentTree.createChildNodes("*|**|*||*"))
        .comparingElementsUsing(
            Correspondence.from((Correspondence.BinaryPredicate<Node, Node>) Node::equals, ""))
        .containsExactly(new Node(1, 4, 2), new Node(4, 6, 1), new Node(6, 7, 0))
        .inOrder();
    ImmutableMap<Query, Integer> map =
        ImmutableMap.<Query, Integer>builder()
            .put(new Query(0, 0), 0)
            .put(new Query(1, 6), 3)
            .put(new Query(1, 7), 3)
            .put(new Query(7, 7), 3)
            .build();
    assertThat(segmentTree.countItems("*|**|*||*", List.copyOf(map.keySet())))
        .containsExactly(map.values().toArray())
        .inOrder();
  }
}
