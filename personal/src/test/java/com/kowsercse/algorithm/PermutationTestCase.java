package com.kowsercse.algorithm;

import static com.google.common.truth.Truth.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.*;
import org.junit.jupiter.api.Test;

public class PermutationTestCase {

  @Test
  void testAllPermutation() {
    assertThat(Permutation.allPermutation("122"))
        .containsExactlyElementsIn(Arrays.asList("122", "212", "221"))
        .inOrder();
  }

  @Test
  void testNextPermutation() {
    assertEquals("1243", Permutation.nextPermutation("1234"));
    assertEquals("9130124", Permutation.nextPermutation("9124310"));
  }

  @Test
  public void testPackUnpack() {
    final List<Integer> radix = Arrays.asList(3, 2, 1);
    for (int i = 0; i < 6; i++) {
      final List<Integer> unpack = Permutation.unpack(i, radix);
      System.out.println(unpack);

      final int n = Permutation.pack(unpack, radix);
      System.out.println(n);
    }
  }

  @Test
  public void testNthPermutation() {
    final List<Integer> digits = Arrays.asList(1, 2, 3);
    for (int i = 0; i < 6; i++) {
      final List<Integer> permutation = Permutation.nthPermutation(digits, i, 3);
      System.out.println(permutation);
    }
    final List<Integer> allDigits = Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
    final List<Integer> permutation = Permutation.nthPermutation(allDigits, 1, 10);
    System.out.println(permutation);
  }

  @Test
  public void test1() {
    System.out.println(Permutation.nthPermutation(Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9), 2));
  }
}
