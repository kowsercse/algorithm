package com.kowsercse.algorithm;

import com.kowsercse.IOTestCase;
import java.util.LinkedList;
import org.junit.Test;

public class GreedyAlgorithmsTest extends IOTestCase {

  private GreedyAlgorithms greedyAlgorithms = new GreedyAlgorithms();

  @Test
  public void testMergeIntervals() {
    final LinkedList<Integer[]> mergeIntervals =
        greedyAlgorithms.mergeIntervals(new int[][] {{6, 8}, {1, 9}, {2, 4}, {4, 7}});
    mergeIntervals.forEach(interval -> System.err.println(interval[0] + ":" + interval[1]));
  }

  @Test
  public void testIsSumOfTwoNumber() throws Exception {
    final boolean isSumOfTwoNumber =
        greedyAlgorithms.isSumOfTwoNumber(new int[] {19, 15, 2, 5, 13, 14}, 15);
    System.out.println(isSumOfTwoNumber);
  }
}
