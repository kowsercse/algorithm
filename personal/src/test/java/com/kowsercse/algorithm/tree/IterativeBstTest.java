package com.kowsercse.algorithm.tree;

import java.util.Random;
import org.junit.Test;

/**
 * @author kowsercse@gmail.com
 */
public class IterativeBstTest {

  @Test
  public void testAdd() {
    final IterativeBst iterativeBst1 = new IterativeBst();
    iterativeBst1.add(8);
    iterativeBst1.add(2);
    iterativeBst1.add(1);
    iterativeBst1.add(10);

    for (final Integer integer : iterativeBst1) {
      System.out.println(integer);
    }
    System.out.println();
    System.out.println();
    final Random random = new Random();
    final IterativeBst bst = new IterativeBst();
    for (int i = 0; i < 16; i++) {
      final int integer = random.nextInt(30);
      System.out.printf(" " + integer);
      bst.add(integer);
    }
    System.out.println();
    for (final Integer integer : bst) {
      System.out.printf(" " + integer);
    }
  }

  @Test
  public void mergeBst() {
    final Random random = new Random();
    final IterativeBst bst1 = new IterativeBst();
    for (int i = 0; i < 16; i++) {
      final int integer = random.nextInt(30);
      System.out.printf(" " + integer);
      bst1.add(integer);
    }
    System.out.println();
    final IterativeBst bst2 = new IterativeBst();
    for (int i = 0; i < 16; i++) {
      final int integer = random.nextInt(30);
      System.out.printf(" " + integer);
      bst2.add(integer);
    }
    System.out.println();

    final IterativeBst.BstIterator iterator1 = bst1.iterator();
    final IterativeBst.BstIterator iterator2 = bst2.iterator();
    while (iterator1.hasNext() && iterator2.hasNext()) {
      if (iterator1.peek() < iterator2.peek()) {
        System.out.println(iterator1.next());
      } else {
        System.out.println(iterator2.next());
      }
    }
    while (iterator1.hasNext()) {
      System.out.println(iterator1.next());
    }
    while (iterator2.hasNext()) {
      System.out.println(iterator2.next());
    }
  }
}
