package com.kowsercse.algorithm.tree;

import java.util.stream.Stream;
import org.junit.Test;

/**
 * @author kowsercse@gmail.com
 */
public class TrieTest {

  @Test
  public void testAdd() throws Exception {
    final Trie trie = new Trie('$');
    Stream.of("cat", "can", "capital", "car").forEach(trie::add);
    trie.preOrder();
  }
}
