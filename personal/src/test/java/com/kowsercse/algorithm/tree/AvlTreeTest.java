package com.kowsercse.algorithm.tree;

import java.util.*;
import org.junit.Before;
import org.junit.Test;

public class AvlTreeTest {

  private AvlTree tree;

  @Before
  public void setUp() {
    tree = new AvlTree();
    final Random random = new Random();
    for (int i = 0; i < 15; i++) {
      final int value = i * 2;
      System.out.println(value);
      tree.insert(String.format("%02d", value));
      tree.printEulerTour();
      System.out.println();
      tree.inorderTraversal(node -> System.out.print(" " + node + " "));
      System.out.println();
      tree.preOrderTraversal(node -> System.out.print(" " + node + " "));
      System.out.println();
      tree.postOrderTraversal(node -> System.out.print(" " + node + " "));

      System.out.println("\n--------------");
    }
  }

  @Test
  public void testInorder() throws Exception {
    tree.inorderTraversal(node -> System.out.print(" " + node + " "));
  }

  @Test
  public void testEulerTour() throws Exception {
    tree.printEulerTour();
  }

  @Test
  public void testDelete() throws Exception {
    System.out.println("---- DELETION ----");
    final List<Integer> integers = Arrays.asList(3, 11, 2, 10);
    for (int i = 0; i < 15; i++) {
      int value = i;
      tree.printEulerTour();
      System.out.println();
      System.out.println("Delete: " + value);
      tree.delete(String.format("%02d", value));
      tree.printEulerTour();
      System.out.println();
      tree.inorderTraversal(node -> System.out.print(" " + node + " "));
      System.out.println();
      tree.preOrderTraversal(node -> System.out.print(" " + node + " "));
      System.out.println();
      tree.postOrderTraversal(node -> System.out.print(" " + node + " "));

      System.out.println("\n--------------");
    }
  }

  @Test
  public void testIsBst() throws Exception {
    System.out.println(tree.isBst());
  }

  @Test
  public void testLeastCommonAncestor() {
    System.out.println(tree.leastCommonAncestor("07", "14"));
  }

  @Test
  public void testFloor() {
    System.out.println("ceil 07: " + tree.ceil("07"));
    System.out.println("floor 07: " + tree.floor("07"));
    System.out.println("ceil 17: " + tree.ceil("17"));
    System.out.println("floor 17: " + tree.floor("17"));
  }
}
