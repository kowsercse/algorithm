package com.kowsercse.algorithm.tree;

import java.util.Random;
import org.junit.Before;
import org.junit.Test;

public class BinarySearchTreeTestCase {

  private IndexedBinarySearchTree tree;

  @Before
  public void setUp() {
    final Random random = new Random();
    tree = new IndexedBinarySearchTree();
    for (int i = 0; i < 10; i++) {
      final int anInt = random.nextInt(100);
      System.out.println(anInt);
      tree.insert(new Node("" + anInt));
    }
    System.out.println();
  }

  @Test
  public void testTree() {
    tree.inorderTraversal(System.out::println);
  }
}
