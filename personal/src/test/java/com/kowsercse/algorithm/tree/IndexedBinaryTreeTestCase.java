package com.kowsercse.algorithm.tree;

import org.junit.Before;
import org.junit.Test;

public class IndexedBinaryTreeTestCase {

  private IndexedBinaryTree tree;

  @Before
  public void setUp() {
    tree = new IndexedBinaryTree();
    // 8 / 4 + 3 * 2
    tree.add(0, new Node("+"));
    tree.add(1, new Node("/"));
    tree.add(2, new Node("*"));
    tree.add(4, new Node("8"));
    tree.add(5, new Node("4"));
    tree.add(6, new Node("3"));
    tree.add(7, new Node("2"));
  }

  @Test
  public void testPreOrder() {
    tree.preOrderTraversal(node -> System.out.println(node.getValue()));
  }

  @Test
  public void testInorder() {
    tree.inorderTraversal(node -> System.out.println(node.getValue()));
  }

  @Test
  public void testPostOrder() {
    tree.postOrderTraversal(node -> System.out.println(node.getValue()));
  }

  @Test
  public void testEulerTour() {
    tree.eulerTour(node -> System.out.print(node.getValue()));
  }
}
