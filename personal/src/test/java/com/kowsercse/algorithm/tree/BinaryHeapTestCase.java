package com.kowsercse.algorithm.tree;

import java.util.Random;
import org.junit.Before;
import org.junit.Test;

public class BinaryHeapTestCase {

  private BinaryHeap binaryHeap;
  private Random random;

  @Before
  public void setUp() {
    binaryHeap = new BinaryHeap();
    random = new Random();
  }

  @Test
  public void testPush() {
    for (int i = 0; i < 10; i++) {
      int x = random.nextInt(100);
      System.out.println(x);
      binaryHeap.push(x);
    }

    System.out.println("Pop");
    for (int i = 0; i < 10; i++) {
      Integer pop = binaryHeap.pop();
      System.out.println(pop);
    }
  }

  @Test
  public void testInPlaceSort() {
    binaryHeap.inPlaceSort();
  }
}
