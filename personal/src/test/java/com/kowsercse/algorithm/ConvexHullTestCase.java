package com.kowsercse.algorithm;

import java.util.Scanner;
import java.util.Stack;
import org.junit.Before;
import org.junit.Test;

public class ConvexHullTestCase {

  private ConvexHull convexHull;

  @Before
  public void setUp() {
    convexHull = new ConvexHull();
  }

  @Test
  public void testConvexHull2() {
    final Scanner scanner = new Scanner(this.getClass().getResourceAsStream("/convex-hull.txt"));
    final int total = scanner.nextInt();
    for (int i = 0; i < total; i++) {
      final int totalInput = scanner.nextInt();
      final ConvexHull convexHull = new ConvexHull();
      for (int j = 0; j < totalInput; j++) {
        convexHull.add(scanner.nextInt(), scanner.nextInt());
      }
      convexHull.calculate();
      System.out.println();
    }
  }

  @Test
  public void testConvexHull() {
    convexHull.add(3, 0);
    convexHull.add(3, 1);
    convexHull.add(3, 2);
    convexHull.add(3, 3);
    convexHull.add(0, 3);
    convexHull.add(0, 2);
    convexHull.add(0, 1);
    convexHull.add(1, 1);
    convexHull.add(0, 0);
    convexHull.add(1, 0);
    convexHull.add(2, 0);

    final Stack<Point> hull = convexHull.calculate();
  }
}
