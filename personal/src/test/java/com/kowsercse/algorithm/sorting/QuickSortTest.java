package com.kowsercse.algorithm.sorting;

import com.google.common.truth.Truth;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.Test;

public class QuickSortTest {

  @Test
  public void testSort() {
    List<Integer> list = Arrays.asList(3, 4, 5, 6, 7, 1, 2, 3, 6, 9, 10);
    QuickSort.sort(list);
    Truth.assertThat(list).isInOrder();
  }
}
