package com.kowsercse.algorithm;

import static com.google.common.truth.Truth.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;
import org.junit.jupiter.api.Test;

public class CombinationTest {
  @Test
  void testCombination() {
    Combination combination = new Combination("12345".toCharArray(), 3);
    combination.calculate();
    List<String> result = combination.getResult();
    System.out.println(String.join("%n", result));
    assertEquals(10, result.size());
    assertThat(result)
        .containsExactly("123", "124", "125", "134", "135", "145", "234", "235", "245", "345")
        .inOrder();
  }
}
