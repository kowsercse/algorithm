package com.kowsercse.algorithm;

import com.kowsercse.algorithm.graph.Graph;
import com.kowsercse.algorithm.graph.MinimumSpanningTree;
import java.util.Arrays;
import java.util.Stack;
import org.junit.Before;
import org.junit.Test;

public class GraphTestCase {

  private Graph graph;

  @Before
  public void setUp() {
    int[][] edges = {
      {1, 2, 6},
      {1, 4, 5},
      //        {3, 2, 5},
      //        {3, 4, 5},
      //        {3, 5, 6},
      //        {3, 6, 4},
      //        {3, 1, 1},
      {5, 2, 3},
      //        {5, 6, 6},
      {6, 4, 2},
    };
    graph = new Graph(6);

    for (final int[] edge : edges) {
      graph.addEdge(edge[0], edge[1], edge[2]);
    }
  }

  @Test
  public void testBfs() {
    graph.bfs(1);
  }

  @Test
  public void testDfs() {
    graph.dfs(1);
  }

  @Test
  public void testTopologicalSort() {
    Graph graph = new Graph(6);
    Arrays.stream(
            new Integer[][] {
              {2, 1},
              {1, 6},
              {4, 2},
              {4, 3},
              {5, 3},
              {5, 6},
            })
        .forEach(edge -> graph.addEdge(edge[0], edge[1], 1));
    final Stack<Integer> topologicalSort = graph.topologicalSort();
    while (!topologicalSort.isEmpty()) {
      System.out.println((char) (topologicalSort.pop() - 1 + 'A'));
    }
  }

  @Test
  public void testMinimumSpanningTree() {
    int[][] edges = {
      {1, 2, 6},
      {1, 4, 5},
      {3, 2, 5},
      {3, 4, 5},
      {3, 5, 6},
      {3, 6, 4},
      {3, 1, 1},
      {5, 2, 3},
      {5, 6, 6},
      {6, 4, 2},
    };
    MinimumSpanningTree graph = new MinimumSpanningTree(6);

    for (final int[] edge : edges) {
      graph.addEdge(edge[0], edge[1], edge[2]);
    }

    graph.calculate().forEach(System.out::println);
  }
}
