package com.kowsercse.algorithm.dp;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

/**
 * @author kowsercse@gmail.com
 */
public class DynamicProgrammingTest {

  private final DynamicProgramming dp = new DynamicProgramming();

  @Test
  @Disabled
  public void testLcs() {
    String actual = dp.lcs("GAC", "AGCAT");
    assertEquals("Didn't match", "AC", actual);

    actual = dp.lcs("AGCAT", "GAC");
    assertEquals("Didn't match", "GA", actual);
  }

  @Test
  public void testLis() {
    final int lis = dp.lis(Arrays.asList(100, 10, 200, 20, 300, 30));
    System.out.println("lis = " + lis);
    final int lis1 = dp.lis(Arrays.asList(1, 101, 2, 3, 100, 4, 5));
    System.out.println("lis1 = " + lis1);
    final int lis2 = dp.lis(Arrays.asList(0, 8, 4, 12, 2, 10, 6, 14, 1, 9, 5, 13, 3, 11, 7, 15));
    System.out.println("lis2 = " + lis2);
  }

  @Test
  public void testGetMaxEggDrop() throws Exception {
    final int maxEggDrop = dp.getMaxEggDrop(10, 16);
    System.out.println("maxEggDrop = " + maxEggDrop);
    assertEquals(maxEggDrop, 5);
  }

  @Test
  public void testKmp() {
    final int result = dp.kmp("ABCDABD", "ABC ABCDAB ABCDABCDABDE");
    System.out.println("ABCDABD = " + result);
    assertEquals(result, 22);
  }

  @Test
  public void testGetMaxRectangularArea() {
    final int maxRectangularArea = dp.getMaxRectangularArea(new int[] {2, 1, 2, 3, 1});
    System.out.println("maxRectangularArea = " + maxRectangularArea);
    assertEquals(5, maxRectangularArea);
    final int maxRectangularArea2 = dp.getMaxRectangularArea(new int[] {6, 2, 5, 4, 5, 1, 6});
    System.out.println("maxRectangularArea2 = " + maxRectangularArea2);
    assertEquals(12, maxRectangularArea2);
    final int maxRectangularArea3 = dp.getMaxRectangularArea(new int[] {1, 2, 4});
    System.out.println("maxRectangularArea3 = " + maxRectangularArea3);
    assertEquals(4, maxRectangularArea3);
  }

  @Test
  public void testEditDistance() {
    final int distance = dp.editDistance("geek", "gesek");
    assertEquals(1, distance);
  }

  @Test
  public void testEvaluateZeroOneKnapsack() {
    final int knapsack =
        dp.evaluateZeroOneKnapsack(50, Arrays.asList(10, 20, 30), Arrays.asList(60, 100, 120));
    System.out.println("knapsack = " + knapsack);
    assertEquals(220, knapsack);
  }

  @Test
  public void testEvaluateZeroOneKnapsack1() {
    int knapsack =
        dp.evaluateZeroOneKnapsack(5, Arrays.asList(1, 1, 2, 4), Arrays.asList(2, 3, 3, 4));
    knapsack = dp.evaluateZeroOneKnapsack(7, Arrays.asList(1, 3, 4, 5), Arrays.asList(1, 4, 5, 7));
    System.out.println("knapsack = " + knapsack);
  }

  @Test
  public void testMinCostPath() {
    int cost[][] = {
      {1, 2, 3},
      {4, 8, 2},
      {1, 5, 3}
    };
    final int minCostPath = dp.minCostPath(cost);
    System.out.println("minCostPath = " + minCostPath);
  }

  @Test
  public void testLongestPalindromeSubSequence() {
    final String sequence = "ZBXXBABZ";
    final int length = dp.longestPalindromeSubSequence(sequence);
    System.out.println("longestPalindromeSubSequence = " + length);
    assertEquals(6, length);
  }

  @Test
  public void testMaxSumIncreasingSubsequence() {
    final int maxSumIncreasingSubSequence =
        dp.maxSumIncreasingSubSequence(Arrays.asList(1, 101, 2, 3, 100, 4, 5));
    System.out.println("maxSumIncreasingSubSequence = " + maxSumIncreasingSubSequence);
  }

  @Test
  public void testMaxProfit() {
    final int getMaxProfitFromShare =
        dp.getMaxProfitFromShare(Arrays.asList(2, 30, 15, 10, 8, 25, 80));
    System.out.println("getMaxProfitFromShare = " + getMaxProfitFromShare);
  }

  @Test
  public void testMaxProfitSingle() {
    final int maxProfitSingle = dp.maxProfitSingle(Arrays.asList(2, 30, 15, 10, 8, 25, 80));
    System.out.println("maxProfitSingle = " + maxProfitSingle);
  }

  @Test
  public void testGetMaxVolume() {
    final int maxVolume = dp.getMaxVolume(new int[] {2, 1, 2, 2}, new int[] {1, 1, 1}, 1, 1);
    System.out.println("maxVolume = " + maxVolume);
  }

  @Test
  public void testCalculateMatrixChainMultiplication() {
    final int multiplication =
        dp.calculateMatrixChainMultiplication(new int[] {1, 2, 3, 4, 5, 6, 7});
    System.out.println("multiplication = " + multiplication);
  }

  @Test
  public void testCoinChange() {
    final int coinChange = dp.calculateCoinChange(new int[] {3, 2, 1}, 6);
    System.out.println("coinChange = " + coinChange);
  }

  @Test
  public void testAllPossibleSum() {
    final int[] allPossibleSum = dp.calculateAllPossibleSum(new int[] {6, 2, 1, 3, 5});
    for (final int total : allPossibleSum) {
      System.out.println(total);
    }
  }

  @Test
  public void testSubsetSum() {
    assertTrue(dp.subsetSumExists(new int[] {3, 34, 4, 12, 5, 2}, 9));
    assertTrue(dp.subsetSumExists(new int[] {3, 34, 4, 12, 5, 2}, 11));
    assertTrue(dp.subsetSumExists(new int[] {3, 34, 4, 12, 5, 2}, 12));
  }
}
