package com.kowsercse.algorithm.dp;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class PerfectSquaresTest {
  PerfectSquares perfectSquares = new PerfectSquares();

  @Test
  public void test() {
    assertEquals(3, perfectSquares.numSquares(12));
    assertEquals(3, perfectSquares.numSquares(6));
    assertEquals(1, perfectSquares.numSquares(1));
  }
}
