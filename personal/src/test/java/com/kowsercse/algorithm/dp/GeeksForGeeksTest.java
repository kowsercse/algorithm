package com.kowsercse.algorithm.dp;

import com.kowsercse.algorithm.GeeksForGeeks;
import org.junit.Test;

/**
 * @author kowsercse@gmail.com
 */
public class GeeksForGeeksTest {

  private GeeksForGeeks geeksForGeeks = new GeeksForGeeks();

  @Test
  public void testFindPlatform() {
    final int platform =
        geeksForGeeks.findPlatform(
            new int[] {900, 940, 950, 1100, 1500, 1800},
            new int[] {910, 1200, 1120, 1130, 1900, 2000});
    System.out.println("platform = " + platform);
  }

  @Test
  public void testConstructTree() {
    final GeeksForGeeks.Node root =
        geeksForGeeks.constructTree(
            new int[] {10, 30, 20, 5, 15}, new char[] {'N', 'N', 'L', 'L', 'L'});
    root.inOrder();
  }
}
