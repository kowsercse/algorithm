package com.kowsercse.algorithm.dp;

import static org.junit.Assert.*;

import java.util.Arrays;
import org.junit.Test;

public class BuySellStockTest {

  @Test
  public void testMaxProfitForOneSell() {
    assertEquals(5, BuySellStock.maxProfit(Arrays.asList(2, 5, 7, 1, 4, 1, 3)));
  }

  @Test
  public void testMaxProfitForTransaction() {
    assertEquals(
        30,
        BuySellStock.maxProfitForTransactionVersionOne(
            1, Arrays.asList(310, 315, 275, 295, 260, 270, 290, 230, 255, 250)));
    assertEquals(
        5, BuySellStock.maxProfitForTransactionVersionOne(1, Arrays.asList(2, 5, 7, 1, 4, 1, 3)));
    assertEquals(
        8, BuySellStock.maxProfitForTransactionVersionOne(2, Arrays.asList(2, 5, 7, 1, 4, 1, 3)));
    assertEquals(
        10, BuySellStock.maxProfitForTransactionVersionOne(3, Arrays.asList(2, 5, 7, 1, 4, 1, 3)));
  }

  @Test
  public void testMaxProfitForTransactionOptimized() {
    assertEquals(
        30,
        BuySellStock.maxProfitForTransactionOptimized(
            1, Arrays.asList(310, 315, 275, 295, 260, 270, 290, 230, 255, 250)));
    assertEquals(
        5, BuySellStock.maxProfitForTransactionOptimized(1, Arrays.asList(2, 5, 7, 1, 4, 1, 3)));
    assertEquals(
        8, BuySellStock.maxProfitForTransactionOptimized(2, Arrays.asList(2, 5, 7, 1, 4, 1, 3)));
    assertEquals(
        10, BuySellStock.maxProfitForTransactionOptimized(3, Arrays.asList(2, 5, 7, 1, 4, 1, 3)));
  }
}
