package com.kowsercse.algorithm;

import java.util.Arrays;
import org.junit.Test;

/**
 * @author kowsercse@gmail.com
 */
public class AdHocProblemTest {
  AdHocProblem adHocProblem = new AdHocProblem();

  @Test
  public void testGetMedian() throws Exception {
    adHocProblem.getMedian(new int[] {1, 12, 15, 26, 38}, new int[] {2, 13, 17, 30, 45});
  }

  @Test
  public void testConstructTree() throws Exception {
    final AdHocProblem.Node node =
        adHocProblem.constructTree(
            Arrays.asList('A', 'B', 'D', 'E', 'C', 'F'),
            Arrays.asList('D', 'B', 'E', 'A', 'F', 'C'));
    node.inOrder();
  }
}
