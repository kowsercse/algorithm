package com.kowsercse.algorithm.simple;

import com.google.common.collect.ImmutableList;
import com.google.common.truth.Truth;
import java.util.List;
import org.junit.Test;

public class BinaryTreeTest {

  public static final List<Integer> IN_ORDER_TRAVERSAL = List.of(1, 2, 3, 4, 5, 6, 7);

  @Test
  public void testConstructTreeFromPreOrderTraversal() {
    BinaryNode binaryNode =
        BinaryNode.fromPreOrder(IN_ORDER_TRAVERSAL, List.of(4, 2, 1, 3, 6, 5, 7));
    ImmutableList.Builder<Integer> builder = ImmutableList.builder();
    binaryNode.inOrder(element -> builder.add(element.getValue()));
    Truth.assertThat(builder.build()).containsExactlyElementsIn(IN_ORDER_TRAVERSAL);
  }

  @Test
  public void testConstructTreeFromPostOrderTraversal() {
    BinaryNode binaryNode =
        BinaryNode.fromPostOrder(IN_ORDER_TRAVERSAL, List.of(1, 3, 2, 5, 7, 6, 4));
    ImmutableList.Builder<Integer> builder = ImmutableList.builder();
    binaryNode.inOrder(element -> builder.add(element.getValue()));
    Truth.assertThat(builder.build()).containsExactlyElementsIn(IN_ORDER_TRAVERSAL);
  }
}
