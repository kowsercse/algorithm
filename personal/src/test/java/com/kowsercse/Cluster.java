package com.kowsercse;

import java.util.HashMap;
import java.util.Map;
import org.junit.Test;

public class Cluster {
  int[][] field = {
    {1, 1, 0, 1, 1, 1, 0},
    {1, 0, 1, 1, 1, 0, 1},
    {0, 1, 1, 1, 0, 1, 1},
    {1, 1, 1, 0, 1, 1, 1},
    {1, 1, 0, 1, 0, 1, 1},
    {1, 0, 1, 1, 1, 0, 1},
    {0, 1, 1, 1, 1, 1, 0},
  };
  boolean[][] visited = new boolean[field.length][field.length];
  Map<Integer, Integer> clusterSize = new HashMap<>();

  @Test
  public void testCluster() {
    for (int i = 0; i < field.length; i++) {
      for (int j = 0; j < field.length; j++) {
        if (!visited[i][j]) {
          dfs(i, j, clusterSize.size() + 1);
        }
      }
    }

    System.out.println(clusterSize);
  }

  private void dfs(int row, int column, int clusterId) {
    if (row < 0 || column < 0 || row >= field.length || column >= field.length) {
      return;
    }
    if (visited[row][column]) {
      return;
    }

    visited[row][column] = true;
    if (field[row][column] == 0) {
      return;
    }

    clusterSize.compute(clusterId, (id, size) -> size == null ? 1 : size + 1);

    dfs(row, column + 1, clusterId);
    dfs(row, column - 1, clusterId);
    dfs(row + 1, column, clusterId);
    dfs(row - 1, column, clusterId);
  }
}
