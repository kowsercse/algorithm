//import java.math.BigDecimal;
//import java.util.*;
//import java.util.concurrent.ConcurrentHashMap;
//import java.util.concurrent.locks.ReadWriteLock;
//import org.junit.Assert;
//import org.junit.jupiter.api.Test;
//import org.junit.runner.JUnitCore;
//
///**
// * This is a Vending Machine! Complete the stub methods to make it work!
// */
//public class Solution {
//
//  public static void main(String[] args) {
//    JUnitCore.main("Solution");
//  }
//
//  @Test
//  public void testVendingMachine() {
//
//    VendingMachine machine = new VendingMachine();
//    Item cookie = new Item(100);
//    Item candyBar = new Item(150);
//    machine.addItem("1A", cookie);
//    machine.addItem("1A", cookie);
//    machine.addItem("2B", candyBar);
//    machine.addChange(200);
//
//    Assert.assertEquals(2, machine.getItemCount("1A"));
//    Assert.assertEquals(100, machine.getItem("1A").cost);
//
//    VendingMachine.VendedItem vended = machine.vend("1A", 125);
//    Assert.assertEquals(cookie, vended.item);
//    Assert.assertEquals(25, vended.change);
//
//    // show that the item stock is decremented
//    Assert.assertEquals(1, machine.getItemCount("1A"));
//    // machine started with 200, the user added 125, and the machine vended 25, so we should have 200 + 125 - 25 = 300
//    Assert.assertEquals(300, machine.getChange());
//
//    Assert.assertEquals(0, machine.getItemCount("3C"));
//    try {
//      machine.getItem("3C");
//      Assert.fail("Item is out of stock. Machine should throw exception, but it didn't");
//    } catch (IllegalArgumentException ex) {
//    }
//
//    try {
//      machine.vend("2B", 100);
//      Assert.fail(
//          "Insufficient provided change to purchase item. Machine should throw exception, but it didn't");
//    } catch (IllegalArgumentException ex) {
//    }
//
//    try {
//      machine.vend("2B", 1000);
//      Assert.fail(
//          "Machine has insufficient change to vend item. Machine should throw exception, but it didn't");
//    } catch (IllegalArgumentException ex) {
//    }
//
//  }
//
//  public static class VendingMachine {
//
//    private final Map<String, ReadWriteLock> lockByCode = new HashMap<>();
//
//    private final Map<String, Queue<Item>> itemsByCode = new HashMap<>();
//    private int change = 0;
//
//    /**
//     * Vend the provided item based on its code. Once the item is vended, it should be removed from
//     * the machine's stock and the customer's change added to the machine
//     *
//     * @param code           the item code
//     * @param providedChange the change the user provides to purchase the item
//     * @return the vended item
//     * @throws IllegalArgumentException if the item is out of stock
//     * @throws IllegalArgumentException if providedChange is less than the cost of the item
//     * @throws IllegalArgumentException if the vending machine has insufficient change to vend the
//     *                                  item
//     */
//    public VendedItem vend(String code, int providedChange) {
//      concurrentHashMap.get(code)
//      Queue<Item> itemsInternal = getItemsInternal(code);
//      Item item = itemsInternal.peek();
//      if (item.cost > providedChange) {
//        throw new IllegalArgumentException();
//      }
//      int returnChange = providedChange - item.cost;
//      if (returnChange > change) {
//        throw new IllegalStateException();
//      }
//
//      itemsInternal.poll();
//      addChange(providedChange-returnChange);
//      return new VendedItem(item, returnChange);
//    }
//
//    /**
//     * Get an item by its code. Throw an exception if not found
//     *
//     * @return the item
//     * @throws IllegalArgumentException if the item is not found or is out of stock
//     */
//    public Item getItem(String code) {
//      return getItemsInternal(code).peek();
//    }
//
//    private Queue<Item> getItemsInternal(String code) {
//      Queue<Item> items = itemsByCode.get(code);
//      if (items == null || items.size() == 0) {
//        throw new IllegalArgumentException();
//      }
//      return items;
//    }
//
//    /**
//     * Get the number of items with this code. Return 0 if the item is not available or is out of
//     * stock
//     *
//     * @param code the item code
//     * @return the number of items
//     */
//    public int getItemCount(String code) {
//      return itemsByCode.getOrDefault(code, new LinkedList<>()).size();
//    }
//
//    /**
//     * Add an item to the vending machine
//     *
//     * @param code the item code
//     * @param item the item
//     */
//    public void addItem(String code, Item item) {
//      Objects.requireNonNull(item);
//      if (code == null || code.isEmpty()) {
//        throw new IllegalArgumentException();
//      }
//
//      itemsByCode.computeIfAbsent(code, (key) -> new LinkedList<>()).add(item);
//    }
//
//    /**
//     * Remove an item from the vending machine
//     *
//     * @param code the item code
//     * @param item the item
//     */
//    public void removeItem(String code, Item item) {
//      throw new IllegalStateException();
//    }
//
//    /**
//     * Add change to the vending machine
//     *
//     * @param change the change to add
//     */
//    public void addChange(int change) {
////      if (change <= 0) {
////        throw new IllegalArgumentException();
////      }
//      this.change += change;
//    }
//
//    /**
//     * Remove change from the vending machine
//     *
//     * @param change the change to remove
//     */
//    public void removeChange(int change) {
//      throw new IllegalStateException();
//    }
//
//    /**
//     * Get the change in the vending machine
//     *
//     * @return the change in the vending machine
//     */
//    public int getChange() {
//      return change;
//    }
//
//    public static class VendedItem {
//
//      Item item;
//      int change;
//
//      public VendedItem(Item item, int change) {
//        this.item = item;
//        this.change = change;
//      }
//    }
//
//  }
//
//  public static class Item {
//
//    int cost;
//
//    public Item(int cost) {
//      this.cost = cost;
//    }
//  }
//
//}
