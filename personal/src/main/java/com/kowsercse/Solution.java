package com.kowsercse;

import java.util.*;

public class Solution {

  public static void main(String... args) {
    Scanner scanner = new Scanner(System.in);
    int T = scanner.nextInt();
    for (int i = 0; i < T; i++) {
      System.out.printf("Case #%d: %d%n", i, solve(scanner.next(), scanner.next()));
    }
  }

  private static int solve(String S, String F) {
    int[] frequency = new int[26];
    for (char c : S.toCharArray()) {
      frequency[c - 'a']++;
    }

    int totalCost = 0;
    for (int i = 0; i < 26; i++) {
      if (frequency[i] == 0) {
        continue;
      }

      int minCost = Integer.MAX_VALUE;
      for (char f : F.toCharArray()) {
        int currentCost = Math.min(Math.abs(f - 'a' - i), Math.abs(i - f + 'a'));
        minCost = Math.min(minCost, currentCost);
      }
      totalCost += frequency[i] * minCost;
    }

    return totalCost;
  }
}