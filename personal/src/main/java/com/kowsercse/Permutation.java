package com.kowsercse;

import java.util.Arrays;
import java.util.Stack;

public class Permutation {
  void combination(int[] nums) {
    backtrack(nums, new Stack<>(), 0);
  }

  private void backtrack(int[] nums, Stack<Integer> stack, int current) {
    if (current == nums.length) {
      System.out.println(stack);
      return;
    }

    backtrack(nums, stack, current + 1);
    stack.push(nums[current]);
    backtrack(nums, stack, current + 1);
    stack.pop();
  }

  void permute(int[] nums) {
    backtrack(nums, 0);
  }

  private void backtrack(int[] nums, int position) {
    if (position == nums.length) {
      System.out.println(Arrays.toString(nums));
      return;
    }

    for (int i = position; i < nums.length; i++) {
      swap(nums, position, i);
      backtrack(nums, position + 1);
      swap(nums, position, i);
    }
  }

  private void swap(int[] nums, int left, int right) {
    int temp = nums[left];
    nums[left] = nums[right];
    nums[right] = temp;
  }
}
