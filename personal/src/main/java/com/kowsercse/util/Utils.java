package com.kowsercse.util;

import java.util.StringJoiner;

public class Utils {
  public static void printTable(final int[] table) {
    for (final int i : table) {
      System.out.print(i + "\t");
    }
    System.out.println();
  }

  public static void printTable(final int[][] table) {
    for (final int[] row : table) {
      for (final int i : row) {
        System.out.print(i + "\t");
      }
      System.out.println();
    }
  }

  public static void printTable(final Integer[][] table) {
    printTable(table, "%d\t");
  }

  public static void printTable(final Integer[][] table, String format) {
    for (final Integer[] row : table) {
      StringJoiner stringJoiner = new StringJoiner("|");
      for (final Integer i : row) {
        stringJoiner.add(String.format(format, i));
      }
      System.out.println(stringJoiner);
    }
  }

  public static void printTable(final boolean[][] table) {
    for (final boolean[] booleans : table) {
      for (final boolean aBoolean : booleans) {
        System.out.print(aBoolean ? "1 " : "0 ");
      }
      System.out.println();
    }
  }
}
