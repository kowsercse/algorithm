package com.kowsercse.algorithm;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class UnionFindPathCompression {

  private Map<String, String> roots = new HashMap<>();
  private Set<String> nodes = new HashSet<>();

  public void union(String node1, String node2) {
    final String root1 = getRoot(node1);
    final String root2 = getRoot(node2);
    if (root1.equals(root2)) {
      return;
    }

    roots.put(node2, node1);
  }

  public int getSize(String name) {
    int size = 0;
    final String root = getRoot(name);
    for (String node : nodes) {
      final String nameRoot = getRoot(node);
      if (root.equals(nameRoot)) {
        size++;
      }
    }

    return size;
  }

  public void initializeRoot(String name) {
    if (roots.get(name) == null) {
      roots.put(name, name);
      nodes.add(name);
    }
  }

  private String getRoot(String name) {
    String root = roots.get(name);
    while (!name.equals(root)) {
      roots.put(name, roots.get(root));
      name = root;
      root = roots.get(name);
    }
    return root;
  }
}
