package com.kowsercse.algorithm.tree;

import java.util.ArrayList;
import java.util.List;

public class BinaryNode extends Node<BinaryNode> {

  public static final BinaryNode MIN =
      new BinaryNode("") {
        @Override
        public int compareTo(final Node o) {
          return Integer.MIN_VALUE;
        }
      };
  public static final BinaryNode MAX =
      new BinaryNode("") {
        @Override
        public int compareTo(final Node o) {
          return Integer.MAX_VALUE;
        }
      };

  private BinaryNode left;
  private BinaryNode right;
  private int height = 1;

  public BinaryNode(final String value) {
    super(value);
  }

  public BinaryNode getLeft() {
    return left;
  }

  public void setLeft(final BinaryNode left) {
    this.left = left;
    if (left != null) {
      left.parent = this;
    }
  }

  public BinaryNode getRight() {
    return right;
  }

  public void setRight(final BinaryNode right) {
    this.right = right;
    if (right != null) {
      right.parent = this;
    }
  }

  public List<BinaryNode> children() {
    List<BinaryNode> children = new ArrayList<>();
    if (left != null) {
      children.add(left);
    }
    if (right != null) {
      children.add(right);
    }
    return children;
  }

  public int getHeight() {
    return height;
  }

  public void updateHeight() {
    height = 1 + Math.max(getLeftHeight(), getRightHeight());
  }

  public int getRightHeight() {
    return right != null ? right.getHeight() : 0;
  }

  public int getLeftHeight() {
    return left != null ? left.getHeight() : 0;
  }
}
