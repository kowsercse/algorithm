package com.kowsercse.algorithm.tree;

import java.util.ArrayList;
import java.util.List;

public class BinaryHeap {
  private List<Integer> arrayList = new ArrayList<>();

  public void push(final int data) {
    arrayList.add(data);
    lift(arrayList.size() - 1);
  }

  public Integer pop() {
    if (arrayList.size() == 0) {
      return null;
    }

    Integer top = arrayList.get(0);
    Integer removed = arrayList.remove(arrayList.size() - 1);
    if (arrayList.size() > 0) {
      arrayList.set(0, removed);
    }

    if (arrayList.size() > 0) {
      sink(0);
    }
    return top;
  }

  public void inPlaceSort() {
    for (int i = 1; i < arrayList.size(); i++) {
      lift(i);
    }
  }

  private void sink(final int index) {
    int size = arrayList.size();
    final Integer data = arrayList.get(index);

    final int leftIndex = index * 2;
    final int rightIndex = leftIndex + 1;

    final Integer left = leftIndex < size ? arrayList.get(leftIndex) : Integer.MAX_VALUE;
    final Integer right = rightIndex < size ? arrayList.get(rightIndex) : Integer.MAX_VALUE;

    if (data > left && data > right) {
      if (left < right) {
        arrayList.set(leftIndex, data);
        arrayList.set(index, left);
        sink(leftIndex);
      } else {
        arrayList.set(index, right);
        arrayList.set(rightIndex, data);
        sink(rightIndex);
      }
    } else if (data > left) {
      arrayList.set(leftIndex, data);
      arrayList.set(index, left);
      sink(leftIndex);
    } else if (data > right) {
      arrayList.set(index, right);
      arrayList.set(rightIndex, data);
      sink(rightIndex);
    }
  }

  private void lift(final int index) {
    int mid = index / 2;
    Integer indexData = arrayList.get(index);
    Integer midData = arrayList.get(mid);
    if (indexData < midData) {
      arrayList.set(mid, indexData);
      arrayList.set(index, midData);
      lift(mid);
    }
  }
}
