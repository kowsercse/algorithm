package com.kowsercse.algorithm.tree;

import java.util.Objects;

public class Node<T extends Node> implements Comparable<Node> {
  private String value;
  T parent;

  public Node(final String value) {
    this.value = value;
  }

  public String getValue() {
    return value;
  }

  public String setValue(final String value) {
    final String oldValue = this.value;
    this.value = value;
    return oldValue;
  }

  public T getParent() {
    return parent;
  }

  public void setParent(final T parent) {
    this.parent = parent;
  }

  @Override
  public int compareTo(final Node o) {
    return getValue().compareTo(o.getValue());
  }

  @Override
  public String toString() {
    return value;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    final Node node = (Node) o;

    return Objects.equals(value, node.value);
  }

  @Override
  public int hashCode() {
    return value != null ? value.hashCode() : 0;
  }
}
