package com.kowsercse.algorithm.tree;

import java.util.*;

/**
 * @author kowsercse@gmail.com
 */
public class Trie {
  private Map<Character, Trie> children = new TreeMap<>();

  private int value;
  private final char symbol;

  public Trie(final char symbol) {
    this.symbol = symbol;
  }

  public void add(final String word) {
    Trie parent = this;
    for (int i = 0; i < word.length(); i++) {
      final char character = word.charAt(i);
      Trie child = parent.children.get(character);
      if (child == null) {
        child = new Trie(character);
        parent.children.put(character, child);
      }

      if (i == word.length() - 1) {
        child.value++;
      }
      parent = child;
    }
  }

  public void preOrder() {
    System.out.printf(" [%c:%d]->", symbol, value);
    System.out.printf("(");
    children.forEach((character, trie) -> trie.preOrder());
    System.out.printf(") ");
  }
}
