package com.kowsercse.algorithm.tree;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;

public interface Tree<T extends Node> {

  T root();

  T parent(T node);

  List<T> children(T node);

  default boolean isInternal(T node) {
    return !children(node).isEmpty();
  }

  default boolean isExternal(T node) {
    return children(node).isEmpty();
  }

  default boolean isRoot(T node) {
    return node.equals(root());
  }

  int size();

  default List<T> positions() {
    final List<T> nodes = new LinkedList<>();
    preOrderTraversal(nodes::add);
    return nodes;
  }

  default int depth(T v) {
    return isRoot(v) ? 0 : 1 + depth(parent(v));
  }

  default int height(T node) {
    return node == null
        ? 0
        : 1 + children(node).stream().map(this::height).max(Math::max).orElse(0);
  }

  void preOrderTraversal(final Consumer<T> consumer);

  void postOrderTraversal(final Consumer<T> consumer);

  void dfs(final Consumer<T> consumer);
}
