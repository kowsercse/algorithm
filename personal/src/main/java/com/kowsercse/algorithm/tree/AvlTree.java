package com.kowsercse.algorithm.tree;

import java.util.*;
import java.util.function.Consumer;

public class AvlTree extends AbstractTree<BinaryNode> {

  private BinaryNode root;

  public void insert(final String value) {
    final BinaryNode node = new BinaryNode(value);
    if (root == null) {
      root = node;
    } else {
      insert(root, node);
    }

    while (root.getParent() != null) {
      root = root.getParent();
    }
  }

  public void delete(final String value) {
    delete(root, value);
    if (root != null) {
      while (root.getParent() != null) {
        root = root.getParent();
      }
    }
  }

  private void delete(final BinaryNode node, final String value) {
    if (node == null) {
      return;
    }

    final int comparison = node.getValue().compareTo(value);
    if (comparison > 0) {
      delete(node.getLeft(), value);
      if (node.getRightHeight() - node.getLeftHeight() > 1) {
        final BinaryNode right = node.getRight();
        if (right.getLeftHeight() > right.getRightHeight()) {
          rotateRight(right);
        }
        rotateLeft(node);
      }
    } else if (comparison < 0) {
      delete(node.getRight(), value);
      if (node.getLeftHeight() - node.getRightHeight() > 1) {
        final BinaryNode left = node.getLeft();
        if (left.getRightHeight() > left.getLeftHeight()) {
          rotateLeft(left);
        }
        rotateRight(node);
      }
    } else {
      deleteUsingBstAlgorithm(node);
    }
  }

  private void deleteUsingBstAlgorithm(final BinaryNode node) {
    final BinaryNode parent = node.getParent();
    if (node.getLeft() != null && node.getRight() != null) {
      BinaryNode leftMax = node.getLeft();
      while (leftMax.getRight() != null) {
        leftMax = leftMax.getRight();
      }
      node.setValue(leftMax.getValue());
      deleteUsingBstAlgorithm(leftMax);
    } else {
      final BinaryNode singleChildOrNull =
          node.getLeft() != null ? node.getLeft() : node.getRight();
      if (parent == null) {
        root = singleChildOrNull;
        if (root != null) {
          root.setParent(null);
          root.updateHeight();
        }
      } else if (node.equals(parent.getLeft())) {
        parent.setLeft(singleChildOrNull);
        parent.updateHeight();
      } else if (node.equals(parent.getRight())) {
        parent.setRight(singleChildOrNull);
        parent.updateHeight();
      }
    }
  }

  private void insert(final BinaryNode parent, final BinaryNode node) {
    final BinaryNode left = parent.getLeft();
    final BinaryNode right = parent.getRight();

    final int comparison = parent.getValue().compareTo(node.getValue());
    if (comparison > 0) {
      if (left == null) {
        parent.setLeft(node);
        parent.updateHeight();
      } else {
        insert(left, node);
        parent.updateHeight();
      }

      if (parent.getLeftHeight() - parent.getRightHeight() > 1) {
        final int comparison2 = parent.getLeft().getValue().compareTo(node.getValue());
        if (comparison2 < 0) {
          rotateLeft(parent.getLeft());
        }
        rotateRight(parent);
      }
    } else if (comparison < 0) {
      if (right == null) {
        parent.setRight(node);
        parent.updateHeight();
      } else {
        insert(right, node);
        parent.updateHeight();
      }

      if (parent.getRightHeight() - parent.getLeftHeight() > 1) {
        final int comparison2 = parent.getRight().getValue().compareTo(node.getValue());
        if (comparison2 > 0) {
          rotateRight(parent.getRight());
        }
        rotateLeft(parent);
      }
    }
  }

  private void rotateLeft(final BinaryNode node) {
    final BinaryNode parent = node.getParent();
    final BinaryNode right = node.getRight();
    final BinaryNode b = right == null ? null : right.getLeft();

    updateParent(parent, node, right);
    if (right != null) {
      right.setLeft(node);
    }
    node.setRight(b);

    node.updateHeight();
    if (right != null) {
      right.updateHeight();
    }
    if (parent != null) {
      parent.updateHeight();
    }
  }

  private void rotateRight(final BinaryNode node) {
    final BinaryNode parent = node.getParent();
    final BinaryNode left = node.getLeft();
    final BinaryNode b = left == null ? null : left.getRight();

    updateParent(parent, node, left);
    if (left != null) {
      left.setRight(node);
    }
    node.setLeft(b);

    node.updateHeight();
    if (left != null) {
      left.updateHeight();
    }
    if (parent != null) {
      parent.updateHeight();
    }
  }

  private void updateParent(
      final BinaryNode parentParent, final BinaryNode currentParent, final BinaryNode newParent) {
    if (parentParent == null) {
      newParent.setParent(null);
      return;
    }

    if (currentParent.equals(parentParent.getLeft())) {
      parentParent.setLeft(newParent);
    } else {
      parentParent.setRight(newParent);
    }
  }

  public void inorderTraversal(final Consumer<BinaryNode> consumer) {
    inorderTraversal(root, consumer);
  }

  private void inorderTraversal(final BinaryNode node, final Consumer<BinaryNode> consumer) {
    if (node == null) {
      return;
    }

    inorderTraversal(node.getLeft(), consumer);
    consumer.accept(node);
    inorderTraversal(node.getRight(), consumer);
  }

  @Override
  public BinaryNode root() {
    return root;
  }

  @Override
  public BinaryNode parent(final BinaryNode node) {
    return node.getParent();
  }

  @Override
  public List<BinaryNode> children(final BinaryNode node) {
    return node.children();
  }

  @Override
  public int size() {
    List<BinaryNode> nodes = new ArrayList<>();
    inorderTraversal(nodes::add);
    return nodes.size();
  }

  @Override
  public int height(final BinaryNode node) {
    return node == null ? 0 : 1 + Math.max(height(node.getLeft()), height(node.getRight()));
  }

  @Override
  public boolean isInternal(final BinaryNode node) {
    return node.getLeft() != null || node.getRight() != null;
  }

  @Override
  public boolean isExternal(final BinaryNode node) {
    return !isInternal(node);
  }

  public void printEulerTour() {
    if (root == null) {
      return;
    }
    final Consumer<BinaryNode> eulerTourConsumer =
        new Consumer<BinaryNode>() {
          @Override
          public void accept(final BinaryNode node) {
            if (isExternal(node)) {
              System.out.print(node);
            } else {
              System.out.print(" ( ");
              if (node.getLeft() != null) {
                accept(node.getLeft());
              }
              System.out.printf(" [%s] ", node);
              if (node.getRight() != null) {
                accept(node.getRight());
              }
              System.out.print(" ) ");
            }
          }
        };

    eulerTourConsumer.accept(root());
  }

  public boolean isBst() {
    return isBst(root, BinaryNode.MIN, BinaryNode.MAX);
  }

  private boolean isBst(final BinaryNode node, final BinaryNode min, final BinaryNode max) {
    if (node == null) return true;
    if (min.compareTo(node) > 0 || max.compareTo(node) < 0) return false;
    return isBst(node.getLeft(), min, node) && isBst(node.getRight(), node, max);
  }

  public BinaryNode leastCommonAncestor(final String a, final String b) {
    List<BinaryNode> aAncestors = getAncestors(a);
    List<BinaryNode> bAncestors = getAncestors(b);
    final int searchLength = Math.min(aAncestors.size(), bAncestors.size());

    BinaryNode lca = null;
    for (int i = 0; i < searchLength; i++) {
      if (aAncestors.get(i).compareTo(bAncestors.get(i)) == 0) {
        lca = aAncestors.get(i);
      } else {
        return lca;
      }
    }

    return lca;
  }

  private List<BinaryNode> getAncestors(final String a) {
    List<BinaryNode> ancestors = new ArrayList<>();
    BinaryNode node = this.root;
    while (node != null) {
      ancestors.add(node);
      final int comparison = a.compareTo(node.getValue());
      if (comparison < 0) {
        node = node.getLeft();
      } else if (comparison > 0) {
        node = node.getRight();
      } else {
        break;
      }
    }
    return ancestors;
  }

  public String ceil(final String value) {
    BinaryNode min = BinaryNode.MAX;
    BinaryNode node = this.root;

    while (node != null) {
      final int comparison = value.compareTo(node.getValue());
      if (comparison == 0) {
        return value;
      } else {
        if (min.compareTo(node) > 0 && comparison < 0) {
          min = node;
        }

        node = comparison < 0 ? node.getLeft() : node.getRight();
      }
    }

    return min == BinaryNode.MAX ? null : min.getValue();
  }

  public String floor(final String value) {
    BinaryNode max = BinaryNode.MIN;
    BinaryNode node = root;

    while (node != null) {
      final int comparison = value.compareTo(node.getValue());
      if (comparison == 0) {
        return value;
      } else {
        if (max.compareTo(node) < 0 && comparison > 0) {
          max = node;
        }
        node = comparison < 0 ? node.getLeft() : node.getRight();
      }
    }

    return max != BinaryNode.MIN ? max.getValue() : null;
  }

  public ArrayList<BinaryNode> rangeSearch(final String key1, final String key2) {
    final BinaryNode lca = leastCommonAncestor(key1, key2);

    final BinaryNode min = new BinaryNode(key1);
    final BinaryNode max = new BinaryNode(key2);
    final ArrayList<BinaryNode> binaryNodes = new ArrayList<>();
    final Consumer<BinaryNode> consumer = binaryNodes::add;
    rangeSearchLeft(lca.getLeft(), min, max, consumer);
    consumer.accept(lca);
    rangeSearchRight(lca.getRight(), min, max, consumer);
    return binaryNodes;
  }

  private void rangeSearchLeft(
      final BinaryNode node,
      final BinaryNode min,
      final BinaryNode max,
      final Consumer<BinaryNode> consumer) {
    if (node == null) {
      return;
    }

    final int comparison = min.compareTo(node);
    if (comparison < 0) {
      rangeSearchLeft(node.getLeft(), min, max, consumer);
    }
    if (comparison <= 0 && max.compareTo(node) >= 0) {
      consumer.accept(node);
    }
    rangeSearchLeft(node.getRight(), min, max, consumer);
  }

  private void rangeSearchRight(
      final BinaryNode node,
      final BinaryNode min,
      final BinaryNode max,
      final Consumer<BinaryNode> consumer) {
    if (node == null) {
      return;
    }

    rangeSearchRight(node.getLeft(), min, max, consumer);
    final int comparison = max.compareTo(node);
    if (min.compareTo(node) <= 0 && comparison >= 0) {
      consumer.accept(node);
    }
    if (comparison > 0) {
      rangeSearchRight(node.getRight(), min, max, consumer);
    }
  }
}
