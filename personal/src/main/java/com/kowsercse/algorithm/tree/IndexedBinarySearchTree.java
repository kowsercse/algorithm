package com.kowsercse.algorithm.tree;

public class IndexedBinarySearchTree extends IndexedBinaryTree {

  public void insert(final Node node) {
    final Node parent = root();
    if (parent == null) {
      add(0, node);
    } else {
      insert(parent, node);
    }
  }

  private void insert(final Node parent, final Node node) {
    if (parent.getValue().compareTo(node.getValue()) > 0) {
      final Node leftChild = leftChild(parent);
      if (leftChild == null) {
        final Integer position = getPosition(parent);
        add((position << 1) + 1, node);
      } else {
        insert(leftChild, node);
      }
    } else {
      final Node rightChild = rightChild(parent);
      if (rightChild == null) {
        final Integer position = getPosition(parent);
        add((position << 1) + 2, node);
      } else {
        insert(rightChild, node);
      }
    }
  }
}
