package com.kowsercse.algorithm.tree;

import java.util.Iterator;
import java.util.Stack;

/**
 * @author kowsercse@gmail.com
 */
public class IterativeBst implements Iterable<Integer> {

  private Node root;

  public void add(final int value) {
    if (root == null) {
      root = new Node(value);
    } else {
      root.add(value);
    }
  }

  @Override
  public BstIterator iterator() {
    return new BstIterator();
  }

  public class BstIterator implements Iterator<Integer> {
    private Stack<Processing> stack = new Stack<>();

    public BstIterator() {
      stack.push(new Processing(root));
    }

    @Override
    public boolean hasNext() {
      return !stack.isEmpty();
    }

    public Integer peek() {
      populateStack();
      return hasNext() ? stack.peek().node.value : null;
    }

    @Override
    public Integer next() {
      populateStack();
      final Processing pop = stack.pop();
      return pop.node.value;
    }

    private void populateStack() {
      final Processing processing = stack.pop();
      if (!processing.isProcessed && !processing.node.isEmpty()) {
        if (processing.node.right != null) {
          stack.push(new Processing(processing.node.right));
        }
        processing.isProcessed = true;
        stack.push(processing);

        if (processing.node.left != null) {
          stack.push(new Processing(processing.node.left));
        }
        populateStack();
      } else {
        processing.isProcessed = true;
        stack.push(processing);
      }
    }

    private class Processing {
      Node node;
      boolean isProcessed;

      public Processing(final Node node) {
        this.node = node;
        isProcessed = false;
      }
    }
  }

  private static class Node {
    Node left;
    Node right;
    int value;

    public Node(final int value) {
      this.value = value;
    }

    private void add(final int value) {
      if (value < this.value) {
        if (left == null) {
          left = new Node(value);
        } else {
          left.add(value);
        }
      } else if (value > this.value) {
        if (right == null) {
          right = new Node(value);
        } else {
          right.add(value);
        }
      }
    }

    public boolean isEmpty() {
      return left == null && right == null;
    }
  }
}
