package com.kowsercse.algorithm.tree;

import java.util.*;
import java.util.function.Consumer;

public class IndexedBinaryTree extends AbstractTree<Node> {

  private Map<Node, Integer> nodeToIndex = new HashMap<>();
  private Map<Integer, Node> nodes = new HashMap<>();

  void add(final int position, final Node node) {
    nodeToIndex.put(node, position);
    nodes.put(position, node);
  }

  public Node leftChild(final Node node) {
    final Integer position = getPosition(node);
    return nodes.get((position << 1) + 1);
  }

  public Node rightChild(final Node node) {
    final Integer position = getPosition(node);
    return nodes.get((position << 1) + 2);
  }

  public Node sibling(final Node node) {
    final Node parent = parent(node);
    final Node leftChild = leftChild(parent);
    final Node rightChild = rightChild(parent);
    return node.equals(leftChild) ? rightChild : leftChild;
  }

  public void inorderTraversal(final Consumer<Node> consumer) {
    final Consumer<Node> inOrderConsumer =
        new Consumer<Node>() {
          @Override
          public void accept(final Node node) {
            final Node leftChild = leftChild(node);
            final Node rightChild = rightChild(node);

            if (leftChild != null) {
              accept(leftChild);
            }
            consumer.accept(node);
            if (rightChild != null) {
              accept(rightChild);
            }
          }
        };

    inOrderConsumer.accept(root());
  }

  public void eulerTour(final Consumer<Node> consumer) {
    final Consumer<Node> eulerTourConsumer =
        new Consumer<Node>() {
          @Override
          public void accept(final Node node) {
            consumer.accept(node);

            if (isInternal(node)) {
              final Node leftChild = leftChild(node);
              if (leftChild != null) {
                accept(leftChild);
              }
              consumer.accept(node);

              final Node rightChild = rightChild(node);
              if (rightChild != null) {
                accept(rightChild);
              }
              consumer.accept(node);
            }
          }
        };

    eulerTourConsumer.accept(root());
  }

  @Override
  public Node root() {
    return nodes.get(0);
  }

  @Override
  public Node parent(final Node node) {
    final int parentPosition = getPosition(node) >> 1;
    return nodes.get(parentPosition);
  }

  @Override
  public int size() {
    return nodes.size();
  }

  @Override
  public List<Node> children(final Node node) {
    List<Node> children = new ArrayList<>();
    final Node leftChild = leftChild(node);
    final Node rightChild = rightChild(node);
    if (leftChild != null) {
      children.add(leftChild);
    }
    if (rightChild != null) {
      children.add(rightChild);
    }
    return children;
  }

  @Override
  public int height(final Node node) {
    return node == null ? 0 : 1 + Math.max(height(leftChild(node)), height(rightChild(node)));
  }

  @Override
  public boolean isInternal(final Node node) {
    return leftChild(node) != null || rightChild(node) != null;
  }

  @Override
  public boolean isExternal(final Node node) {
    return !isInternal(node);
  }

  protected Integer getPosition(final Node node) {
    return nodeToIndex.get(node);
  }
}
