package com.kowsercse.algorithm.tree;

import java.util.function.Consumer;

public abstract class AbstractTree<T extends Node> implements Tree<T> {

  public void preOrderTraversal(final Consumer<T> consumer) {
    if (root() == null) {
      return;
    }
    internalPreOrder(root(), consumer);
  }

  public void postOrderTraversal(final Consumer<T> consumer) {
    if (root() == null) {
      return;
    }
    internalPostOrder(root(), consumer);
  }

  public void dfs(final Consumer<T> consumer) {
    if (root() == null) {
      return;
    }
    dfsInternal(root(), consumer);
  }

  private void internalPreOrder(final T node, final Consumer<T> consumer) {
    consumer.accept(node);
    children(node).forEach(child -> internalPreOrder(child, consumer));
  }

  private void internalPostOrder(final T node, final Consumer<T> consumer) {
    children(node).forEach(child -> internalPostOrder(child, consumer));
    consumer.accept(node);
  }

  private void dfsInternal(final T node, final Consumer<T> consumer) {
    consumer.accept(node);
    children(node).forEach(child -> dfsInternal(child, consumer));
  }
}
