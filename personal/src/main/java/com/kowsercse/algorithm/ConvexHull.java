package com.kowsercse.algorithm;

import java.util.*;

public class ConvexHull {

  private List<Point> points = new ArrayList<>();

  public Stack<Point> calculate() {
    Collections.sort(points, Point.Y_AXIS_COMPARATOR);
    final Point lowestRight = points.get(0);
    final Comparator<Point> counterClockwiseComparator =
        (left, right) -> {
          final int compare = lowestRight.ccw(left, right);
          if (compare == 0) {
            if (left.x == right.x) {
              return Integer.compare(left.y, right.y);
            } else if (left.y == right.y) {
              return Integer.compare(left.x, right.x);
            }
          }
          return compare;
        };
    Collections.sort(points.subList(1, points.size()), counterClockwiseComparator);

    points.add(lowestRight);
    return grahamScan();
  }

  private Stack<Point> grahamScan() {
    Stack<Point> hull = new Stack<>();
    hull.push(points.get(0));
    hull.push(points.get(1));

    for (int i = 2; i < points.size(); i++) {
      Point top = hull.pop();
      final Point ithPoint = points.get(i);
      while (!hull.isEmpty() && Point.ccw(hull.peek(), top, ithPoint) <= 0) {
        top = hull.pop();
      }

      hull.push(top);
      hull.push(ithPoint);
    }
    return hull;
  }

  public void add(final int x, final int y) {
    points.add(new Point(x, y));
  }
}
