package com.kowsercse.algorithm;

import java.util.Arrays;
import java.util.List;

/**
 * @author kowsercse@gmail.com
 */
public class AdHocProblem {

  public int getMedian(final int[] first, final int[] second) {
    final int nth = (first.length + second.length) / 2 - 2;

    int i = 0;
    int j = 0;
    while (i + j != nth) {
      if (first[i] < second[j]) {
        i++;
      } else if (first[i] >= second[j]) {
        j++;
      }
    }
    System.out.println("i = " + i + " " + first[i]);
    System.out.println("j = " + j + " " + second[j]);

    return 0;
  }

  private static boolean isSumOfTwoNumber(final int[] numbers, final int expectedSum) {
    Arrays.sort(numbers);

    int start = 0;
    int end = numbers.length - 1;

    while (start < end) {
      final int sum = numbers[start] + numbers[end];

      if (sum > expectedSum) {
        end--;
      } else if (sum < expectedSum) {
        start++;
      } else {
        return sum == expectedSum;
      }
    }

    return false;
  }

  public Node constructTree(final List<Character> preOrder, final List<Character> inOrder) {
    if (preOrder.size() == 0) {
      return null;
    }

    final Node node = new Node();
    node.value = preOrder.get(0);

    int i = 0;
    for (final Character character : inOrder) {
      if (node.value == character) {
        break;
      }
      i++;
    }

    node.left = constructTree(preOrder.subList(1, 1 + i), inOrder.subList(0, i));
    node.right =
        constructTree(
            preOrder.subList(1 + i, preOrder.size()), inOrder.subList(i + 1, inOrder.size()));

    return node;
  }

  public static class Node {
    private Node left;
    private Node right;
    private Character value;

    public void inOrder() {
      System.out.printf("( ");
      if (left != null) {
        left.inOrder();
      }
      System.out.printf("" + value);
      if (right != null) {
        right.inOrder();
      }
      System.out.printf(" )");
    }
  }
}
