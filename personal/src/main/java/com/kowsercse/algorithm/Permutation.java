package com.kowsercse.algorithm;

import static java.util.stream.Collectors.toUnmodifiableList;

import java.util.*;

/**
 * Inspired by http://stackoverflow.com/a/24257996/511736
 */
public class Permutation {

  public static List<String> allPermutation(String symbols) {
    Map<Character, Integer> map = new TreeMap<>();
    for (char c : symbols.toCharArray()) {
      map.compute(c, (key, value) -> value == null ? 1 : value + 1);
    }
    char[] chars = new char[map.size()];
    int[] count = new int[map.size()];
    int i = 0;
    for (Map.Entry<Character, Integer> entry : map.entrySet()) {
      chars[i] = entry.getKey();
      count[i] = entry.getValue();
      i++;
    }

    List<String> strings = new LinkedList<>();
    permute(chars, count, new char[symbols.length()], 0, strings);
    return strings;
  }

  private static void permute(
      char[] chars, int[] count, char[] result, int depth, List<String> strings) {
    if (depth == result.length) {
      strings.add(new String(result));
      return;
    }

    for (int i = 0; i < chars.length; i++) {
      if (count[i] > 0) {
        count[i]--;
        result[depth] = chars[i];
        permute(chars, count, result, depth + 1, strings);
        count[i]++;
      }
    }
  }

  public static String nextPermutation(String permutation) {
    if (permutation == null || permutation.length() == 1) {
      return permutation;
    }

    char[] chars = permutation.toCharArray();
    int pivotIndex = chars.length - 1;
    while (pivotIndex > 1 && chars[pivotIndex] < chars[pivotIndex - 1]) {
      pivotIndex--;
    }
    pivotIndex--;

    int index = chars.length - 1;
    while (chars[index] < chars[pivotIndex]) {
      index--;
    }
    swap(chars, index, pivotIndex);
    reverse(chars, pivotIndex + 1, chars.length - 1);

    return new String(chars);
  }

  private static void reverse(char[] chars, int start, int end) {
    while (start < end) {
      swap(chars, start++, end--);
    }
  }

  private static void swap(char[] chars, int left, int right) {
    char temp = chars[left];
    chars[left] = chars[right];
    chars[right] = temp;
  }

  static int pack(final List<Integer> digits, final List<Integer> radix) {
    int n = 0;
    for (int i = 0; i < digits.size(); i++) {
      n = n * radix.get(i) + digits.get(i);
    }
    return n;
  }

  public static List<Integer> unpack(int n, final List<Integer> radix) {
    final LinkedList<Integer> digits = new LinkedList<>();
    for (int i = radix.size() - 1; i >= 0; i--) {
      digits.addFirst(n % radix.get(i));
      n = Math.floorDiv(n, radix.get(i));
    }

    return digits;
  }

  public static List<Integer> nthPermutation(
      final List<Integer> digits, final int n, final int permutationLength) {
    final ArrayList<Integer> digitsInternal = new ArrayList<>(digits);
    final List<Integer> radix = new ArrayList<>();
    for (int i = 0; i < permutationLength; i++) {
      radix.add(digitsInternal.size() - i);
    }

    return unpack(n, radix).stream()
        .map(i -> digitsInternal.remove(i.intValue()))
        .toList();
  }

  public static List<Integer> nthPermutation(List<Integer> integers, int i) {
    return nthPermutation(integers, i, integers.size());
  }

  public static List<Integer> nextPermutation(
      final List<Integer> digits, final List<Integer> permutation) {
    final ArrayList<Integer> internalDigits = new ArrayList<>(digits);
    final List<Integer> radix = new ArrayList<>();
    for (int i = 0; i < permutation.size(); i++) {
      radix.add(internalDigits.size() - i);
    }

    final ArrayList<Integer> unpackedDigits = new ArrayList<>();
    permutation.forEach(
        digit -> {
          unpackedDigits.add(internalDigits.indexOf(digit));
          internalDigits.remove(digit);
        });

    final int pack = pack(unpackedDigits, radix);
    return nthPermutation(digits, pack + 1, permutation.size());
  }

  public static void printAllPermutation(int n) {
    int[] digits = new int[n];
    for (int i = 0; i < n; i++) {
      digits[i] = i + 1;
    }

    allPermutation(digits, 0, n);
  }

  static void allPermutation(int[] digits, int left, int right) {
    if (left == right) {
      System.out.println(Arrays.toString(digits));
      return;
    }

    for (int i = left; i < right; i++) {
      swap(digits, i, left);
      allPermutation(digits, left + 1, right);
      swap(digits, i, left);
    }
  }

  private static void swap(int[] digits, int left, int right) {
    int temp = digits[left];
    digits[left] = digits[right];
    digits[right] = temp;
  }
}
