package com.kowsercse.algorithm;

import java.util.LinkedList;
import java.util.List;

public class Combination {
  private final List<String> result = new LinkedList<>();
  private final char[] symbols;
  private final int length;

  private final char[] temporary;

  public Combination(char[] symbols, int length) {
    this.symbols = symbols;
    this.length = length;
    temporary = new char[length];
  }

  public List<String> getResult() {
    return result;
  }

  public void calculate() {
    dfs(0, 0);
  }

  private void dfs(int depth, int start) {
    if (depth == length) {
      result.add(new String(temporary));
      return;
    }
    for (int i = start; i <= symbols.length - length + depth; i++) {
      temporary[depth] = symbols[i];
      dfs(depth + 1, i + 1);
    }
  }
}
