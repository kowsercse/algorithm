package com.kowsercse.algorithm.simple;

import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

public class BinaryNode {
  private BinaryNode left;
  private BinaryNode right;
  private final int value;

  public int getValue() {
    return value;
  }

  public BinaryNode(int value) {
    this.value = value;
  }

  public BinaryNode(int value, BinaryNode left, BinaryNode right) {
    this.left = left;
    this.right = right;
    this.value = value;
  }

  public void inOrder(Consumer<BinaryNode> consumer) {
    Optional.ofNullable(left).ifPresent(binaryNode -> binaryNode.inOrder(consumer));
    consumer.accept(this);
    Optional.ofNullable(right).ifPresent(binaryNode -> binaryNode.inOrder(consumer));
  }

  public void preOrder() {
    Optional.ofNullable(left).ifPresent(BinaryNode::preOrder);
    Optional.ofNullable(right).ifPresent(BinaryNode::preOrder);
    System.out.println(value);
  }

  public void postOrder() {
    Optional.ofNullable(left).ifPresent(BinaryNode::postOrder);
    Optional.ofNullable(right).ifPresent(BinaryNode::postOrder);
    System.out.println(value);
  }

  public static BinaryNode fromPreOrder(List<Integer> inOrder, List<Integer> preOrder) {
    if (inOrder.size() == 0 || preOrder.size() == 0) {
      return null;
    }

    Integer value = preOrder.get(0);
    int splitIndex = inOrder.indexOf(value);

    List<Integer> leftInOrder = inOrder.subList(0, splitIndex);
    List<Integer> rightInOrder = inOrder.subList(splitIndex + 1, inOrder.size());
    return new BinaryNode(
        value,
        fromPreOrder(leftInOrder, preOrder.subList(1, 1 + leftInOrder.size())),
        fromPreOrder(rightInOrder, preOrder.subList(1 + rightInOrder.size(), preOrder.size())));
  }

  public static BinaryNode fromPostOrder(List<Integer> inOrder, List<Integer> postOrder) {
    if (inOrder.size() == 0 || postOrder.size() == 0) {
      return null;
    }

    Integer value = postOrder.get(postOrder.size() - 1);
    int splitIndex = inOrder.indexOf(value);

    List<Integer> leftInOrder = inOrder.subList(0, splitIndex);
    List<Integer> rightInOrder = inOrder.subList(splitIndex + 1, inOrder.size());
    return new BinaryNode(
        value,
        fromPostOrder(leftInOrder, postOrder.subList(0, leftInOrder.size())),
        fromPostOrder(rightInOrder, postOrder.subList(leftInOrder.size(), postOrder.size() - 1)));
  }
}
