package com.kowsercse.algorithm;

import java.util.LinkedList;
import java.util.Random;

public class StableMarriageProblem {
  private final Random random = new Random();

  private final int n;
  private final Integer[][] manPreference;
  private final Integer[][] womanPreference;
  private final Integer[] womanMarriedTo;

  public StableMarriageProblem(final int n) {
    this.n = n;
    manPreference = new Integer[n][];
    womanPreference = new Integer[n][];

    womanMarriedTo = new Integer[n];
  }

  private void apply() {
    int[] nextToPropose = new int[n];
    final LinkedList<Integer> unmarriedMan = new LinkedList<>();
    for (int i = 0; i < n; i++) {
      unmarriedMan.add(i);
    }

    int total = 0;

    while (!unmarriedMan.isEmpty()) {
      total++;
      final Integer theMan = unmarriedMan.remove();
      final Integer nextWomanToPropose = manPreference[theMan][nextToPropose[theMan]];
      nextToPropose[theMan]++;

      final Integer marriedToMan = womanMarriedTo[nextWomanToPropose];
      if (marriedToMan == null) {
        womanMarriedTo[nextWomanToPropose] = theMan;
      } else if (prefers(nextWomanToPropose, theMan, marriedToMan)) {
        unmarriedMan.add(marriedToMan);
        womanMarriedTo[nextWomanToPropose] = theMan;
      } else {
        unmarriedMan.add(theMan);
      }
    }

    System.out.println("Iteration: " + total);
  }

  private boolean prefers(int woman, int newMan, int marriedToMan) {
    for (int i = 0; i < n; i++) {
      int pref = womanPreference[woman][i];
      if (pref == newMan) {
        return true;
      } else if (pref == marriedToMan) {
        return false;
      }
    }
    return false;
  }

  private void initialize() {
    for (int i = 0; i < n; i++) {
      manPreference[i] = createRandomPrefs();
      womanPreference[i] = createRandomPrefs();
    }
  }

  private Integer[] createRandomPrefs() {
    Integer[] preference = new Integer[n];
    for (int choice = 0; choice < preference.length; choice++) {
      preference[choice] = choice;
    }
    for (int choice = preference.length - 1; choice > 0; choice--) {
      int j = random.nextInt(choice + 1);
      int temp = preference[choice];
      preference[choice] = preference[j];
      preference[j] = temp;
    }

    return preference;
  }

  private void print() {
    System.out.println("Man Preferences:");
    printPreferences(manPreference);

    System.out.println("Woman Preferences:");
    printPreferences(womanPreference);

    printMarriage();
  }

  private void printMarriage() {
    System.out.println("Couples (woman + man): ");
    for (int i = 0; i < womanMarriedTo.length; i++) {
      System.out.println((i + 1) + " + " + (womanMarriedTo[i] + 1));
    }
  }

  private void printPreferences(Integer[][] preference) {
    for (int i = 0; i < preference.length; i++) {
      for (int j = 0; j < preference[i].length; j++) {
        System.out.print((preference[i][j] + 1) + " ");
      }
      System.out.println();
    }
  }

  public static void main(String[] args) {
    final StableMarriageProblem stableMarriageProblem = new StableMarriageProblem(3);
    stableMarriageProblem.initialize();
    stableMarriageProblem.apply();
    stableMarriageProblem.print();
  }
}
