package com.kowsercse.algorithm;

import java.util.Comparator;

public class Point {
  static final Comparator<Point> Y_AXIS_COMPARATOR =
      (left, right) ->
          left.equals(right)
              ? 0
              : left.y < right.y || (left.y == right.y && left.x > right.x) ? -1 : 1;

  int x;
  int y;

  public Point(final int x, final int y) {
    this.x = x;
    this.y = y;
  }

  public int ccw(final Point left, final Point right) {
    return (right.x - x) * (left.y - y) - (right.y - y) * (left.x - x);
  }

  public static int ccw(Point a, Point b, Point c) {
    final int area = (b.x - a.x) * (c.y - a.y) - (b.y - a.y) * (c.x - a.x);
    return Integer.compare(area, 0);
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) return true;
    if (!(o instanceof Point)) return false;

    final Point point = (Point) o;

    return x == point.x && y == point.y;
  }

  @Override
  public int hashCode() {
    return 31 * x + y;
  }

  @Override
  public String toString() {
    return "" + x + " " + y;
  }
}
