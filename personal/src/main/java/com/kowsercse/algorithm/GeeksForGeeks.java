package com.kowsercse.algorithm;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author kowsercse@gmail.com
 */
public class GeeksForGeeks {

  /**
   * @see <a
   *     href="http://www.geeksforgeeks.org/minimum-number-platforms-required-railwaybus-station/">
   *     http://www.geeksforgeeks.org/minimum-number-platforms-required-railwaybus-station/</a>
   */
  public int findPlatform(final int[] arrival, final int[] departure) {
    int i = 0;
    int j = 0;
    int totalPlatform = 0;
    int maxPlatform = 0;

    while (i < arrival.length) {
      if (arrival[i] < departure[j]) {
        totalPlatform++;
        maxPlatform = Math.max(totalPlatform, maxPlatform);
        i++;
      } else if (arrival[i] > departure[j]) {
        totalPlatform--;
        j++;
      }
    }

    return maxPlatform;
  }

  /**
   * @see <a
   *     href="http://www.geeksforgeeks.org/construct-a-special-tree-from-given-preorder-traversal/">
   *     http://www.geeksforgeeks.org/construct-a-special-tree-from-given-preorder-traversal/</a>
   */
  public Node constructTree(final int[] preOrder, final char[] isLeaf) {
    return constructTree(preOrder, isLeaf, new AtomicInteger(0), preOrder.length);
  }

  private Node constructTree(
      final int[] preOrder, final char[] isLeaf, final AtomicInteger index, final int length) {
    final Node node = new Node(preOrder[index.get()]);

    if (isLeaf[index.get()] == 'N') {
      index.incrementAndGet();
      node.left = constructTree(preOrder, isLeaf, index, length);
      index.incrementAndGet();
      node.right = constructTree(preOrder, isLeaf, index, length);
    }

    return node;
  }

  public class Node {
    Node left, right;
    int value;

    public Node(final int value) {
      this.value = value;
    }

    public void inOrder() {
      System.out.printf(" { ");
      if (left != null) {
        left.inOrder();
      }
      System.out.print(value);
      if (right != null) {
        right.inOrder();
      }
      System.out.printf(" } ");
    }
  }
}
