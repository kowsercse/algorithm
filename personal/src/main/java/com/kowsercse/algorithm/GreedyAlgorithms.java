package com.kowsercse.algorithm;

import java.util.Arrays;
import java.util.LinkedList;

/**
 * @author kowsercse@gmail.com
 */
public class GreedyAlgorithms {

  public LinkedList<Integer[]> mergeIntervals(final int[][] intervals) {
    Arrays.sort(intervals, (left, right) -> Integer.compare(left[0], right[0]));

    LinkedList<Integer[]> mergedIntervals = new LinkedList<>();
    mergedIntervals.push(new Integer[] {intervals[0][0], intervals[0][1]});
    for (int i = 1; i < intervals.length; i++) {
      final Integer[] lastInterval = mergedIntervals.getLast();
      final int[] currentInterval = intervals[i];
      if (currentInterval[0] > lastInterval[1]) {
        mergedIntervals.add(new Integer[] {currentInterval[0], currentInterval[1]});
      } else {
        lastInterval[1] = currentInterval[1];
      }
    }

    return mergedIntervals;
  }

  public boolean isSumOfTwoNumber(final int[] numbers, final int expectedSum) {
    Arrays.sort(numbers);

    int start = 0;
    int end = numbers.length - 1;

    while (start < end) {
      final int sum = numbers[start] + numbers[end];

      if (sum > expectedSum) {
        end--;
      } else if (sum < expectedSum) {
        start++;
      } else {
        return sum == expectedSum;
      }
    }

    return false;
  }
}
