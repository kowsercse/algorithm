package com.kowsercse.algorithm;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class WeightedUnionFindPathCompression {
  private Map<String, String> roots = new HashMap<>();
  private Map<String, Integer> sizes = new HashMap<>();
  private Set<String> nodes = new HashSet<>();

  public void union(String node1, String node2) {
    final String root1 = getRoot(node1);
    final String root2 = getRoot(node2);
    if (root1.equals(root2)) {
      return;
    }

    final Integer size1 = sizes.get(root1);
    final Integer size2 = sizes.get(root2);
    if (size1 < size2) {
      roots.put(root1, root2);
      sizes.put(root2, size1 + size2);
    } else {
      roots.put(root2, root1);
      sizes.put(root1, size1 + size2);
    }
  }

  public String getRoot(String name) {
    String root = roots.get(name);
    while (!name.equals(root)) {
      roots.put(name, roots.get(root));
      name = root;
      root = roots.get(name);
    }
    return root;
  }

  public int getSize(final String name) {
    final String root = getRoot(name);
    return sizes.get(root);
  }

  public void addRoot(final String name) {
    if (roots.get(name) == null) {
      roots.put(name, name);
      sizes.put(name, 1);
      nodes.add(name);
    }
  }
}
