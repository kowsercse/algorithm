package com.kowsercse.algorithm;

import java.util.*;

/**
 * @author kowsercse@gmail.com
 */
public class Josephus {
  public int solve(final int n, final int skip) {
    LinkedList<Integer> persons = new LinkedList<>();
    for (int i = 0; i < n; i++) {
      persons.add(i);
    }
    while (persons.size() >= skip) {
      final Iterator<Integer> personIterator = persons.iterator();
      LinkedList<Integer> temp = new LinkedList<>();

      final int I = persons.size() / skip;
      for (int i = 0; i < I; i++) {
        for (int j = 0; j < skip - 1; j++) {
          temp.add(personIterator.next());
        }
        personIterator.next();
      }
      final int reminder = persons.size() % skip;
      for (int i = 0; i < reminder; i++) {
        temp.addFirst(persons.removeLast());
      }

      persons = temp;
    }

    while (persons.size() != 1) {
      final int removeIndex = skip % persons.size();
      if (removeIndex == 0) {
        persons.removeLast();
      } else {
        persons.remove(removeIndex - 1);
      }
    }
    return persons.getFirst() + 1;
  }

  int josephus(int n, int k) {
    if (n == 1) return 1;
    else
    /* The position returned by josephus(n - 1, k) is adjusted because the
    recursive call josephus(n - 1, k) considers the original position
    k%n + 1 as position 1 */ {
      final int joseph = (josephus(n - 1, k) + k - 1) % n + 1;
      System.out.println(n + " " + k + " - " + joseph);
      return joseph;
    }
  }

  public static void main(String[] args) {
    final Josephus josephus = new Josephus();
    final int n = 14;
    final int k = 6;
    final int solution = josephus.solve(n, k);
    josephus.josephus(n, k);
    System.out.println("solution = " + solution);
  }
}
