package com.kowsercse.algorithm.dp;

import java.util.HashSet;
import java.util.Set;

public class PerfectSquares {
  private static final int MAX = 10_000;
  Set<Integer> squares = prepareSquares();
  int[] perfectSquares = new int[MAX + 1];

  public int numSquares(int n) {
    if (perfectSquares[n] != 0) {
      return perfectSquares[n];
    }

    perfectSquares[n] = calculatePerfectSquare(n);
    return perfectSquares[n];
  }

  int calculatePerfectSquare(int number) {
    if (perfectSquares[number] != 0) {
      return perfectSquares[number];
    }
    if (squares.contains(number)) {
      perfectSquares[number] = 1;
      return perfectSquares[number];
    }

    perfectSquares[number] = Integer.MAX_VALUE;
    for (int i = 1; i <= number / 2; i++) {
      perfectSquares[number] =
          Math.min(
              perfectSquares[number],
              calculatePerfectSquare(i) + calculatePerfectSquare(number - i));
    }
    return perfectSquares[number];
  }

  Set<Integer> prepareSquares() {
    Set<Integer> squares = new HashSet<>();
    int i = 1;
    while (i * i <= MAX) {
      squares.add(i * i);
      i++;
    }

    return squares;
  }
}
