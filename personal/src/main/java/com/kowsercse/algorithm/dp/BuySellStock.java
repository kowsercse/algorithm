package com.kowsercse.algorithm.dp;

import static com.kowsercse.util.Utils.printTable;
import static java.lang.Integer.*;

import java.util.List;

class BuySellStock {
  static int maxProfit(List<Integer> price) {
    int lowestSellPrice = price.get(0);
    int maxProfit = 0;
    for (int day = 1; day < price.size(); day++) {
      Integer todayPrice = price.get(day);
      maxProfit = max(maxProfit, todayPrice - lowestSellPrice);
      lowestSellPrice = min(lowestSellPrice, todayPrice);
    }
    return maxProfit;
  }

  static int maxProfitForTransactionVersionOne(int transaction, List<Integer> price) {
    int[][] profitTable = new int[transaction + 1][price.size()];
    for (int t = 0; t < transaction; t++) {
      profitTable[t][0] = 0; // no profit on first day
    }
    for (int d = 0; d < price.size(); d++) {
      profitTable[0][d] = 0; // no profit for 0 transaction
    }
    for (int t = 1; t <= transaction; t++) {
      for (int d = 1; d < price.size(); d++) {
        int excludedProfit = profitTable[t][d - 1];
        int includedProfit = MIN_VALUE;
        for (int j = 0; j < d; j++) {
          includedProfit = max(includedProfit, price.get(d) + profitTable[t - 1][j] - price.get(j));
        }
        profitTable[t][d] = max(includedProfit, excludedProfit);
      }
    }

    System.out.println("");
    printTable(profitTable);
    System.out.println("");
    return profitTable[transaction][price.size() - 1];
  }

  static int maxProfitForTransactionOptimized(int transaction, List<Integer> price) {
    int[][] profitTable = new int[transaction + 1][price.size()];
    for (int t = 0; t < transaction; t++) {
      profitTable[t][0] = 0; // no profit on first day
    }
    for (int d = 0; d < price.size(); d++) {
      profitTable[0][d] = 0; // no profit for 0 transaction
    }

    for (int t = 1; t <= transaction; t++) {
      int maxDiff = 0 - price.get(0);
      for (int sellDay = 1; sellDay < price.size(); sellDay++) {
        maxDiff = max(maxDiff, profitTable[t - 1][sellDay] - price.get(sellDay));
        profitTable[t][sellDay] = max(profitTable[t][sellDay - 1], maxDiff + price.get(sellDay));
      }
    }

    return profitTable[transaction][price.size() - 1];
  }
}
