package com.kowsercse.algorithm.dp;

import static com.kowsercse.util.Utils.printTable;

import java.util.List;
import java.util.Stack;
import java.util.stream.IntStream;

/**
 * @author kowsercse@gmail.com
 */
public class DynamicProgramming {

  public int kmp(final String pattern, final String text) {
    int m = 0;
    int i = 0;
    int[] table = buildTable(pattern);

    printTable(table);

    int c = 0;
    while (i < text.length()) {
      c++;
      if (text.charAt(i) == pattern.charAt(m)) {
        m++;
        i++;
        if (m == pattern.length()) {
          return i;
        }
      } else {
        if (m == 0) {
          i++;
        }
        m = table[m];
      }
    }

    System.out.println(c);
    return -1;
  }

  private int[] buildTable(final String pattern) {
    int pos = 2;
    int cnd = 0;
    final int[] table = new int[pattern.length()];
    table[0] = 0;
    table[1] = 0;

    while (pos < pattern.length()) {
      final char p = pattern.charAt(pos - 1);
      final char c = pattern.charAt(cnd);
      if (p == c) {
        table[pos] = cnd + 1;
        cnd++;
        pos++;
      } else if (cnd > 0) {
        cnd = table[cnd];
      } else {
        table[pos] = 0;
        pos++;
      }
    }

    return table;
  }

  /** Longest consecutive sequence */
  public String lcs(final String first, final String second) {
    int table[][] = new int[first.length() + 1][second.length() + 1];

    for (int row = 1; row <= first.length(); row++) {
      for (int col = 1; col <= second.length(); col++) {
        table[row][col] = Math.max(table[row - 1][col], table[row][col - 1]);
        if (first.charAt(row - 1) == second.charAt(col - 1)) {
          table[row][col]++;
        }
      }
    }

    final StringBuilder stringBuilder = new StringBuilder();
    int row = first.length();
    for (int col = second.length(); col > 0; col--) {
      while (table[row][col] == table[row][col - 1]) {
        col--;
      }
      stringBuilder.append(second.charAt(col - 1));
      row--;
    }

    return stringBuilder.reverse().toString();
  }

  public int getMaxVolume(int[] heights, int[] distances, int depth, int columnWidth) {
    final int size = heights.length;
    int maxVolume = 0;

    int maxHeightBetweenAB[][] = new int[size][size];
    int distanceBetweenAB[][] = new int[size][size];
    int extraBetweenAB[][] = new int[size][size];
    int volume[][] = new int[size][size];

    for (int i = 0; i < size - 1; i++) {
      distanceBetweenAB[i][i + 1] = distances[i];
      volume[i][i + 1] = distances[i] * Math.min(heights[i], heights[i + 1]);
      if (volume[i][i + 1] > maxVolume) {
        maxVolume = volume[i][i + 1];
      }
    }

    for (int i = 0; i < size; i++) {
      for (int j = i + 2; j < size; j++) {
        maxHeightBetweenAB[i][j] = Math.max(heights[j - 1], maxHeightBetweenAB[i][j - 1]);
        distanceBetweenAB[i][j] = distanceBetweenAB[i][j - 1] + distances[j - 1] + columnWidth;
        extraBetweenAB[i][j] = extraBetweenAB[i][j - 1] + columnWidth * heights[j - 1];

        if (maxHeightBetweenAB[i][j] < Math.min(heights[i], heights[j])) {
          final int x = Math.min(heights[i], heights[j]);
          volume[i][j] = distanceBetweenAB[i][j] * x - extraBetweenAB[i][j];
          if (volume[i][j] > maxVolume) {
            maxVolume = volume[i][j];
          }
        }
      }
    }

    return maxVolume;
  }

  /** The longest increasing sequence */
  public int lis(final List<Integer> sequence) {
    final int[] localLis = new int[sequence.size()];
    for (int i = 0; i < sequence.size(); i++) {
      localLis[i] = 1;
    }

    for (int i = 1; i < sequence.size(); i++) {
      int localMax = 0;
      for (int j = 0; j < i; j++) {
        if (sequence.get(i) > sequence.get(j) && localLis[j] > localMax) {
          localMax = localLis[j];
        }
      }
      localLis[i] = localMax + 1;
    }

    return IntStream.of(localLis).max().orElse(0);
  }

  public int getMaxEggDrop(final int egg, final int floor) {
    int[][] table = new int[egg + 1][floor + 1];

    for (int i = 1; i <= floor; i++) {
      table[1][i] = i;
    }

    for (int i = 1; i <= egg; i++) {
      table[i][1] = 1;
    }

    for (int e = 2; e <= egg; e++) {
      for (int f = 2; f <= floor; f++) {
        int min = Integer.MAX_VALUE;

        for (int col = 1; col < f; col++) {
          final int max = Math.max(table[e - 1][col - 1], table[e][f - col]);
          if (min > max) {
            min = max;
          }
        }
        table[e][f] = 1 + min;
      }
    }

    printTable(table);

    return table[egg][floor];
  }

  public int getMaxRectangularArea(final int[] histogram) {
    int maxArea = 0;
    final Stack<Integer> stack = new Stack<>();
    for (int i = 0; i < histogram.length; i++) {
      while (!stack.isEmpty() && histogram[i] < histogram[stack.peek()]) {
        final Integer top = stack.pop();
        int area = stack.isEmpty() ? histogram[top] * i : histogram[top] * (i - stack.peek() - 1);
        if (area > maxArea) {
          maxArea = area;
        }
      }
      stack.push(i);
    }

    while (!stack.isEmpty()) {
      final Integer top = stack.pop();
      int area =
          stack.isEmpty()
              ? histogram[top] * histogram.length
              : histogram[top] * (histogram.length - stack.peek() - 1);
      if (area > maxArea) {
        maxArea = area;
      }
    }

    return maxArea;
  }

  public int editDistance(final String s1, final String s2) {
    int table[][] = new int[s1.length() + 1][s2.length() + 1];
    for (int i = 0; i <= s1.length(); i++) {
      table[i][0] = i;
    }
    for (int i = 0; i <= s2.length(); i++) {
      table[0][i] = i;
    }
    System.out.println();
    for (int i = 1; i <= s1.length(); i++) {
      for (int j = 1; j <= s2.length(); j++) {
        table[i][j] =
            s1.charAt(i - 1) == s2.charAt(j - 1)
                ? table[i - 1][j - 1]
                : 1 + min(table[i - 1][j], table[i][j - 1], table[i - 1][j - 1]);
      }
    }
    printTable(table);
    return table[s1.length()][s2.length()];
  }

  public int evaluateZeroOneKnapsack(
      final int w, final List<Integer> weights, final List<Integer> values) {
    final int size = weights.size();
    int table[][] = new int[size + 1][w + 1];
    for (int i = 1; i <= size; i++) {
      for (int j = 1; j <= w; j++) {
        table[i][j] =
            j < weights.get(i - 1)
                ? table[i - 1][j]
                : Math.max(
                    table[i - 1][j - weights.get(i - 1)] + values.get(i - 1), table[i - 1][j]);
      }
    }

    return table[size][w];
  }

  public int minCostPath(final int[][] cost) {
    int table[][] = new int[cost.length][cost[0].length];
    table[0][0] = cost[0][0];
    for (int i = 1; i < cost.length; i++) {
      table[i][0] = table[i - 1][0] + cost[i][0];
    }
    for (int i = 1; i < cost[0].length; i++) {
      table[0][i] = table[0][i - 1] + cost[0][i];
    }

    for (int i = 1; i < cost.length; i++) {
      for (int j = 1; j < cost[0].length; j++) {
        table[i][j] = cost[i][j] + min(table[i - 1][j], table[i][j - 1], table[i - 1][j - 1]);
      }
    }

    printTable(cost);
    System.out.println();
    printTable(table);
    return table[cost.length - 1][cost[0].length - 1];
  }

  public int longestPalindromeSubSequence(final String sequence) {
    int maxLength = 0;
    final int[][] cost = new int[sequence.length()][sequence.length()];
    final char[][] str = new char[sequence.length()][sequence.length()];
    final boolean[][] palindrome = new boolean[sequence.length()][sequence.length()];
    for (int i = 0; i < sequence.length(); i++) {
      cost[i][i] = 1;
      palindrome[i][i] = true;
    }
    for (int i = 1; i < sequence.length(); i++) {
      palindrome[i][i - 1] = true;
    }

    for (int length = 1; length < sequence.length(); length++) {
      for (int i = 0; i < sequence.length() - length; i++) {
        final int j = i + length;
        cost[i][j] = Math.max(cost[i][j - 1], cost[i + 1][j]);
        if (sequence.charAt(i) == sequence.charAt(j)) {
          cost[i][j] = Math.max(cost[i][j], 2 + cost[i + 1][j - 1]);
          palindrome[i][j] = true;
        }
        if (cost[i][j] > maxLength) {
          maxLength = cost[i][j];
          str[i][j] = sequence.charAt(i);
        }
      }
    }

    return maxLength;
  }

  public int maxSumIncreasingSubSequence(final List<Integer> numbers) {
    final int[] cost = new int[numbers.size()];
    cost[0] = numbers.get(0);

    for (int i = 1; i < numbers.size(); i++) {
      int max = 0;
      final Integer number = numbers.get(i);
      for (int j = 0; j < i; j++) {
        if (number > numbers.get(j) && cost[j] > max) {
          max = cost[j];
        }
      }
      cost[i] = max + number;
    }

    int max = cost[0];
    for (int i = 1; i < cost.length; i++) {
      if (cost[i] > max) {
        max = cost[i];
      }
    }

    return max;
  }

  public int getMaxProfitFromShare(final List<Integer> prices) {
    final int size = prices.size();
    final int[] profits = new int[size];

    for (int i = 2; i < size - 2; i++) {
      profits[i] = maxProfitSingle(prices.subList(0, i)) + maxProfitSingle(prices.subList(i, size));
    }

    int maxProfit = profits[0];
    for (final int profit : profits) {
      maxProfit = Math.max(maxProfit, profit);
    }

    return maxProfit;
  }

  public int maxProfitSingle(final List<Integer> shares) {
    return maxDiff(shares.toArray(new Integer[] {}));
  }

  public int maxDiff(final Integer[] numbers) {
    int localMin = numbers[0];
    int maxDiff = 0;

    for (final int current : numbers) {
      maxDiff = Math.max(maxDiff, current - localMin);
      localMin = Math.min(localMin, current);
    }

    return maxDiff;
  }

  public int calculateMatrixChainMultiplication(final int[] sizes) {
    final int size = sizes.length - 1;
    int cost[][] = new int[size][size];

    for (int length = 1; length < size; length++) {
      for (int start = 0; start < size - length; start++) {
        int end = start + length;
        cost[start][end] = Integer.MAX_VALUE;

        for (int k = start; k < end; k++) {
          int currentCost =
              cost[start][k] + cost[k + 1][end] + sizes[start] * sizes[k + 1] * sizes[end + 1];
          cost[start][end] = Math.min(cost[start][end], currentCost);
        }
      }
    }

    return cost[0][size - 1];
  }

  public int calculateCoinChange(final int[] coins, final int amount) {
    int table[][] = new int[coins.length][amount + 1];
    int change[] = new int[amount + 1];

    change[0] = 1;
    for (int i = 0; i < coins.length; i++) {
      final int coin = coins[i];
      for (int j = coin; j <= amount; j++) {
        change[j] += change[j - coin];
        table[i][j] = change[j];
      }
    }

    printTable(table);

    return change[amount];
  }

  public int[] calculateAllPossibleSum(final int[] numbers) {
    int totalSum = 0;
    for (final int number : numbers) {
      totalSum += number;
    }

    final int table[][] = new int[numbers.length + 1][totalSum + 1];
    table[0][0] = 1;

    for (int i = 1; i <= numbers.length; i++) {
      final int number = numbers[i - 1];
      table[i][0] = 1;
      for (int sum = totalSum; sum > 0; sum--) {
        table[i][sum] = table[i - 1][sum];
        if ((sum - number) >= 0 && table[i - 1][sum - number] > 0) {
          table[i][sum]++;
        }
      }
    }

    return table[numbers.length];
  }

  boolean subsetSumExists(int[] numbers, int targetSum) {
    boolean[] table = new boolean[targetSum + 1];
    table[0] = true;

    for (int number : numbers) {
      for (int sum = number; sum <= targetSum; sum++) {
        if (table[sum - number]) {
          table[sum] = true;
        }
      }
    }

    return table[targetSum];
  }

  private int max(int x, int y, int z) {
    return Math.max(Math.max(x, y), z);
  }

  private int min(int x, int y, int z) {
    return Math.min(Math.min(x, y), z);
  }
}
