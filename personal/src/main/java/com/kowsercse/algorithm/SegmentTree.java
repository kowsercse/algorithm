package com.kowsercse.algorithm;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class SegmentTree {

  List<Integer> countItems(String input, List<Query> queries) {
    if (input == null || input.isEmpty()) {
      return List.of();
    }

    Node root = createSegmentTree(input);

    List<Integer> result = new LinkedList<>();
    for (Query query : queries) {
      result.add(root.countElement(query.start(), query.end()));
    }

    return result;
  }

  private Node createSegmentTree(String input) {
    List<Node> level = createChildNodes(input);

    while (level.size() != 1) {
      level = createLevel(level);
    }

    return level.get(0);
  }

  private List<Node> createLevel(List<Node> children) {
    List<Node> nodes = new ArrayList<>();

    for (int i = 0; i + 1 < children.size(); i += 2) {
      nodes.add(new Node(children.get(i), children.get(i + 1)));
    }

    if (children.size() % 2 == 1) {
      nodes.add(children.get(children.size() - 1));
    }
    return nodes;
  }

  List<Node> createChildNodes(String input) {
    int i = 0;
    char[] chars = input.toCharArray();
    while (chars[i] != '|') {
      i++;
    }

    List<Node> entries = new ArrayList<>();
    int start = i;
    for (i = i + 1; i < chars.length; i++) {
      if (chars[i] == '|') {
        entries.add(new Node(start, i, i - start - 1));
        start = i;
      }
    }

    return entries;
  }
}

record Query(int start, int end) {}

record Node(int start, int end, int total, Node left, Node right) {
  Node(int start, int end, int total) {
    this(start, end, total, null, null);
  }

  Node(Node left, Node right) {
    this(left.start(), right.end(), left.total() + right.total(), left, right);
  }

  public int countElement(int start, int end) {
    if (this.start <= start && this.end >= end) {
      return total;
    } else if (this.start >= end || this.end <= start) {
      return 0;
    }
    int total = 0;
    if (left != null) {
      total += left.countElement(start, end);
    }
    if (right != null) {
      total += right.countElement(start, end);
    }
    return total;
  }
}
