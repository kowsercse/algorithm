package com.kowsercse.algorithm.graph;

public class Edge {
  private final Integer source;
  private final Integer destination;
  private final Integer cost;

  public Integer getSource() {
    return source;
  }

  public Integer getDestination() {
    return destination;
  }

  public Integer getCost() {
    return cost;
  }

  public Edge(final int source, final int destination, final int cost) {
    this.source = source;
    this.destination = destination;
    this.cost = cost;
  }

  @Override
  public String toString() {
    return "Edge{source=" + source + ", destination=" + destination + ", cost=" + cost + '}';
  }
}
