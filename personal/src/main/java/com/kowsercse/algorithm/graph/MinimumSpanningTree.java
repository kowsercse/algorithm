package com.kowsercse.algorithm.graph;

import java.util.*;

public class MinimumSpanningTree {
  private final int size;
  private List<Edge> edges = new ArrayList<>();
  private Map<Integer, Integer> roots = new HashMap<>();
  private Map<Integer, Integer> sizes = new HashMap<>();

  private List<Edge> selectedEdges = new ArrayList<>();

  public MinimumSpanningTree(final int size) {
    this.size = size;
    for (int i = 1; i <= size; i++) {
      initializeRoot(i);
    }
  }

  public void addEdge(final int source, final int destination, final int cost) {
    final Edge edge = new Edge(source, destination, cost);
    edges.add(edge);
  }

  public void union(final Edge edge) {
    final Integer root1 = getRoot(edge.getSource());
    final Integer root2 = getRoot(edge.getDestination());
    if (root1.equals(root2)) {
      return;
    }

    selectedEdges.add(edge);

    final Integer size1 = sizes.get(root1);
    final Integer size2 = sizes.get(root2);
    if (size1 < size2) {
      roots.put(root1, root2);
      sizes.put(root2, size1 + size2);
    } else {
      roots.put(root2, root1);
      sizes.put(root1, size1 + size2);
    }
  }

  public Integer getRoot(Integer name) {
    Integer root = roots.get(name);
    while (!name.equals(root)) {
      roots.put(name, roots.get(root));
      name = root;
      root = roots.get(name);
    }
    return root;
  }

  public List<Edge> calculate() {
    Collections.sort(edges, (left, right) -> left.getCost().compareTo(right.getCost()));
    for (final Edge edge : edges) {
      union(edge);
    }

    return selectedEdges;
  }

  public int getSize(final Integer name) {
    final Integer root = getRoot(name);
    return sizes.get(root);
  }

  private void initializeRoot(final Integer name) {
    if (roots.get(name) == null) {
      roots.put(name, name);
      sizes.put(name, 1);
    }
  }
}
