package com.kowsercse.algorithm.graph;

import java.util.ArrayDeque;
import java.util.Queue;
import java.util.Stack;

public class Graph {
  public static final boolean TEMPORARY = false;
  private final int size;
  private final Integer[][] graph;

  public void addEdge(final int source, final int destination, final int cost) {
    graph[source][destination] = cost;
    //    graph[destination][source] = cost;
  }

  public Graph(final int size) {
    this.size = size;
    graph = new Integer[size + 1][size + 1];
  }

  public void bfs(final int vertex) {
    final Queue<Integer> queue = new ArrayDeque<>();
    final boolean[] processed = new boolean[size + 1];

    queue.add(vertex);
    processed[vertex] = true;

    while (!queue.isEmpty()) {
      final Integer next = queue.remove();
      System.out.println(next);
      for (int column = 0; column <= size; column++) {
        if (graph[next][column] != null && !processed[column]) {
          processed[column] = true;
          queue.add(column);
        }
      }
    }
  }

  public void dfs(final int vertex) {
    final boolean[] processed = new boolean[size + 1];
    dfs(vertex, 0, processed);
  }

  private void dfs(final int vertex, final int depth, final boolean[] processed) {
    processed[vertex] = true;
    for (int i = 0; i < depth; i++) {
      System.out.print("|");
    }
    System.out.println(vertex + " " + depth);
    for (int column = 1; column <= size; column++) {
      if (graph[vertex][column] != null && !processed[column]) {
        dfs(column, depth + 1, processed);
      }
    }
  }

  public Stack<Integer> topologicalSort() {
    final Stack<Integer> stack = new Stack<>();
    final boolean[] processed = new boolean[size + 1];
    for (int vertex = 1; vertex <= size; vertex++) {
      if (!processed[vertex]) {
        visit(vertex, processed, stack);
      }
    }

    return stack;
  }

  private void visit(final int vertex, final boolean[] processed, final Stack<Integer> stack) {
    if (!processed[vertex]) {
      processed[vertex] = true;
      for (int destination = 1; destination <= size; destination++) {
        if (graph[vertex][destination] != null && !processed[destination]) {
          visit(destination, processed, stack);
        }
      }
      stack.push(vertex);
    }
  }
}
