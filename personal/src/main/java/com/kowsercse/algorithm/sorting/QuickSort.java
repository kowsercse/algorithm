package com.kowsercse.algorithm.sorting;

import java.util.Collections;
import java.util.List;
import java.util.Random;

public class QuickSort {

  private static final Random random = new Random();

  public static void sort(List<Integer> list) {
    sort(list, 0, list.size() - 1);
  }

  private static void sort(List<Integer> list, int low, int high) {
    if (low >= high) {
      return;
    }
    int pivot = randomizedPartition(list, low, high);
    sort(list, low, pivot - 1);
    sort(list, pivot + 1, high);
  }

  private static int randomizedPartition(List<Integer> list, int low, int high) {
    int index = random.nextInt(1 + high - low);
    Collections.swap(list, high, low + index);
    return partition(list, low, high);
  }

  private static int partition(List<Integer> list, int low, int high) {
    int left = low;
    int right = high - 1;
    int pivot = list.get(high);

    while (true) {
      while (list.get(left) < pivot) {
        left++;
      }
      while (right >= low && list.get(right) > pivot) {
        right--;
      }

      if (left >= right) {
        break;
      } else {
        Collections.swap(list, left, right);
      }
    }

    Collections.swap(list, left, high);
    return left;
  }
}
