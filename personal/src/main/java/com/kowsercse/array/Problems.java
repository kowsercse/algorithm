package com.kowsercse.array;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

class Problems {

  int[] sumOfTwoNumber(int[] numbers, int target) {
    HashMap<Integer, Integer> numberByIndex = new HashMap<>();

    for (int i = 0; i < numbers.length; i++) {
      numberByIndex.put(numbers[i], i++);
    }
    for (int i = 0; i < numbers.length; i++) {
      if (numberByIndex.containsKey(target - numbers[i])) {
        return new int[] {i, numberByIndex.get(target - numbers[i])};
      }
    }
    return null;
  }

  static boolean[] gameOfLife(boolean[] input, int year) {
    boolean[] sequenceOfLife = new boolean[input.length];
    boolean[] temp = new boolean[input.length];
    System.arraycopy(input, 0, sequenceOfLife, 0, input.length);
    System.arraycopy(input, 0, temp, 0, input.length);
    for (int i = 0; i < year; i++) {
      temp[0] = false;
      temp[temp.length - 1] = false;
      for (int j = 1; j < sequenceOfLife.length - 1; j++) {
        temp[j] =
            (sequenceOfLife[j - 1] && sequenceOfLife[j + 1])
                || (!sequenceOfLife[j - 1] && !sequenceOfLife[j + 1]);
      }
      System.arraycopy(temp, 0, sequenceOfLife, 0, temp.length);
    }
    return sequenceOfLife;
  }

  static void applyPermutation(List<Character> input, List<Integer> permutation) {
    for (int i = 0; i < input.size(); i++) {
      int next = i;
      while (permutation.get(next) >= 0) {
        Collections.swap(input, i, permutation.get(next));
        int temp = permutation.get(next);
        permutation.set(next, permutation.get(next) - permutation.size());
        next = temp;
      }
    }
    for (int i = 0; i < permutation.size(); i++) {
      permutation.set(i, permutation.get(i) + permutation.size());
    }
  }

  static void nextPermutation(List<Character> input, List<Integer> currentPermutation) {
    ArrayList<Integer> output = new ArrayList<>();
    int pivotIndex = getPivotIndex(currentPermutation);
    Integer pivot = currentPermutation.get(pivotIndex);

    int i = currentPermutation.size() - 1;
    while (currentPermutation.get(i) < pivot) {
      i--;
    }
    Integer newPivot = currentPermutation.get(i);

    for (int j = 0; j < pivotIndex; j++) {
      output.add(currentPermutation.get(j));
    }
    output.add(newPivot);
    for (int j = currentPermutation.size() - 1; j > i; j--) {
      output.add(currentPermutation.get(j));
    }
    output.add(pivot);
    for (int j = i - 1; j > pivot + 1; j--) {
      output.add(currentPermutation.get(j));
    }
    applyPermutation(input, output);
  }

  private static int getPivotIndex(List<Integer> currentPermutation) {
    Integer previousElement = Integer.MIN_VALUE;
    int i = currentPermutation.size() - 1;
    while (i > 0 && currentPermutation.get(i) > previousElement) {
      previousElement = currentPermutation.get(i);
      i--;
    }
    return i;
  }
}
