package com.kowsercse.interview;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/*
calendar = [["9:00", "10:30"], ["12:00", "13:00"], ["16:00", "18:00"]]
dailyBounds1 = ["9:00", "20:00"]
calendar2 = [["10:00", "11:30"], ["12:30", "14:30"], ["14:30", "15:00"], ["16:00", "17:00]]
dailyBounds2 = ["10:00", "18:30"]
duration = 30
 */
public class Spotnana {

  public static void main(String[] args) {
    var slots =
        getSlots(
            new String[][]{{"09:00", "10:30"}, {"12:00", "13:00"}, {"16:00", "18:00"}},
            new String[]{"09:00", "20:00"},
            new String[][]{{"10:00", "11:30"}, {"12:30", "14:30"}, {"14:30", "15:00"},
                {"16:00", "17:00"}},
            new String[]{"10:00", "18:30"},
            30);
    System.out.println(slots);
  }

  private static LinkedList<List<Integer>> getSlots(
      String[][] calendar1, String[] dailyBounds1,
      String[][] calendar2, String[] dailyBounds2,
      int query) {
    var left = getLocalTimes(calendar1);
    var right = getLocalTimes(calendar2);

    var result = new LinkedList<List<Integer>>();
    var start = Math.max(toMinute(dailyBounds1[0]), toMinute(dailyBounds2[0]));
    while (!left.isEmpty() || !right.isEmpty()) {
      var min = min(left, right);
      int[] slot = min.removeFirst();
      if (start < slot[0] && (slot[1] - start) >= query) {
        result.add(List.of(start, slot[0]));
      }
      start = slot[1];
    }

    var end = Math.min(toMinute(dailyBounds1[1]), toMinute(dailyBounds2[1]));
    if (start < end && (end - start) >= query) {
      result.add(List.of(start, end));
    }

    return result;
  }

  private static LinkedList<int[]> min(LinkedList<int[]> left, LinkedList<int[]> right) {
    if (left.isEmpty()) {
      return right;
    } else if (right.isEmpty()) {
      return left;
    } else {
      return left.getFirst()[0] < right.getFirst()[0] ? left : right;
    }
  }

  private static LinkedList<int[]> getLocalTimes(String[][] calendar) {
    return Arrays.stream(calendar)
        .map(entry -> new int[]{toMinute(entry[0]), toMinute(entry[1])})
        .collect(Collectors.toCollection(LinkedList::new));
  }

  private static int toMinute(String time) {
    String[] split = time.split(":");
    return Integer.parseInt(split[0]) * 60 + Integer.parseInt(split[1]);
  }

  static String toString(int slot) {
    return String.format("%02d:%02d", slot / 60, slot % 60);
  }
}
