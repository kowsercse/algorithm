package com.kowsercse.interview;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class Solution {

  public static void main(String[] args) {

  }


}

class SimpleBlockingQueue {

  Queue<Integer> queue = new LinkedList<>();
  ReadWriteLock lock = new ReentrantReadWriteLock();

  Lock writeLock = lock.writeLock();
  Lock readLock = lock.readLock();
  Condition condition;

  void push(int value) {
    try {
      writeLock.lock();
      queue.add(value);
      condition.signal();
    } finally {
      writeLock.unlock();
    }
  }

  int pop() {
    while (true) {
      if (queue.isEmpty()) {
        try {
          condition.await();
        } catch (InterruptedException e) {
          throw new RuntimeException(e);
        }
      }

      try {
        readLock.lock();
        return queue.poll();
      } finally {
        readLock.unlock();
      }
    }
  }
}

/*

Order + Delivery

checkout -> [confirm payment -> verified, notify restaurant ->  confirm order, allocate delivery] apply (&) -> order completed

Order -> event stream -> OrderService -> OrderOrchastrationService -> decompose multiple events [events] -> [corresponding service events ] -> aggrgate [result] => transaction is succesfull
OrderOrchastrationService

PaymentService
RestaurantNotificationService
DeliveryService

OrderConfirmationService
aggregate[confirm payment -> verified, notify restaurant ->  confirm order, allocate delivery]

nodes -> connect ->


bots -> [ssh | secured] -> machine
traffic inspection -> data
 */
