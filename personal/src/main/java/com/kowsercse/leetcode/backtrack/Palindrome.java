package com.kowsercse.leetcode.backtrack;

import java.util.Arrays;

/**
 * @see <a href="https://leetcode.com/problems/palindrome-partitioning-ii/">132. Palindrome
 *     Partitioning II</a>
 */
public class Palindrome {

  public int minCut(String s) {
    if (s == null || s.isEmpty()) {
      return 0;
    }

    int length = s.length();
    char[] string = s.toCharArray();
    boolean[][] table = new boolean[length][length];

    for (int i = 0; i < length; i++) {
      table[i][i] = true;
    }
    for (int i = 0; i < length - 1; i++) {
      table[i][i + 1] = string[i] == string[i + 1];
    }

    for (int delta = 2; delta < length; delta++) {
      for (int start = 0; start + delta < length; start++) {
        int end = start + delta;
        table[start][end] = table[start + 1][end - 1] && string[start] == string[end];
      }
    }
    /**/
    for (int i = 0; i < length; i++) {
      for (int c = 0; c < length; c++) {
        System.out.print(table[i][c] ? 1 : 0);
      }
      System.out.println();
    }
    /**/

    int[] cut = new int[length];
    for (int c = 0; c < length; c++) {
      cut[c] = table[0][c] ? 0 : cut[c - 1] + 1;
    }
    for (int c = 1; c < length; c++) {
      for (int r = 1; r < c; r++) {
        if (table[r][c]) {
          cut[c] = Math.min(cut[c], cut[r - 1] + 1);
        }
      }
      System.out.println(Arrays.toString(cut));
    }

    return cut[length - 1];
  }
}
