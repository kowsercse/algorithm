package com.kowsercse.leetcode.dynamic;

public class Interleaving {

  private char[] left;
  private char[] right;
  private char[] I;
  private Boolean[][] dp;

  public boolean isInterleave(String s1, String s2, String s3) {
    if (s1 == null || s2 == null || s3 == null) {
      return false;
    }
    if(s1.length() + s2.length() != s3.length()) {
      return false;
    }

    left = s1.toCharArray();
    right = s2.toCharArray();
    I = s3.toCharArray();
    dp = new Boolean[left.length + 1][right.length + 1];

    dp[0][0] = true;
    return dfs(I.length, left.length, right.length);
  }

  private boolean dfs(int length, int l, int r) {
    if (length < 0 || l < 0 || r < 0) {
      return false;
    }
    if (dp[l][r] != null) {
      return dp[l][r];
    }

    return dp[l][r] =
        (l > 0 && I[length - 1] == left[l - 1] && dfs(length - 1, l - 1, r))
            || (r > 0 && I[length - 1] == right[r - 1] && dfs(length - 1, l, r - 1));
  }

  private boolean bottomUpSolution(String s1, String s2, String s3) {
    if (s1 == null || s2 == null || s3 == null) {
      return false;
    }

    left = s1.toCharArray();
    right = s2.toCharArray();
    I = s3.toCharArray();
    dp = new Boolean[left.length + 1][right.length + 1];

    dp[0][0] = true;
    for (int l = 0; l <= left.length; l++) {
      for (int r = 0; r <= right.length; r++) {
        if (l != 0 || r != 0) {
          dp[l][r] = isInterleaveByLeft(l, r) || isInterleaveByRight(l, r);
        }
      }
    }
    return dp[left.length][right.length];
  }

  private boolean isInterleaveByLeft(int l, int r) {
    return l != 0 && dp[l - 1][r] && I[l + r - 1] == left[l - 1];
  }

  private boolean isInterleaveByRight(int l, int r) {
    return r != 0 && dp[l][r - 1] && I[l + r - 1] == right[r - 1];
  }
}
