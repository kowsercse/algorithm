package com.kowsercse.leetcode.dynamic;

public class PalindromeSubstring {

  public String longestPalindrome(String s) {
    if (s == null || s.length() == 0) {
      return null;
    }

    char[] chars = s.toCharArray();
    int length = s.length();
    boolean[][] dp = new boolean[length][length];
    int max = 0;
    int start = 0, end = 0;

    for (int i = 0; i < length; i++) {
      dp[i][i] = true;
    }

    for (int i = 0; i < length - 1; i++) {
      dp[i][i + 1] = chars[i] == chars[i + 1];
      if (dp[i][i + 1]) {
        max = 1;
        start = i;
        end = i + 1;
      }
    }

    for (int l = 2; l < length; l++) {
      for (int i = 0; i + l < length; i++) {
        dp[i][i + l] = chars[i] == chars[i + l] && dp[i + 1][i + l - 1];

        if (dp[i][i + l] && max < l) {
          max = l;
          start = i;
          end = i + l;
        }
      }
    }

    return s.substring(start, end + 1);
  }
}