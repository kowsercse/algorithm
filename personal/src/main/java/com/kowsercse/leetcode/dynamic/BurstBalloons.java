package com.kowsercse.leetcode.dynamic;

import com.kowsercse.util.Utils;

public class BurstBalloons {

  static class Solution {

    int[] nums;
    Integer[][] table;

    public int maxCoins(int[] nums) {
      if (nums == null || nums.length == 0) {
        return 0;
      }

      this.nums = createNums(nums);
      this.table = new Integer[nums.length + 2][nums.length + 2];

      dp(1, nums.length);
      Utils.printTable(table, "%4d");

      return table[1][nums.length];
    }

    int dp(int left, int right) {
      if (table[left][right] != null) {
        return table[left][right];
      }

      int max = 0;
      for (int i = left; i <= right; i++) {
        int gain = dp(left, i - 1) + nums[left - 1] * nums[i] * nums[right + 1] + dp(i + 1, right);
        max = Math.max(gain, max);
      }

      return table[left][right] = max;
    }

    int[] createNums(int[] nums) {
      int[] temp = new int[nums.length + 2];
      temp[0] = temp[temp.length - 1] = 1;
      System.arraycopy(nums, 0, temp, 1, nums.length);
      return temp;
    }
  }
}