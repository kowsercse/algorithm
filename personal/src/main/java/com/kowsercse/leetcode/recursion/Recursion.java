package com.kowsercse.leetcode.recursion;

public class Recursion {

  public TreeNode searchBST(TreeNode root, int val) {
    if (root == null) {
      return null;
    }

    if (val > root.val) {
      return searchBST(root.right, val);
    }
    if (val < root.val) {
      return searchBST(root.left, val);
    } else {
      return root;
    }
  }

  public ListNode reverseList(ListNode head) {
    return reverseListInternal(null, head);
  }

  // previous -> current -> next
  private ListNode reverseListInternal(ListNode previousNode, ListNode currentNode) {
    if (currentNode == null) {
      return previousNode;
    }

    ListNode nextNode = currentNode.next;
    currentNode.next = previousNode;

    return reverseListInternal(currentNode, nextNode);
  }

  public void reverseString(char[] s) {
    reverseStringInternal(s, 0, s.length - 1);
  }

  private void reverseStringInternal(char[] string, int source, int destination) {
    if (source > destination) {
      return;
    }

    char temp = string[destination];
    string[destination] = string[source];
    string[source] = temp;

    reverseStringInternal(string, source + 1, destination - 1);
  }

  public ListNode swapPairs(ListNode head) {
    if (head == null) {
      return head;
    }

    ListNode second = head.next;
    if (second == null) {
      return head;
    }

    int temp = second.val;
    second.val = head.val;
    head.val = temp;

    swapPairs(second.next);
    return head;
  }
}
