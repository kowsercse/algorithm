package com.kowsercse.leetcode.august2020;

public class P124 {
  Integer sum;
  int max;

  public int maxPathSum(TreeNode root) {
    max = root.val;
    inOrder(root);
    return max;
  }

  void inOrder(TreeNode node) {
    if (node.left != null) {
      inOrder(node.left);
    }

    sum = sum == null ? node.val : sum + node.val;
    if (node.val > sum) {
      sum = node.val;
    }

    max = Math.max(max, sum);
    System.out.println(node.val + " - sum=" + sum + " max=" + max);

    if (node.right != null) {
      inOrder(node.right);
    }
  }

  static record TreeNode(int val, TreeNode left, TreeNode right) {
    TreeNode(int val) {
      this(val, null, null);
    }
  }
}
