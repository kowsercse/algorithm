package com.kowsercse.leetcode.august2020;

import java.util.*;

public class P269 {
  static class Solution {
    private final Graph graph = new Graph();

    public String alienOrder(String[] words) {
      if (words == null && words.length == 0) {
        return "";
      }

      createGraph(Arrays.asList(words), 0);
      return graph.hasCycle() ? "" : graph.topologicalSort();
    }

    void createGraph(List<String> words, int index) {
      if (words.size() <= 1) {
        return;
      }

      char source = words.get(0).charAt(index);
      List<String> wordBucket = new LinkedList<>();
      for (String word : words) {
        char destination = word.charAt(index);
        graph.getNode(destination);
        if (destination != source) {
          createGraph(wordBucket, index + 1);

          graph.addVertices(source, destination);
          source = destination;
          wordBucket = new LinkedList<>();
        }

        if (word.length() > index + 1) {
          wordBucket.add(word);
        }
      }

      createGraph(wordBucket, index + 1);
    }
  }

  static class Graph {
    private Stack<Node> stack = new Stack<>();
    Map<Character, Node> nodes = new HashMap<>();
    Set<Character> visited;

    Set<Character> cycleCheck = new HashSet<>();

    void addVertices(char source, char destination) {
      getNode(source).destinations.add(getNode(destination));
      getNode(destination).totalParent++;
    }

    Node getNode(char key) {
      return nodes.computeIfAbsent(key, Node::new);
    }

    String topologicalSort() {
      visited = new HashSet<>();
      for (Node node : nodes.values()) {
        dfs(node);
      }
      StringBuilder stringJoiner = new StringBuilder();
      while (!stack.isEmpty()) {
        stringJoiner.append(stack.pop().source);
      }
      System.out.println(stringJoiner.toString());
      return stringJoiner.toString();
    }

    private void dfs(Node node) {
      if (visited.contains(node.source)) {
        return;
      }

      visited.add(node.source);
      for (Node destination : node.destinations) {
        dfs(destination);
      }

      stack.add(node);
    }

    public boolean hasCycle() {
      visited = new HashSet<>();
      for (Node node : nodes.values()) {
        if (!visited.contains(node.source)) {
          if (hasCycle(node)) {
            return true;
          }
        }
      }

      return false;
    }

    private boolean hasCycle(Node node) {
      if (cycleCheck.contains(node.source)) {
        return true;
      }

      cycleCheck.add(node.source);
      if (visited.contains(node.source)) {
        return false;
      }

      for (Node destination : node.destinations) {
        if (hasCycle(destination)) {
          return true;
        }
      }

      cycleCheck.remove(node.source);
      return false;
    }
  }

  static class Node {
    final char source;
    int totalParent = 0;
    List<Node> destinations = new LinkedList<>();

    Node(char source) {
      this.source = source;
    }

    @Override
    public String toString() {
      return String.valueOf(source);
    }
  }
}
