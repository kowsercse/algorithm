package com.kowsercse.leetcode.august2020;

import java.util.HashMap;
import java.util.Map;

class Logger {

  private final Map<String, Integer> messageByTimestamp = new HashMap<>();

  /** Initialize your data structure here. */
  public Logger() {}

  /**
   * Returns true if the message should be printed in the given timestamp, otherwise returns false.
   * If this method returns false, the message will not be printed. The timestamp is in seconds
   * granularity.
   */
  public boolean shouldPrintMessage(int timestamp, String message) {
    if (message == null) {
      return false;
    }

    if (!messageByTimestamp.containsKey(message)) {
      messageByTimestamp.put(message, timestamp);
      return true;
    }

    if (timestamp >= messageByTimestamp.get(message) + 10) {
      messageByTimestamp.put(message, timestamp);
      return true;
    }

    return false;
  }

  public boolean detectCapitalUse(String word) {
    if (word == null) {
      return false;
    }

    char firstChar = word.charAt(0);
    if (word.length() == 1) {
      return true;
    }

    if (isLowerCase(firstChar)) {
      return isBetweenRange(word, 'a', 'z');
    }

    return isLowerCase(word.charAt(1))
        ? isBetweenRange(word, 'a', 'z')
        : isBetweenRange(word, 'A', 'Z');
  }

  boolean isBetweenRange(String word, char start, char end) {
    for (int i = 1; i < word.length(); i++) {
      char middleChar = word.charAt(i);
      if (middleChar < start || middleChar > end) {
        return false;
      }
    }

    return true;
  }

  boolean isLowerCase(char c) {
    return c >= 'a' && c <= 'z';
  }

  boolean isUpperCase(char c) {
    return c >= 'A' && c <= 'Z';
  }
}
