package com.kowsercse.leetcode.august2020;

class MyHashSet {

  private Node root;

  /** Initialize your data structure here. */
  public MyHashSet() {}

  public void add(int key) {
    if (root == null) {
      root = new Node(key);
    }

    Node parent = sink(key);
    if (key < parent.value) {
      parent.left = new Node(key);
    } else if (key > parent.value) {
      parent.right = new Node(key);
    }
  }

  /** Returns true if this set contains the specified element */
  public boolean contains(int key) {
    if (root == null) {
      return false;
    }

    return sink(key).value == key;
  }

  public void remove(int key) {
    root = delete(root, key);
  }

  Node delete(Node node, int key) {
    if (node == null) {
      return null;
    } else if (key < node.value) {
      node.left = delete(node.left, key);
    } else if (key > node.value) {
      node.right = delete(node.right, key);
    } else {
      if (node.left == null && node.right == null) {
        return null;
      } else if (node.left != null && node.right == null) {
        return node.left;
      } else if (node.left == null && node.right != null) {
        return node.right;
      } else {
        Node inorderNode = node.right;
        while (inorderNode.left != null) {
          inorderNode = inorderNode.left;
        }

        node.right = delete(node.right, inorderNode.value);
        node.value = inorderNode.value;
      }
    }

    return node;
  }

  Node sink(int key) {
    Node parent = root;
    Node child = key < parent.value ? parent.left : parent.right;

    while (parent.value != key && child != null) {
      parent = child;
      child = key < parent.value ? parent.left : parent.right;
    }

    return parent;
  }

  public void inorder() {
    if (root == null) {
      return;
    }

    root.inorder();
  }

  static class Node {
    int value;
    Node left;
    Node right;

    Node(int key) {
      value = key;
    }

    public void inorder() {
      System.out.print("(");
      if (left != null) {
        left.inorder();
      }
      System.out.print(" " + value + " ");
      if (right != null) {
        right.inorder();
      }
      System.out.print(")");
    }
  }
}

/**
 * Your MyHashSet object will be instantiated and called as such: MyHashSet obj = new MyHashSet();
 * obj.add(key); obj.remove(key); boolean param_3 = obj.contains(key);
 */
