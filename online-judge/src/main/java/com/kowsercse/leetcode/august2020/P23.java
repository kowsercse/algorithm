package com.kowsercse.leetcode.august2020;

public class P23 {

  public static void main(String[] args) {
    Solution solution = new Solution();

    ListNode result =
        solution.mergeKLists(
            new ListNode[] {
              toListNode(-6, -3, -1, 1, 2, 2, 2),
              toListNode(-10, -8, -6, -2, 4),
              toListNode(-2),
              toListNode(-8, -4, -3, -3, -2, -1, 1, 2, 3),
              toListNode(-8, -6, -5, -4, -2, -2, 2, 4)
            });

    while (result != null) {
      System.out.print(" " + result.val + " ");
      result = result.next;
    }
  }

  private static ListNode toListNode(int... args) {
    ListNode head = new ListNode();
    ListNode tail = head;

    for (int arg : args) {
      tail.next = new ListNode(arg);
      tail = tail.next;
    }
    return head.next;
  }

  static class ListNode {
    int val;
    ListNode next;

    ListNode() {}

    ListNode(int val) {
      this.val = val;
    }

    ListNode(int val, ListNode next) {
      this.val = val;
      this.next = next;
    }

    @Override
    public String toString() {
      return "" + val;
    }
  }

  static class Solution {

    public ListNode mergeKLists(ListNode[] lists) {
      if (lists == null || lists.length == 0) {
        return null;
      }

      ListNode head = new ListNode();
      ListNode tail = head;

      Heap heap = new Heap(lists.length);
      for (ListNode listNode : lists) {
        if (listNode != null) {
          heap.add(listNode);
        }
      }

      while (!heap.isEmpty()) {
        ListNode listNode = heap.poll();
        tail.next = new ListNode(listNode.val);
        tail = tail.next;

        if (listNode.next != null) {
          heap.add(listNode.next);
        }
      }

      return head.next;
    }
  }

  static class Heap {
    ListNode[] values;
    int size;

    Heap(int initialSize) {
      values = new ListNode[initialSize];
    }

    void add(ListNode node) {
      if (node == null) {
        return;
      }

      values[size] = node;
      bubbleUp(size);
      size++;
    }

    void bubbleUp(int index) {
      ListNode current = values[index];
      ListNode parent = values[(index - 1) / 2];

      if (current.val < parent.val) {
        swap(index, index / 2);
        bubbleUp((index - 1) / 2);
      }
    }

    ListNode poll() {
      ListNode value = values[0];
      size--;

      values[0] = values[size];
      values[size] = null;

      sink(0);

      return value;
    }

    void sink(int index) {
      int leftIndex = index * 2 + 1;
      int rightIndex = leftIndex + 1;

      ListNode left = leftIndex < size ? values[leftIndex] : null;
      ListNode right = rightIndex < size ? values[rightIndex] : null;

      int minIndex = index;
      ListNode minNode = values[index];

      if (left != null && left.val < minNode.val) {
        minIndex = leftIndex;
        minNode = left;
      }

      if (right != null && right.val < minNode.val) {
        minIndex = rightIndex;
        minNode = right;
      }

      if (minIndex == index) {
        return;
      }

      swap(index, minIndex);
      sink(minIndex);
    }

    boolean isEmpty() {
      return size == 0;
    }

    void swap(int left, int right) {
      ListNode temp = values[left];
      values[left] = values[right];
      values[right] = temp;
    }
  }
}
