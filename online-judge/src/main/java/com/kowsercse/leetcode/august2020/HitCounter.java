package com.kowsercse.leetcode.august2020;

import java.util.Map;
import java.util.TreeMap;

class HitCounter {

  private final TreeMap<Integer, Integer> totalCallsByTimestamp = new TreeMap<>();
  private int totalCalls = 0;

  /** Initialize your data structure here. */
  public HitCounter() {}

  /**
   * Record a hit.
   *
   * @param timestamp - The current timestamp (in seconds granularity).
   */
  public void hit(int timestamp) {
    clearHits(timestamp);

    totalCalls++;
    totalCallsByTimestamp.compute(timestamp, (key, value) -> value == null ? 1 : value + 1);
  }

  /**
   * Return the number of hits in the past 5 minutes.
   *
   * @param timestamp - The current timestamp (in seconds granularity).
   */
  public int getHits(int timestamp) {
    clearHits(timestamp);
    return totalCalls;
  }

  private void clearHits(int timestamp) {
    Map<Integer, Integer> map = totalCallsByTimestamp.headMap(timestamp - 300, true);
    totalCalls -= map.values().stream().mapToInt(i -> i).sum();
    map.clear();
  }
}

/**
 * Your HitCounter object will be instantiated and called as such: HitCounter obj = new
 * HitCounter(); obj.hit(timestamp); int param_2 = obj.getHits(timestamp);
 */
