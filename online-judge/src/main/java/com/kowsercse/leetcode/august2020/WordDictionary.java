package com.kowsercse.leetcode.august2020;

import java.util.HashMap;
import java.util.Map;

class WordDictionary {

  private final Trie trie = new Trie();

  /** Initialize your data structure here. */
  public WordDictionary() {}

  /** Adds a word into the data structure. */
  public void addWord(String word) {
    trie.add(word + '$');
  }

  /**
   * Returns if the word is in the data structure. A word could contain the dot character '.' to
   * represent any one letter.
   */
  public boolean search(String word) {
    return trie.search('^' + word + '$');
  }

  static class Trie {

    private final Node root = new Node('^');

    void add(String word) {
      Node node = root;
      for (int i = 0; i < word.length(); i++) {
        node = node.getOrCreateChild(word.charAt(i));
      }
    }

    boolean search(String word) {
      return root.search(word, 0);
    }
  }

  static class Node {
    private static final char WILD_CHAR = '.';
    private Map<Character, Node> children = new HashMap<>();
    private final char value;

    Node(char value) {
      this.value = value;
    }

    Node getOrCreateChild(char key) {
      return children.computeIfAbsent(key, Node::new);
    }

    boolean search(String word, int index) {
      char key = word.charAt(index);
      if (key != WILD_CHAR && key != value) {
        return false;
      }
      if (index == word.length() - 1) {
        return key == value;
      }

      int nextIndex = index + 1;
      char nextKey = word.charAt(nextIndex);

      if (nextKey == WILD_CHAR) {
        for (Node child : children.values()) {
          if (child.search(word, nextIndex)) {
            return true;
          }
        }
        return false;
      } else {
        Node child = children.get(nextKey);
        return child != null && child.search(word, nextIndex);
      }
    }
  }
}
