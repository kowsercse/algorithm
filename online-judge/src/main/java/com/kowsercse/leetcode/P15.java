package com.kowsercse.leetcode;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class P15 {


  static class Solution {

    public List<List<Integer>> threeSum(int[] nums) {
      if (nums == null || nums.length == 0) {
        return Collections.emptyList();
      }

      var results = new HashSet<Combination>();
      var numbers = IntStream.of(nums).boxed().collect(Collectors.toSet());
      for (int i = 0; i < nums.length; i++) {
        for (int j = i + 1; j < nums.length; j++) {
          var sum = nums[i] + nums[j];
          if (numbers.contains(-sum)) {
            results.add(new Combination(nums[i], nums[j], -sum));
          }
        }
      }
      return results.stream().map(Combination::toList).collect(Collectors.toList());
    }
  }
}

class Combination {

  private final int a;
  private final int b;
  private final int c;
  private final TreeSet<Integer> treeSet;

  Combination(int a, int b, int c) {
    this.a = a;
    this.b = b;
    this.c = c;
    this.treeSet = new TreeSet<>(Arrays.asList(a, b, c));
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Combination that = (Combination) o;
    return Objects.equals(treeSet, that.treeSet);
  }

  @Override
  public int hashCode() {
    return Objects.hash(treeSet);
  }

  List<Integer> toList() {
    return List.of(a, b, c);
  }
}