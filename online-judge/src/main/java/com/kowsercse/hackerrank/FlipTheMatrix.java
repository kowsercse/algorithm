package com.kowsercse.hackerrank;

import java.util.Scanner;

public class FlipTheMatrix {

  private int[][] matrix;
  private int n;

  public static void main(String[] args) {
    new FlipTheMatrix().solve();
  }

  public void solve() {
    final Scanner scanner = new Scanner(System.in);

    int testCase = scanner.nextInt();
    while (testCase-- > 0) {
      n = scanner.nextInt();

      int matrixSize = 2 * n;
      matrix = new int[matrixSize][matrixSize];
      for (int i = 0; i < matrixSize; i++) {
        for (int j = 0; j < matrixSize; j++) {
          matrix[i][j] = scanner.nextInt();
        }
      }

      int maxSum = processTestCase();
      System.out.println(maxSum);
    }
  }

  private int processTestCase() {
    for (int r = 0; r < n; r++) {
      for (int c = 0; c < n; c++) {
        Cell candidate = getMaximumCandidateCell(r, c);
        Cell currentElement = new Cell(r, c);
        swapElement(currentElement, candidate);
      }
    }

    int sum = 0;
    for (int r = 0; r < n; r++) {
      for (int c = 0; c < n; c++) {
        sum += matrix[r][c];
      }
    }

    return sum;
  }

  private void swapElement(Cell element, Cell candidate) {
    boolean isSameColumn = candidate.c == element.c;
    boolean isSameRow = candidate.r == element.r;
    if (isSameColumn && isSameRow) {
      return;
    }
    if (isSameColumn) {
      reverse(candidate.r);
      flip(element.c);
      reverse(candidate.r);
      flip(element.c);
    } else if (isSameRow) {
      flip(candidate.c);
      flip(element.c);
      reverse(2 * n - 1 - candidate.r);
      flip(element.c);
    } else {
      flip(element.c);
      reverse(candidate.r);
      flip(element.c);
    }
  }

  private void flip(int c) {
    for (int i = 0; i < n; i++) {
      int temp = matrix[i][c];
      matrix[i][c] = matrix[2 * n - 1 - i][c];
      matrix[2 * n - 1 - i][c] = temp;
    }
  }

  private void reverse(int r) {
    for (int i = 0; i < n; i++) {
      int temp = matrix[r][i];
      matrix[r][i] = matrix[r][2 * n - 1 - i];
      matrix[r][2 * n - 1 - i] = temp;
    }
  }

  private Cell getMaximumCandidateCell(int r, int c) {
    Cell cell = new Cell(r, c);

    if (matrix[cell.r][cell.c] < matrix[r][2 * n - 1 - c]) {
      cell.r = r;
      cell.c = 2 * n - 1 - c;
    }

    if (matrix[cell.r][cell.c] < matrix[2 * n - 1 - r][c]) {
      cell.r = 2 * n - 1 - r;
      cell.c = c;
    }

    if (matrix[cell.r][cell.c] < matrix[2 * n - 1 - r][2 * n - 1 - c]) {
      cell.r = 2 * n - 1 - r;
      cell.c = 2 * n - 1 - c;
    }

    return cell;
  }

  private void printTable(final int[][] table) {
    for (final int[] row : table) {
      for (final int i : row) {
        System.out.print(i + "\t");
      }
      System.out.println();
    }
    System.out.println();
  }
}

class Cell {
  int r;
  int c;

  Cell(int r, int c) {
    this.r = r;
    this.c = c;
  }
}
