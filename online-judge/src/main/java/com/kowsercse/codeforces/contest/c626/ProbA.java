package com.kowsercse.codeforces.contest.c626;

import java.util.Scanner;

/**
 * @author kowsercse@gmail.com
 */
public class ProbA {
  private static int getTotalCommands(final int totalCommand, final String commands) {
    int total = 0;
    int x = 0;
    int y = 0;

    for (int i = 0; i < totalCommand; i++) {
      final char command = commands.charAt(i);
      if (command == 'L') {
        x--;
      } else if (command == 'R') {
        x++;
      } else if (command == 'U') {
        y++;
      } else if (command == 'D') {
        y--;
      }
    }

    for (int start = 0; start < totalCommand; start++) {
      int X = x;
      int Y = y;

      for (int i = start; i < totalCommand; i++) {
        final char command = commands.charAt(i);

        if (command == 'L') {
          X--;
        } else if (command == 'R') {
          X++;
        } else if (command == 'U') {
          Y++;
        } else if (command == 'D') {
          Y--;
        }

        if (X == x && Y == y) {
          total++;
        }
      }
    }

    return total;
  }

  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    while (scanner.hasNextInt()) {
      final int totalCommand = scanner.nextInt();
      final String commands = scanner.next();

      final int totalCommands = getTotalCommands(totalCommand, commands);
      System.out.println(totalCommands);
    }
  }
}
