package com.kowsercse.codeforces.contest.c626;

import java.util.Arrays;
import java.util.Scanner;

/**
 * @author kowsercse@gmail.com
 */
public class ProbB {

  private static final int MAX = 200;
  private static Boolean[][][] red = new Boolean[MAX + 1][MAX + 1][MAX + 1];
  private static Boolean[][][] green = new Boolean[MAX + 1][MAX + 1][MAX + 1];
  private static Boolean[][][] blue = new Boolean[MAX + 1][MAX + 1][MAX + 1];

  private static void buildTable() {
    red[0][0][0] = green[0][0][0] = blue[0][0][0] = false;

    for (int i = 0; i < MAX; i++) {
      red[i][0][0] = true;
      red[0][i][0] = false;
      red[0][0][i] = false;

      green[i][0][0] = false;
      green[0][i][0] = true;
      green[0][0][i] = false;

      blue[i][0][0] = false;
      blue[0][i][0] = false;
      blue[0][0][i] = true;
    }

    red[0][1][1] = true;
    blue[0][1][1] = false;
    green[0][1][1] = false;

    red[1][0][1] = false;
    green[1][0][1] = true;
    blue[1][0][1] = false;

    red[1][1][0] = false;
    green[1][1][0] = false;
    blue[1][1][0] = true;
  }

  private static String solve(final String cards, final int length) {
    int totalRed = 0;
    int totalBlue = 0;
    int totalGreen = 0;

    for (int i = 0; i < length; i++) {
      if (cards.charAt(i) == 'R') {
        totalRed++;
      } else if (cards.charAt(i) == 'B') {
        totalBlue++;
      } else if (cards.charAt(i) == 'G') {
        totalGreen++;
      }
    }

    String answer = "";
    if (isBluePossible(totalRed, totalGreen, totalBlue)) {
      answer += "B";
    }
    if (isGreenPossible(totalRed, totalGreen, totalBlue)) {
      answer += "G";
    }
    if (isRedPossible(totalRed, totalGreen, totalBlue)) {
      answer += "R";
    }

    return answer;
  }

  private static boolean isRedPossible(
      final int totalRed, final int totalGreen, final int totalBlue) {
    if (totalRed < 0 || totalGreen < 0 || totalBlue < 0) {
      return false;
    }
    if (red[totalRed][totalGreen][totalBlue] != null) {
      return red[totalRed][totalGreen][totalBlue];
    }

    red[totalRed][totalGreen][totalBlue] =
        (totalRed > 1 && isRedPossible(totalRed - 1, totalGreen, totalBlue))
            || (totalGreen > 1 && isRedPossible(totalRed, totalGreen - 1, totalBlue))
            || (totalBlue > 1 && isRedPossible(totalRed, totalGreen, totalBlue - 1))
            || isRedPossible(totalRed + 1, totalGreen - 1, totalBlue - 1)
            || isRedPossible(totalRed - 1, totalGreen + 1, totalBlue - 1)
            || isRedPossible(totalRed - 1, totalGreen - 1, totalBlue + 1);
    return red[totalRed][totalGreen][totalBlue];
  }

  private static boolean isGreenPossible(
      final int totalRed, final int totalGreen, final int totalBlue) {
    if (totalRed < 0 || totalGreen < 0 || totalBlue < 0) {
      return false;
    }
    if (green[totalRed][totalGreen][totalBlue] != null) {
      return green[totalRed][totalGreen][totalBlue];
    }

    green[totalRed][totalGreen][totalBlue] =
        (totalRed > 1 && isGreenPossible(totalRed - 1, totalGreen, totalBlue))
            || (totalGreen > 1 && isGreenPossible(totalRed, totalGreen - 1, totalBlue))
            || (totalBlue > 1 && isGreenPossible(totalRed, totalGreen, totalBlue - 1))
            || isGreenPossible(totalRed + 1, totalGreen - 1, totalBlue - 1)
            || isGreenPossible(totalRed - 1, totalGreen + 1, totalBlue - 1)
            || isGreenPossible(totalRed - 1, totalGreen - 1, totalBlue + 1);
    return green[totalRed][totalGreen][totalBlue];
  }

  private static boolean isBluePossible(
      final int totalRed, final int totalGreen, final int totalBlue) {
    if (totalRed < 0 || totalGreen < 0 || totalBlue < 0) {
      return false;
    }
    if (blue[totalRed][totalGreen][totalBlue] != null) {
      return blue[totalRed][totalGreen][totalBlue];
    }

    blue[totalRed][totalGreen][totalBlue] =
        (totalRed > 1 && isBluePossible(totalRed - 1, totalGreen, totalBlue))
            || (totalGreen > 1 && isBluePossible(totalRed, totalGreen - 1, totalBlue))
            || (totalBlue > 1 && isBluePossible(totalRed, totalGreen, totalBlue - 1))
            || isBluePossible(totalRed + 1, totalGreen - 1, totalBlue - 1)
            || isBluePossible(totalRed - 1, totalGreen + 1, totalBlue - 1)
            || isBluePossible(totalRed - 1, totalGreen - 1, totalBlue + 1);
    return blue[totalRed][totalGreen][totalBlue];
  }

  public static void solve(int n, String s) {
    int[] cnt = new int[3];

    for (int i = 0; i < n; ++i) ++cnt["BGR".indexOf(s.charAt(i))];
    boolean[] can = new boolean[3];
    int numZero = 0;
    for (int x : cnt) if (x == 0) ++numZero;
    if (numZero == 0) {
      Arrays.fill(can, true);
    } else if (numZero == 2) {
      for (int i = 0; i < 3; ++i) if (cnt[i] > 0) can[i] = true;
    } else {
      int sum = 0;
      for (int x : cnt) sum += x;
      for (int i = 0; i < 3; ++i) {
        can[i] = cnt[i] != sum - 1;
      }
    }
    for (int i = 0; i < 3; ++i)
      if (can[i]) {
        System.out.print("BGR".charAt(i));
      }
    System.out.println();
  }

  public static void main(String[] args) {
    buildTable();
    Scanner scanner = new Scanner(System.in);

    while (scanner.hasNextInt()) {
      final int length = scanner.nextInt();
      final String cards = scanner.next();
      //      final String answer = solve(cards, length);
      solve(length, cards);
      //      System.out.println(answer);
    }
  }
}
