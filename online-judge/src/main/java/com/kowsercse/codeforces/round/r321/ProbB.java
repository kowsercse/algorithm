package com.kowsercse.codeforces.round.r321;

import java.io.PrintStream;
import java.util.*;

public class ProbB {
  private final Scanner scanner;
  private final PrintStream out;

  public ProbB(final Scanner scanner, final PrintStream out) {
    this.scanner = scanner;
    this.out = out;
  }

  private ProbB() {
    this(new Scanner(System.in), System.out);
  }

  public void solve() {
    long n = scanner.nextLong();
    long d = scanner.nextLong();

    final ArrayList<Friend> friends = new ArrayList<>((int) n);
    for (int i = 0; i < n; i++) {
      try {

        final Friend friend = new Friend();
        friend.m = scanner.nextInt();
        friend.s = scanner.nextInt();
        friends.add(friend);
      } catch (Exception ex) {
        System.out.println("i = " + i);
        throw ex;
      }
    }
    Collections.sort(friends);

    int start = 0, currentFactor = friends.get(0).s, maxFactor = friends.get(0).s;

    for (int i = 1; i < n; i++) {
      final Friend endFriend = friends.get(i);
      while (Math.abs(endFriend.m - friends.get(start).m) >= d && start != i) {
        currentFactor -= friends.get(start).s;
        start++;
      }

      currentFactor += endFriend.s;
      if (currentFactor > maxFactor) {
        maxFactor = currentFactor;
      }
    }

    out.println(maxFactor);
  }

  public static void main(String[] args) {
    new ProbB().solve();
  }

  private class Friend implements Comparable<Friend> {
    int m;
    int s;

    @Override
    public int compareTo(final Friend other) {
      return Integer.compare(m, other.m);
    }

    @Override
    public String toString() {
      return "" + m + " " + s;
    }
  }
}
