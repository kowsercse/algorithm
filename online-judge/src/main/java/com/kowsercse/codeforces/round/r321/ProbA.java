package com.kowsercse.codeforces.round.r321;

import java.io.PrintStream;
import java.util.Scanner;

public class ProbA {
  private final Scanner scanner;
  private final PrintStream out;

  public ProbA(final Scanner scanner, final PrintStream out) {
    this.scanner = scanner;
    this.out = out;
  }

  private ProbA() {
    this(new Scanner(System.in), System.out);
  }

  public void solve() {
    long totalNumber = scanner.nextLong() - 1;
    long previousNumber = scanner.nextLong();
    long currentNumber, currentIncrement = 1, maxIncrement = 0;
    if (totalNumber == 0) {
      out.println(1);
      return;
    }
    do {
      currentNumber = scanner.nextLong();
      if (currentNumber >= previousNumber) {
        currentIncrement++;
      } else {
        if (currentIncrement > maxIncrement) {
          maxIncrement = currentIncrement;
        }
        currentIncrement = 1;
      }

      previousNumber = currentNumber;
    } while (--totalNumber > 0);

    if (currentIncrement > maxIncrement) {
      maxIncrement = currentIncrement;
    }
    out.println(maxIncrement);
  }

  public static void main(String[] args) {
    new ProbA().solve();
  }
}
