package com.kowsercse.kickstarter;

import java.util.*;

public class HIndexCalculator {
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    int testCase = scanner.nextInt();
    for (int i = 0; i < testCase; i++) {
      calculateHIndex(scanner, i + 1);
    }
  }

  private static void calculateHIndex(Scanner scanner, int testCase) {
    StringJoiner stringJoiner = new StringJoiner(" ", "Case #" + testCase + ": ", "");
    int hIndex = 0;
    int inputSize = scanner.nextInt();

    int totalPaperConsidered = 0;
    TreeMap<Integer, Integer> publishedByCitation = new TreeMap<>();

    while (inputSize-- > 0) {
      int citation = scanner.nextInt();
      if (citation > hIndex) {
        totalPaperConsidered++;
        publishedByCitation.compute(citation, (key, value) -> value == null ? 1 : value + 1);
      }

      hIndex =
          Math.max(
              hIndex,
              Math.min(
                  totalPaperConsidered,
                  publishedByCitation.isEmpty() ? 0 : publishedByCitation.firstEntry().getKey()));
      stringJoiner.add(Integer.toString(hIndex));

      NavigableMap<Integer, Integer> headMap = publishedByCitation.headMap(hIndex, true);
      for (Integer value : headMap.values()) {
        totalPaperConsidered -= value;
      }
      headMap.clear();
    }

    System.out.println(stringJoiner);
  }
}
