package com.kowsercse.uva.V109;

import java.util.PriorityQueue;
import java.util.Scanner;

public class P10954 {
  public static void main(String[] args) {
    final Scanner scanner = new Scanner(System.in);
    int totalNumbers = scanner.nextInt();
    while (totalNumbers != 0) {
      final PriorityQueue<Integer> priorityQueue = new PriorityQueue<>();
      while (totalNumbers-- > 0) {
        priorityQueue.add(scanner.nextInt());
      }

      Long totalCost = 0L;
      while (priorityQueue.size() > 1) {
        final Integer latestCost = priorityQueue.poll() + priorityQueue.poll();
        priorityQueue.add(latestCost);

        totalCost += latestCost;
      }
      System.out.println(totalCost);

      totalNumbers = scanner.nextInt();
    }
  }
}
