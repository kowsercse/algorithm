package com.kowsercse.uva.V109;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class P10901 {
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    int testCase = scanner.nextInt();

    while (testCase-- > 0) {
      Problem problem = new Problem(scanner);
      problem.solve();
      problem.printSolution();
      if (testCase != 0) {
        System.out.println();
      }
    }
  }
}

class Problem {

  private final int ferryCapacity;
  private final int travelTime;
  private int currentTime = 0;
  private String currentBank = "left";
  private int availableCapacity;
  private int[] arrivalTime;

  private final Queue<CarArrival> leftQueue = new LinkedList<>();
  private final Queue<CarArrival> rightQueue = new LinkedList<>();
  private Queue<CarArrival> currentQueue = leftQueue;

  public Problem(Scanner scanner) {
    ferryCapacity = scanner.nextInt();
    travelTime = scanner.nextInt();
    availableCapacity = ferryCapacity;

    int totalCar = scanner.nextInt();
    for (int i = 0; i < totalCar; i++) {
      CarArrival carArrival = new CarArrival(i, scanner.nextInt(), scanner.next());
      Queue<CarArrival> arrivalQueue = "left".equals(carArrival.bank) ? leftQueue : rightQueue;
      arrivalQueue.add(carArrival);
    }
    arrivalTime = new int[totalCar];
  }

  void solve() {
    while (hasMoreCar()) {
      CarArrival earliestArrival = min().peek();
      if (availableCapacity == ferryCapacity && earliestArrival.time > currentTime) {
        currentTime = earliestArrival.time;
        if (!currentBank.equals(earliestArrival.bank)) {
          switchBank();
        }
      } else if (canLoadFromCurrentBank()) {
        availableCapacity--;
        CarArrival carArrival = currentQueue.poll();
        arrivalTime[carArrival.index] = currentTime + travelTime;
        if (availableCapacity == 0) {
          switchBank();
        }
      } else {
        switchBank();
      }
    }
  }

  private boolean canLoadFromCurrentBank() {
    return availableCapacity > 0
        && !currentQueue.isEmpty()
        && currentQueue.peek().time <= currentTime;
  }

  private void switchBank() {
    availableCapacity = ferryCapacity;
    currentBank = "left".equals(currentBank) ? "right" : "left";
    currentQueue = "left".equals(currentBank) ? leftQueue : rightQueue;
    currentTime += travelTime;
  }

  private Queue<CarArrival> min() {
    if (leftQueue.isEmpty()) {
      return rightQueue;
    } else if (rightQueue.isEmpty()) {
      return leftQueue;
    }
    CarArrival leftCar = leftQueue.peek();
    CarArrival rightCar = rightQueue.peek();
    if (leftCar.time == rightCar.time) {
      return currentBank.equals(leftCar.bank) ? leftQueue : rightQueue;
    }

    return leftCar.time < rightCar.time ? leftQueue : rightQueue;
  }

  private boolean hasMoreCar() {
    return !leftQueue.isEmpty() || !rightQueue.isEmpty();
  }

  public void printSolution() {
    for (int i : arrivalTime) {
      System.out.println(i);
    }
  }
}

class CarArrival {
  int index;
  int time;
  String bank;

  public CarArrival(int i, int time, String bank) {
    this.index = i;
    this.time = time;
    this.bank = bank;
  }
}
