package com.kowsercse.uva.V110;

import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.concurrent.atomic.AtomicInteger;

public class P11094 {

  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);

    Graph graph = new Graph(scanner);
    System.out.println(graph.solve());

    while (scanner.hasNext()) {
      scanner.nextLine();
      graph = new Graph(scanner);
      System.out.println(graph.solve());
    }
  }
}

class Graph {
  private final int row;
  private final int col;
  private final String[] world;
  private final boolean[][] visited;
  final int startX;
  final int startY;
  private char land;

  public Graph(Scanner scanner) {
    final String line = scanner.nextLine();
    StringTokenizer tokenizer = new StringTokenizer(line);
    row = Integer.parseInt(tokenizer.nextToken());
    col = Integer.parseInt(tokenizer.nextToken());
    visited = new boolean[row][col];

    world = new String[row];
    for (int r = 0; r < row; r++) {
      world[r] = scanner.nextLine();
    }
    tokenizer = new StringTokenizer(scanner.nextLine());
    startX = Integer.parseInt(tokenizer.nextToken());
    startY = Integer.parseInt(tokenizer.nextToken());
  }

  int solve() {
    land = world[startX].charAt(startY);

    AtomicInteger continentSize = new AtomicInteger();
    dfs(startX, startY, continentSize);

    int maxContinentSize = 0;
    for (int r = 0; r < row; r++) {
      for (int c = 0; c < col; c++) {
        continentSize = new AtomicInteger();
        dfs(r, c, continentSize);
        maxContinentSize = Math.max(maxContinentSize, continentSize.get());
      }
    }
    return maxContinentSize;
  }

  private void dfs(int x, int y, AtomicInteger continentSize) {
    if (x >= row) {
      return;
    } else if (x < 0) {
      return;
    }
    if (y >= col) {
      y = 0;
    } else if (y < 0) {
      y = col - 1;
    }

    if (!isLand(x, y) || visited[x][y]) {
      return;
    }

    visited[x][y] = true;
    continentSize.incrementAndGet();

    dfs(x + 1, y, continentSize);
    dfs(x - 1, y, continentSize);
    dfs(x, y + 1, continentSize);
    dfs(x, y - 1, continentSize);
  }

  private boolean isLand(int x, int y) {
    return world[x].charAt(y) == land;
  }
}
