package com.kowsercse.uva.V110;

import java.util.*;

public class P11085 {

  public static final int N_QUEEN = 8;

  public static void main(String[] args) {
    int testCase = 1;
    Scanner scanner = new Scanner(System.in);
    while (scanner.hasNextInt()) {
      int[] rowPositions = new int[N_QUEEN];
      for (int i = 0; i < N_QUEEN; i++) {
        rowPositions[i] = scanner.nextInt();
      }

      int minimumMovesRequired = Integer.MAX_VALUE;

      for (int[] solution : MemorizedSolution.SOLUTION) {
        int movesRequired = 0;
        for (int i = 0; i < N_QUEEN; i++) {
          if (rowPositions[i] != solution[i]) {
            movesRequired++;
            if (movesRequired >= minimumMovesRequired) {
              break;
            }
          }
        }

        minimumMovesRequired = Math.min(minimumMovesRequired, movesRequired);
      }

      System.out.println("Case " + testCase++ + ": " + minimumMovesRequired);
    }
  }
}

class NQueenSolution {
  private final int nQueen;

  private LinkedList<Integer> solution = new LinkedList<>();
  private List<List<Integer>> solutions = new LinkedList<>();
  private boolean[] row;
  private boolean[] column;
  private boolean[] leftDiagonal;
  private boolean[] rightDiagonal;

  NQueenSolution(int nQueen) {
    this.nQueen = nQueen;
    row = new boolean[nQueen];
    column = new boolean[nQueen];
    leftDiagonal = new boolean[2 * nQueen - 1];
    rightDiagonal = new boolean[2 * nQueen - 1];
  }

  void solve() {
    setQueen(0);
    StringJoiner memorizedSolutionsJoiner = new StringJoiner(",\n", "{", "}");
    for (List<Integer> solution : solutions) {
      StringJoiner solutionsJoiner = new StringJoiner(",", "{", "}");
      for (Integer row : solution) {
        solutionsJoiner.add(String.valueOf(row + 1));
      }
      memorizedSolutionsJoiner.add(solutionsJoiner.toString());
    }
    System.out.println(memorizedSolutionsJoiner);
  }

  private void setQueen(int column) {
    if (column == nQueen) {
      solutions.add(new LinkedList<>(solution));
      return;
    }

    for (int row = 0; row < nQueen; row++) {
      if (canSetQueen(row, column)) {
        setValues(row, column);
        setQueen(column + 1);
        unsetValues(row, column);
      }
    }
  }

  private void setValues(int row, int column) {
    this.solution.addLast(row);
    this.row[row] = true;
    this.column[column] = true;
    this.leftDiagonal[row + column] = true;
    this.rightDiagonal[nQueen - column - 1 + row] = true;
  }

  private void unsetValues(int row, int column) {
    this.solution.removeLast();
    this.row[row] = false;
    this.column[column] = false;
    this.leftDiagonal[row + column] = false;
    this.rightDiagonal[nQueen - column - 1 + row] = false;
  }

  private boolean canSetQueen(int row, int column) {
    return !this.row[row]
        && !this.column[column]
        && !leftDiagonal[row + column]
        && !rightDiagonal[nQueen - column - 1 + row];
  }
}

class MemorizedSolution {
  public static final int[][] SOLUTION = {
    {1, 5, 8, 6, 3, 7, 2, 4},
    {1, 6, 8, 3, 7, 4, 2, 5},
    {1, 7, 4, 6, 8, 2, 5, 3},
    {1, 7, 5, 8, 2, 4, 6, 3},
    {2, 4, 6, 8, 3, 1, 7, 5},
    {2, 5, 7, 1, 3, 8, 6, 4},
    {2, 5, 7, 4, 1, 8, 6, 3},
    {2, 6, 1, 7, 4, 8, 3, 5},
    {2, 6, 8, 3, 1, 4, 7, 5},
    {2, 7, 3, 6, 8, 5, 1, 4},
    {2, 7, 5, 8, 1, 4, 6, 3},
    {2, 8, 6, 1, 3, 5, 7, 4},
    {3, 1, 7, 5, 8, 2, 4, 6},
    {3, 5, 2, 8, 1, 7, 4, 6},
    {3, 5, 2, 8, 6, 4, 7, 1},
    {3, 5, 7, 1, 4, 2, 8, 6},
    {3, 5, 8, 4, 1, 7, 2, 6},
    {3, 6, 2, 5, 8, 1, 7, 4},
    {3, 6, 2, 7, 1, 4, 8, 5},
    {3, 6, 2, 7, 5, 1, 8, 4},
    {3, 6, 4, 1, 8, 5, 7, 2},
    {3, 6, 4, 2, 8, 5, 7, 1},
    {3, 6, 8, 1, 4, 7, 5, 2},
    {3, 6, 8, 1, 5, 7, 2, 4},
    {3, 6, 8, 2, 4, 1, 7, 5},
    {3, 7, 2, 8, 5, 1, 4, 6},
    {3, 7, 2, 8, 6, 4, 1, 5},
    {3, 8, 4, 7, 1, 6, 2, 5},
    {4, 1, 5, 8, 2, 7, 3, 6},
    {4, 1, 5, 8, 6, 3, 7, 2},
    {4, 2, 5, 8, 6, 1, 3, 7},
    {4, 2, 7, 3, 6, 8, 1, 5},
    {4, 2, 7, 3, 6, 8, 5, 1},
    {4, 2, 7, 5, 1, 8, 6, 3},
    {4, 2, 8, 5, 7, 1, 3, 6},
    {4, 2, 8, 6, 1, 3, 5, 7},
    {4, 6, 1, 5, 2, 8, 3, 7},
    {4, 6, 8, 2, 7, 1, 3, 5},
    {4, 6, 8, 3, 1, 7, 5, 2},
    {4, 7, 1, 8, 5, 2, 6, 3},
    {4, 7, 3, 8, 2, 5, 1, 6},
    {4, 7, 5, 2, 6, 1, 3, 8},
    {4, 7, 5, 3, 1, 6, 8, 2},
    {4, 8, 1, 3, 6, 2, 7, 5},
    {4, 8, 1, 5, 7, 2, 6, 3},
    {4, 8, 5, 3, 1, 7, 2, 6},
    {5, 1, 4, 6, 8, 2, 7, 3},
    {5, 1, 8, 4, 2, 7, 3, 6},
    {5, 1, 8, 6, 3, 7, 2, 4},
    {5, 2, 4, 6, 8, 3, 1, 7},
    {5, 2, 4, 7, 3, 8, 6, 1},
    {5, 2, 6, 1, 7, 4, 8, 3},
    {5, 2, 8, 1, 4, 7, 3, 6},
    {5, 3, 1, 6, 8, 2, 4, 7},
    {5, 3, 1, 7, 2, 8, 6, 4},
    {5, 3, 8, 4, 7, 1, 6, 2},
    {5, 7, 1, 3, 8, 6, 4, 2},
    {5, 7, 1, 4, 2, 8, 6, 3},
    {5, 7, 2, 4, 8, 1, 3, 6},
    {5, 7, 2, 6, 3, 1, 4, 8},
    {5, 7, 2, 6, 3, 1, 8, 4},
    {5, 7, 4, 1, 3, 8, 6, 2},
    {5, 8, 4, 1, 3, 6, 2, 7},
    {5, 8, 4, 1, 7, 2, 6, 3},
    {6, 1, 5, 2, 8, 3, 7, 4},
    {6, 2, 7, 1, 3, 5, 8, 4},
    {6, 2, 7, 1, 4, 8, 5, 3},
    {6, 3, 1, 7, 5, 8, 2, 4},
    {6, 3, 1, 8, 4, 2, 7, 5},
    {6, 3, 1, 8, 5, 2, 4, 7},
    {6, 3, 5, 7, 1, 4, 2, 8},
    {6, 3, 5, 8, 1, 4, 2, 7},
    {6, 3, 7, 2, 4, 8, 1, 5},
    {6, 3, 7, 2, 8, 5, 1, 4},
    {6, 3, 7, 4, 1, 8, 2, 5},
    {6, 4, 1, 5, 8, 2, 7, 3},
    {6, 4, 2, 8, 5, 7, 1, 3},
    {6, 4, 7, 1, 3, 5, 2, 8},
    {6, 4, 7, 1, 8, 2, 5, 3},
    {6, 8, 2, 4, 1, 7, 5, 3},
    {7, 1, 3, 8, 6, 4, 2, 5},
    {7, 2, 4, 1, 8, 5, 3, 6},
    {7, 2, 6, 3, 1, 4, 8, 5},
    {7, 3, 1, 6, 8, 5, 2, 4},
    {7, 3, 8, 2, 5, 1, 6, 4},
    {7, 4, 2, 5, 8, 1, 3, 6},
    {7, 4, 2, 8, 6, 1, 3, 5},
    {7, 5, 3, 1, 6, 8, 2, 4},
    {8, 2, 4, 1, 7, 5, 3, 6},
    {8, 2, 5, 3, 1, 7, 4, 6},
    {8, 3, 1, 6, 2, 5, 7, 4},
    {8, 4, 1, 3, 6, 2, 7, 5}
  };
}
