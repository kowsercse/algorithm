package com.kowsercse.uva.V110;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class P11034 {
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    int testCase = scanner.nextInt();
    while (testCase-- > 0) {
      int ferryLength = scanner.nextInt() * 100;
      int totalCar = scanner.nextInt();
      Queue<CarArrival> leftQueue = new LinkedList<>();
      Queue<CarArrival> rightQueue = new LinkedList<>();

      while (totalCar-- > 0) {
        CarArrival carArrival = new CarArrival(scanner.nextInt(), scanner.next());
        Queue<CarArrival> arrivalQueue = "left".equals(carArrival.bank) ? leftQueue : rightQueue;
        arrivalQueue.add(carArrival);
      }

      int totalCross = 0;
      while (!leftQueue.isEmpty() || !rightQueue.isEmpty()) {
        totalCross++;
        int remainingSpace = ferryLength;
        while (!leftQueue.isEmpty() && remainingSpace >= leftQueue.peek().carLength) {
          remainingSpace -= leftQueue.poll().carLength;
        }

        if (!leftQueue.isEmpty() || !rightQueue.isEmpty()) {
          remainingSpace = ferryLength;
          while (!rightQueue.isEmpty() && remainingSpace >= rightQueue.peek().carLength) {
            remainingSpace -= rightQueue.poll().carLength;
          }
          totalCross++;
        }
      }

      System.out.println(totalCross);
    }
  }
}

class CarArrival {
  int carLength;
  String bank;

  public CarArrival(int carLength, String bank) {
    this.carLength = carLength;
    this.bank = bank;
  }
}
