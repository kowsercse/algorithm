package com.kowsercse.uva.V110;

import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class P11057 {
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    while (scanner.hasNext()) {
      int totalBooks = scanner.nextInt();
      TreeMap<Integer, Boolean> bookPrices = new TreeMap<>();
      for (int i = 0; i < totalBooks; i++) {
        int price = scanner.nextInt();
        bookPrices.put(price, bookPrices.containsKey(price));
      }

      int budget = scanner.nextInt();
      int firstBookPrice = 0, secondBookPrice = 0;
      for (Map.Entry<Integer, Boolean> bookPriceEntry : bookPrices.entrySet()) {
        int bookPrice = bookPriceEntry.getKey();
        int secondBookBudget = budget - bookPrice;
        if (secondBookBudget < bookPrice) {
          break;
        }

        if (bookPrices.containsKey(secondBookBudget)) {
          if (bookPrice != secondBookBudget) {
            firstBookPrice = bookPrice;
            secondBookPrice = secondBookBudget;
          } else if (bookPriceEntry.getValue()) {
            firstBookPrice = bookPrice;
            secondBookPrice = secondBookBudget;
          }
        }
      }

      System.out.println(
          "Peter should buy books whose prices are "
              + firstBookPrice
              + " and "
              + secondBookPrice
              + ".\n");
    }
  }
}
