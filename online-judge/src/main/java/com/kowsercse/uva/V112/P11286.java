package com.kowsercse.uva.V112;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class P11286 {

  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    int totalStudent = scanner.nextInt();

    while (totalStudent != 0) {
      Map<CourseSet, Integer> courseSetCounter = new HashMap<>();

      int maxCounter = 0;
      for (int i = 0; i < totalStudent; i++) {
        CourseSet courseSet = new CourseSet(scanner);
        Integer counter =
            courseSetCounter.compute(courseSet, (key, value) -> value == null ? 1 : value + 1);
        if (counter > maxCounter) {
          maxCounter = counter;
        }
      }

      int totalMaxCourseCounterMatch = 0;
      for (int counter : courseSetCounter.values()) {
        if (counter == maxCounter) {
          totalMaxCourseCounterMatch++;
        }
      }

      System.out.println(maxCounter * totalMaxCourseCounterMatch);
      totalStudent = scanner.nextInt();
    }
  }
}

class CourseSet {

  private static final int[] primes = {2, 3, 5, 7, 9};
  private final List<Integer> courseIds = new ArrayList<>();
  private int hashCode;

  public CourseSet(Scanner scanner) {
    for (int i = 0; i < 5; i++) {
      courseIds.add(scanner.nextInt());
    }
    Collections.sort(courseIds);
    for (int i = 0; i < courseIds.size(); i++) {
      hashCode += primes[i] * courseIds.get(i);
    }
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CourseSet student = (CourseSet) o;

    for (int i = 0; i < 5; i++) {
      if (!courseIds.get(i).equals(student.courseIds.get(i))) {
        return false;
      }
    }

    return true;
  }

  @Override
  public int hashCode() {
    return hashCode;
  }
}
