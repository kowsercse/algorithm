package com.kowsercse.uva.V112;

import static java.lang.Integer.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class P11235 {
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    int totalEntries = scanner.nextInt();
    while (totalEntries > 0) {
      int totalQuery = scanner.nextInt();
      SegmentTree segmentTree = new SegmentTree(totalEntries);
      segmentTree.constructTree(scanner);

      segmentTree.answerQueries(scanner, totalQuery);
      totalEntries = scanner.nextInt();
    }
  }
}

class SegmentTree {

  private final int totalEntries;
  private Node root;

  public SegmentTree(int totalEntries) {
    this.totalEntries = totalEntries;
  }

  public void constructTree(Scanner scanner) {
    List<Node> processedEntries = readEntries(scanner);

    while (processedEntries.size() != 1) {
      int size = (processedEntries.size() + 1) / 2;
      List<Node> currentEntries = new ArrayList<>(size);

      for (int i = 0; i < size; i++) {
        Node right = i * 2 + 1 < processedEntries.size() ? processedEntries.get(i * 2 + 1) : null;
        currentEntries.add(createSummary(processedEntries.get(i * 2), right));
      }

      processedEntries = currentEntries;
    }

    root = processedEntries.get(0);
  }

  private Node createSummary(Node left, Node right) {
    if (right == null) {
      return left;
    }

    Node node = new Node();
    node.start = left.start;
    node.end = right.end;
    node.left = left;
    node.right = right;

    if (right.frequency > left.frequency) {
      node.frequency = right.frequency;
      node.value = right.value;
    } else {
      node.frequency = left.frequency;
      node.value = left.value;
    }

    return node;
  }

  private List<Node> readEntries(Scanner scanner) {
    List<Node> nodes = new ArrayList<>(totalEntries);

    Node node = new Node(1, scanner.nextInt());
    for (int i = 2; i <= totalEntries; i++) {
      int value = scanner.nextInt();

      if (value != node.value) {
        node.setEnd(i - 1);
        nodes.add(node);
        node = new Node(i, value);
      }
    }

    node.setEnd(totalEntries);
    nodes.add(node);

    return nodes;
  }

  public void answerQueries(Scanner scanner, int totalQuery) {
    for (int i = 0; i < totalQuery; i++) {
      int start = scanner.nextInt();
      int end = scanner.nextInt();
      int maximumFrequency = root.getMaximumFrequency(start, end);
      System.out.println(maximumFrequency);
    }
  }
}

class Node {
  int start;
  int end;
  int value;
  int frequency;

  Node left;
  Node right;

  Node() {}

  public Node(int start, int value) {
    this.start = start;
    this.value = value;
  }

  public void setEnd(int end) {
    this.end = end;
    frequency = end - start + 1;
  }

  int getMaximumFrequency(int start, int end) {
    if (start <= this.start && end >= this.end) {
      return this.frequency;
    } else if (start > this.end || end < this.start) {
      return Integer.MIN_VALUE;
    }

    if (left == null) {
      if (start <= this.start) {
        return min(end, this.end) - this.start + 1;
      } else if (end >= this.end) {
        return this.end - max(start, this.start) + 1;
      } else {
        return end - start + 1;
      }
    }

    int leftMax = left.getMaximumFrequency(start, end);
    int rightMax = right == null ? Integer.MIN_VALUE : right.getMaximumFrequency(start, end);
    return max(leftMax, rightMax);
  }

  @Override
  public String toString() {
    return "{start="
        + start
        + ", end="
        + end
        + ", value="
        + value
        + ", frequency="
        + frequency
        + '}';
  }
}
