package com.kowsercse.uva.V105;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class P10507 {

  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);

    while (scanner.hasNext()) {
      UnionFind brain = new UnionFind(scanner.nextInt());
      brain.processInput(scanner);
    }
  }
}

class UnionFind {

  Set<Character> availableNodes = new HashSet<>();
  char[] rootByNode = new char['Z' + 1];
  int[] totalAwakeConnection = new int['Z' + 1];
  boolean[] isAlive = new boolean['Z' + 1];
  Map<Character, Set<Character>> destinationsByNode = new HashMap<>();
  private final int totalNodes;

  public UnionFind(int totalNodes) {
    this.totalNodes = totalNodes;
  }

  char getRoot(char node) {
    if (rootByNode[node] == 0) {
      rootByNode[node] = node;
      return node;
    }

    char root = node;
    while (root != rootByNode[root]) {
      root = rootByNode[root];
    }

    char parent = rootByNode[node];
    while (node != root) {
      rootByNode[node] = root;
      node = parent;
      parent = rootByNode[parent];
    }

    return root;
  }

  void setRoot(char first, char second) {
    char firstRoot = getRoot(first);
    char secondRoot = getRoot(second);

    if (firstRoot >= secondRoot) {
      rootByNode[firstRoot] = secondRoot;
    } else {
      rootByNode[secondRoot] = firstRoot;
    }
  }

  void connect(char first, char second) {
    addNode(first);
    addNode(second);

    setRoot(first, second);
    destinationsByNode.computeIfAbsent(first, ignore -> new HashSet<>()).add(second);
    destinationsByNode.computeIfAbsent(second, ignore -> new HashSet<>()).add(first);
  }

  private void addNode(char first) {
    availableNodes.add(first);
    totalAwakeConnection[first] = 0;
  }

  void processInput(Scanner scanner) {
    int totalConnection = scanner.nextInt();
    String awakeNodes = scanner.next();

    List<Character> bfsQueue = bfsQueue(awakeNodes);
    while (totalConnection-- > 0) {
      String connection = scanner.next();
      connect(connection.charAt(0), connection.charAt(1));
    }

    if (totalNodes == 3) {
      System.out.println("WAKE UP IN, 0, YEARS");
      return;
    }

    char expectedRoot = getRoot(awakeNodes.charAt(0));
    if (availableNodes.size() < totalNodes
        || expectedRoot != getRoot(awakeNodes.charAt(1))
        || expectedRoot != getRoot(awakeNodes.charAt(2))
        || hasDifferentForest(expectedRoot)) {
      System.out.println("THIS BRAIN NEVER WAKES UP");
      return;
    }

    int year = 0;
    int totalAwakeNodes = 3;
    while (!bfsQueue.isEmpty()) {
      List<Character> tempBfsQueue = new LinkedList<>();
      for (char source : bfsQueue) {
        for (char destination : destinationsByNode.get(source)) {
          if (isAlive[destination]) {
            continue;
          }

          totalAwakeConnection[destination]++;
          if (totalAwakeConnection[destination] == 3) {
            isAlive[destination] = true;
            totalAwakeNodes++;
            tempBfsQueue.add(destination);
          }
          destinationsByNode.get(destination).remove(source);
        }

        destinationsByNode.put(source, Collections.emptySet());
      }
      bfsQueue = tempBfsQueue;
      year++;
    }

    year--;
    if (totalAwakeNodes == totalNodes) {
      System.out.println("WAKE UP IN, " + year + ", YEARS");
    } else {
      System.out.println("THIS BRAIN NEVER WAKES UP");
    }
  }

  private List<Character> bfsQueue(String awakeNodes) {
    List<Character> bfsQueue = new LinkedList<>();
    for (int i = 0; i < awakeNodes.length(); i++) {
      char node = awakeNodes.charAt(i);
      bfsQueue.add(node);
      addNode(node);
      isAlive[node] = true;
    }
    return bfsQueue;
  }

  private boolean hasDifferentForest(char expectedRoot) {
    for (char node : availableNodes) {
      if (rootByNode[node] != expectedRoot) {
        return true;
      }
    }

    return false;
  }
}
