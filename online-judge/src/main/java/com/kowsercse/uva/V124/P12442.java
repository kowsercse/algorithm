package com.kowsercse.uva.V124;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class P12442 {

  public static void main(String[] args) throws IOException {
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    final int testCase = Integer.parseInt(reader.readLine());
    for (int i = 1; i <= testCase; i++) {
      int totalNode = Integer.parseInt(reader.readLine());

      final Graph graph = new Graph(totalNode);
      graph.readEdges(reader);
      graph.solve();

      System.err.println(totalNode + "," + graph.count + "," + graph.count / totalNode);
      System.out.println("Case " + i + ": " + (graph.maxNode.id + 1));
    }
  }
}

class Graph {
  private final Map<String, Node> nodes = new HashMap<>();
  private final int totalNode;
  Node maxNode;
  int count;

  public Graph(int totalNode) {
    this.totalNode = totalNode;
  }

  void solve() {
    for (Node node : nodes.values()) {
      calculateReachableNodeCount(node);
    }
  }

  private void calculateReachableNodeCount(Node startNode) {
    count++;
    if (startNode.totalReachableNode != 0) {
      return;
    }

    int[] totalReachableNode = new int[totalNode];
    int maximumReachable = 0;
    Node node = startNode;
    while (!node.visited) {
      totalReachableNode[node.id] = ++maximumReachable;
      node.visited = true;

      node = node.destination;
      count++;
    }

    if (node.totalReachableNode == 0) {
      int cycleLength =
          node.isCycle
              ? node.totalReachableNode
              : maximumReachable - totalReachableNode[node.id] + 1;

      fillCycleLength(node, cycleLength);
      fillPrefixLength(startNode, node, maximumReachable);
    } else {
      fillPrefixLength(startNode, node, maximumReachable + node.totalReachableNode);
    }
  }

  private void fillCycleLength(Node cycleStart, int cycleLength) {
    if (cycleStart.id == cycleStart.destination.id) {
      count++;
      cycleStart.setTotalReachableNode(cycleLength);
      cycleStart.isCycle = true;
      return;
    }

    Node node = cycleStart;
    do {
      count++;
      node.setTotalReachableNode(cycleLength);
      node.isCycle = true;

      node = node.destination;
    } while (node != cycleStart);
  }

  private void fillPrefixLength(Node startNode, Node cycleStart, int maximumReachable) {
    Node node = startNode;
    while (node != cycleStart) {
      count++;
      node.setTotalReachableNode(maximumReachable--);
      node = node.destination;
    }
  }

  void readEdges(BufferedReader reader) throws IOException {
    for (int i = 0; i < totalNode; i++) {
      StringTokenizer tokenizer = new StringTokenizer(reader.readLine());
      Node source =
          nodes.computeIfAbsent(tokenizer.nextToken(), id -> new Node(Integer.parseInt(id) - 1));
      source.destination =
          nodes.computeIfAbsent(tokenizer.nextToken(), id -> new Node(Integer.parseInt(id) - 1));
    }
  }

  class Node {
    final int id;
    public boolean visited;
    Node destination;
    boolean isCycle;
    private int totalReachableNode;

    public void setTotalReachableNode(int totalReachableNode) {
      this.totalReachableNode = totalReachableNode;
      if (maxNode == null) {
        maxNode = this;
      } else if (maxNode.totalReachableNode < this.totalReachableNode) {
        maxNode = this;
      } else if (maxNode.totalReachableNode == this.totalReachableNode && maxNode.id > this.id) {
        maxNode = this;
      }
    }

    public Node(int id) {
      this.id = id;
    }

    @Override
    public String toString() {
      return "Node{id="
          + id
          + ", isCycle="
          + isCycle
          + ", totalReachableNode="
          + totalReachableNode
          + '}';
    }
  }
}
