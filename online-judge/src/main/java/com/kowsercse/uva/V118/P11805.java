package com.kowsercse.uva.V118;

import java.util.Scanner;

public class P11805 {

  private final Scanner scanner = new Scanner(System.in);

  public void solve() {
    long testCase = scanner.nextLong();
    long N, K, P;
    long selected;
    for (int i = 1; i <= testCase; i++) {
      N = scanner.nextLong();
      K = scanner.nextLong();
      P = scanner.nextLong();

      selected = (K + P) % N;
      if (selected == 0) {
        selected = N;
      }
      System.out.println("Case " + i + ": " + selected);
    }
  }

  public static void main(String[] args) {
    new P11805().solve();
  }
}
