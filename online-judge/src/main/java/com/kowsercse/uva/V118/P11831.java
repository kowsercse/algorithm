package com.kowsercse.uva.V118;

import java.util.Scanner;

public class P11831 {

  public static void main(String[] args) {
    final Scanner scanner = new Scanner(System.in);

    int row = scanner.nextInt();
    int column = scanner.nextInt();
    int totalInstruction = scanner.nextInt();

    while (row != 0 || column != 0 || totalInstruction != 0) {
      final Graph graph = new Graph(row, column, scanner);

      final String instructions = scanner.nextLine();
      for (int i = 0; i < totalInstruction; i++) {
        graph.startingPosition.applyCommand(instructions.charAt(i));
      }
      System.out.println(graph.totalStickerCollected);

      row = scanner.nextInt();
      column = scanner.nextInt();
      totalInstruction = scanner.nextInt();
    }
  }
}

class Graph {
  private static final char PILLAR = '#';
  private static final char STICKER = '*';
  private static final char[] CLOCKWISE_DIRECTIONS = new char[] {'N', 'L', 'S', 'O'};
  private static final char[] DIRECTIONS = new char[] {'N', 'E', 'S', 'W'};
  private static final int[][] FORWARD_DELTA = new int[][] {{-1, 0}, {0, 1}, {1, 0}, {0, -1}};
  private static final int[] DIRECTION_DELTA = new int[] {1, -1};
  private final int row;
  private final int column;
  private final String[] matrix;
  private final boolean[][] stickerCollected;
  Position startingPosition;
  int totalStickerCollected = 0;

  public Graph(int row, int column, Scanner scanner) {
    this.row = row;
    this.column = column;
    this.matrix = new String[row];
    this.stickerCollected = new boolean[row][column];

    readInput(row, scanner);
  }

  private boolean isValid(int x, int y) {
    return x < row && x >= 0 && y < column && y >= 0 && matrix[x].charAt(y) != PILLAR;
  }

  private void readInput(int row, Scanner scanner) {
    scanner.nextLine();
    for (int r = 0; r < row; r++) {
      matrix[r] = scanner.nextLine();

      if (startingPosition == null) {
        readStartingPosition(r);
      }
    }
  }

  private void readStartingPosition(int r) {
    for (char direction : CLOCKWISE_DIRECTIONS) {
      final int c = matrix[r].indexOf(direction);
      if (c != -1) {
        startingPosition = new Position(r, c, direction);
        return;
      }
    }
  }

  class Position {
    int x;
    int y;
    int currentDirection;

    public Position(int r, int c, char direction) {
      this.x = r;
      this.y = c;
      for (int i = 0; i < CLOCKWISE_DIRECTIONS.length; i++) {
        if (direction == CLOCKWISE_DIRECTIONS[i]) {
          currentDirection = i;
        }
      }
    }

    public void applyCommand(char command) {
      if ('F' == command) {
        int newX = x + FORWARD_DELTA[currentDirection][0];
        int newY = y + FORWARD_DELTA[currentDirection][1];

        if (isValid(newX, newY)) {
          x = newX;
          y = newY;

          if (STICKER == matrix[x].charAt(y) && !stickerCollected[x][y]) {
            totalStickerCollected++;
            stickerCollected[x][y] = true;
          }
        }
      } else {
        currentDirection += DIRECTION_DELTA[command - 'D'];
        if (currentDirection == 4) {
          currentDirection = 0;
        }
        if (currentDirection < 0) {
          currentDirection = 3;
        }
      }
    }
  }
}
