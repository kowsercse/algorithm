package com.kowsercse.uva.V118;

import java.util.HashSet;
import java.util.Scanner;

public class P11849 {
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    int n = scanner.nextInt();
    int m = scanner.nextInt();

    while (n != 0 || m != 0) {
      HashSet<String> catalogNumbers = new HashSet<>();
      for (int i = 0; i < n; i++) {
        catalogNumbers.add(scanner.next());
      }

      int duplicateCatalogCount = 0;
      for (int i = 0; i < m; i++) {
        String catalogNumber = scanner.next();
        if (catalogNumbers.contains(catalogNumber)) {
          duplicateCatalogCount++;
        }
      }

      System.out.println(duplicateCatalogCount);

      n = scanner.nextInt();
      m = scanner.nextInt();
    }
  }
}
