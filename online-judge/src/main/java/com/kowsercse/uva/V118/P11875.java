package com.kowsercse.uva.V118;

import java.util.Scanner;

public class P11875 {

  private final Scanner scanner = new Scanner(System.in);

  public void solve() {
    long testCase = scanner.nextLong();
    int[] members = new int[20];
    for (int i = 1; i <= testCase; i++) {
      int N = scanner.nextInt();
      for (int j = 0; j < N; j++) {
        members[j] = scanner.nextInt();
      }
      int leader = members[N / 2];
      System.out.println("Case " + i + ": " + leader);
    }
  }

  public static void main(String[] args) {
    new P11875().solve();
  }
}
