package com.kowsercse.uva.V9;

import java.util.LinkedList;
import java.util.Scanner;

public class P948 {

  private static Long MAX = 1000000001L;
  private LinkedList<Long> fibs = new LinkedList<>();

  public P948() {
    fibs.addFirst(1L);
    fibs.addFirst(2L);
    while (fibs.getFirst() < MAX) {
      fibs.addFirst(fibs.get(0) + fibs.get(1));
    }
  }

  public void solve() {
    final Scanner scanner = new Scanner(System.in);
    long testCase = scanner.nextLong();

    while (testCase-- > 0) {
      long number = scanner.nextLong();
      LinkedList<Integer> bases = getFibBase(number);
      System.out.print(number + " = ");
      for (Integer base : bases) {
        System.out.print(base);
      }
      System.out.println(" (fib)");
    }
  }

  private LinkedList<Integer> getFibBase(long number) {
    boolean canAdd = false;
    LinkedList<Integer> bases = new LinkedList<>();
    for (Long fib : fibs) {
      if (number >= fib) {
        bases.add(1);
        number = number - fib;
        canAdd = true;
      } else if (canAdd) {
        bases.add(0);
      }
    }

    return bases;
  }

  public static void main(String[] args) {
    new P948().solve();
  }
}
