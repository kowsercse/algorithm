package com.kowsercse.uva.V9;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.StringJoiner;
import java.util.StringTokenizer;

public class P978 {
  public static void main(String[] args) throws IOException {
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    int testCase = Integer.parseInt(reader.readLine());
    while (testCase-- > 0) {
      StringTokenizer tokenizer = new StringTokenizer(reader.readLine());

      int totalBattleFields = Integer.parseInt(tokenizer.nextToken());
      int greenTotal = Integer.parseInt(tokenizer.nextToken());
      int blueTotal = Integer.parseInt(tokenizer.nextToken());

      PriorityQueue<Integer> greenSoldiers = readSoldiers(reader, greenTotal);
      PriorityQueue<Integer> blueSoldiers = readSoldiers(reader, blueTotal);

      while (!greenSoldiers.isEmpty() && !blueSoldiers.isEmpty()) {
        int actualBattleFields = min(totalBattleFields, greenSoldiers.size(), blueSoldiers.size());

        Integer[] greenSelected = new Integer[actualBattleFields];
        Integer[] blueSelected = new Integer[actualBattleFields];
        for (int i = 0; i < actualBattleFields; i++) {
          greenSelected[i] = greenSoldiers.poll();
          blueSelected[i] = blueSoldiers.poll();
        }

        for (int i = 0; i < actualBattleFields; i++) {
          Integer green = greenSelected[i];
          Integer blue = blueSelected[i];

          if (green > blue) {
            greenSoldiers.add(green - blue);
          } else if (green < blue) {
            blueSoldiers.add(blue - green);
          }
        }
      }

      if (blueSoldiers.isEmpty() && greenSoldiers.isEmpty()) {
        System.out.println("green and blue died");
      } else if (!blueSoldiers.isEmpty()) {
        System.out.println("blue wins");
        printWinner(blueSoldiers);
      } else if (!greenSoldiers.isEmpty()) {
        System.out.println("green wins");
        printWinner(greenSoldiers);
      }

      if (testCase != 0) {
        System.out.println();
      }
    }
  }

  private static void printWinner(PriorityQueue<Integer> soldiers) {
    StringJoiner stringJoiner = new StringJoiner("\n");
    while (!soldiers.isEmpty()) {
      stringJoiner.add(String.valueOf(soldiers.poll()));
    }
    System.out.println(stringJoiner.toString());
  }

  private static PriorityQueue<Integer> readSoldiers(BufferedReader reader, int blueTotal)
      throws IOException {
    PriorityQueue<Integer> blueSoldiers = new PriorityQueue<>(blueTotal, Comparator.reverseOrder());
    while (blueTotal-- > 0) {
      blueSoldiers.add(Integer.parseInt(reader.readLine()));
    }
    return blueSoldiers;
  }

  private static int min(int a, int b, int c) {
    return Math.min(a, Math.min(b, c));
  }
}
