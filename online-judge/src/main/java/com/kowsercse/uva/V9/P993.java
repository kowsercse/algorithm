package com.kowsercse.uva.V9;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class P993 {

  private final Scanner scanner = new Scanner(System.in);

  public void solve() {
    long testCase = scanner.nextLong();

    while (testCase-- > 0) {
      long n = scanner.nextLong();
      List<Integer> products = findProducts(n);
      for (Integer integer : products) {
        System.out.print(integer);
      }
      System.out.println();
    }
  }

  private static List<Integer> findProducts(Long number) {
    if (number == 1) {
      return Collections.singletonList(1);
    }

    LinkedList<Integer> products = new LinkedList<>();
    for (int digit = 9; digit > 1; digit--) {
      while (number % digit == 0) {
        products.addFirst(digit);
        number = number / digit;
      }
    }

    if (number != 1) {
      return Collections.singletonList(-1);
    }

    return products;
  }

  public static void main(String[] args) {
    new P993().solve();
  }
}
