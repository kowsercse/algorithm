package com.kowsercse.uva.V115;

import java.util.*;

public class P11572 {

  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    int testCase = scanner.nextInt();

    for (int j = 0; j < testCase; j++) {
      int totalSnowFlakes = scanner.nextInt();
      int maxSnows = 0;
      LinkedHashSet<Integer> snows = new LinkedHashSet<>();
      for (int i = 0; i < totalSnowFlakes; i++) {
        int snow = scanner.nextInt();
        if (snows.contains(snow)) {
          Iterator<Integer> iterator = snows.iterator();
          while (iterator.hasNext() && iterator.next() != snow) {
            iterator.remove();
          }
          iterator.remove();
        }

        snows.add(snow);
        maxSnows = Math.max(maxSnows, snows.size());
      }

      System.out.println(maxSnows);
    }
  }
}
