package com.kowsercse.uva.V115;

import java.util.*;

public class P11503 {

  public static void main(String[] args) {
    new P11503().solve();
  }

  public void solve() {
    final Scanner scanner = new Scanner(System.in);
    int testCase = scanner.nextInt();

    while (testCase-- > 0) {
      final QuickUnionFindPathCompression unionFind = new QuickUnionFindPathCompression();
      int lines = scanner.nextInt();
      scanner.nextLine();
      while (lines-- > 0) {
        final String line = scanner.nextLine();
        final String[] names = line.split(" ");
        unionFind.initializeRoot(names[0].trim());
        unionFind.initializeRoot(names[1].trim());

        unionFind.union(names[0], names[1]);
        System.out.println(unionFind.getSize(names[1]));
      }
    }
  }
}

class QuickUnionFindPathCompression {
  private Map<String, String> roots = new HashMap<>();
  private Map<String, Integer> sizes = new HashMap<>();
  private Set<String> nodes = new HashSet<>();

  public void union(String node1, String node2) {
    final String root1 = getRoot(node1);
    final String root2 = getRoot(node2);
    if (root1.equals(root2)) {
      return;
    }

    final Integer size1 = sizes.get(root1);
    final Integer size2 = sizes.get(root2);
    if (size1 < size2) {
      roots.put(root1, root2);
      sizes.put(root2, size1 + size2);
    } else {
      roots.put(root2, root1);
      sizes.put(root1, size1 + size2);
    }
  }

  public String getRoot(String name) {
    String root = roots.get(name);
    while (!name.equals(root)) {
      roots.put(name, roots.get(root));
      name = root;
      root = roots.get(name);
    }
    return root;
  }

  public int getSize(final String name) {
    final String root = getRoot(name);
    return sizes.get(root);
  }

  public void initializeRoot(final String name) {
    if (roots.get(name) == null) {
      roots.put(name, name);
      sizes.put(name, 1);
      nodes.add(name);
    }
  }
}
