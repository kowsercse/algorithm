package com.kowsercse.uva.V115;

import static java.lang.Math.min;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.TreeMap;

public class P11517 {

  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);

    int testCase = scanner.nextInt();
    while (testCase-- > 0) {
      int LIMIT = 20000;
      int[] totalPath = new int[LIMIT + 1];

      totalPath[0] = 1;

      int targetAmount = scanner.nextInt();
      int totalCoin = scanner.nextInt();
      while (totalCoin-- > 0) {
        int coin = scanner.nextInt();
        for (int i = LIMIT; i >= coin; i--) {
          if (totalPath[i - coin] > 0) {
            totalPath[i] =
                totalPath[i] == 0
                    ? totalPath[i - coin] + 1
                    : min(totalPath[i], totalPath[i - coin] + 1);
          }
        }
      }

      while (totalPath[targetAmount] == 0) {
        targetAmount++;
      }

      System.out.println(targetAmount + " " + (totalPath[targetAmount] - 1));
    }
  }

  private static void solution1(Scanner scanner) {
    int targetAmount = scanner.nextInt();
    int totalCoin = scanner.nextInt();
    List<Integer> coins = new ArrayList<>(totalCoin);
    for (int i = 0; i < totalCoin; i++) {
      coins.add(scanner.nextInt());
    }

    TreeMap<Integer, Integer> reachablePath = new TreeMap<>();

    for (int coin : coins) {
      HashMap<Integer, Integer> map = new HashMap<>();
      map.put(coin, 1);

      for (Entry<Integer, Integer> entry : reachablePath.entrySet()) {
        if (entry.getKey() > targetAmount) {
          break;
        }
        map.put(entry.getKey() + coin, entry.getValue() + 1);
      }

      for (Entry<Integer, Integer> entry : map.entrySet()) {
        reachablePath.compute(
            entry.getKey(),
            (key, value) -> value == null || value > entry.getValue() ? entry.getValue() : value);
      }
    }

    Entry<Integer, Integer> entry = reachablePath.ceilingEntry(targetAmount);
    System.out.println(entry.getKey() + " " + entry.getValue());
  }
}
