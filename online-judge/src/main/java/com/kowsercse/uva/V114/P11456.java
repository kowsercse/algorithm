package com.kowsercse.uva.V114;

import static java.lang.Integer.max;

import java.util.Scanner;
import java.util.StringJoiner;

public class P11456 {
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    int testCase = scanner.nextInt();

    while (testCase-- > 0) {
      int n = scanner.nextInt();
      if (n == 0) {
        System.out.println(0);
        continue;
      }
      int[] input = readInput(scanner, n);
      int[] reverseInput = reverseInput(n, input);

      int[] lis = getLisArray(reverseInput);
      int[] lds = getLdsArray(reverseInput);

      int max = Integer.MIN_VALUE;
      for (int i = 0; i < n; i++) {
        max = max(max, lis[i] + lds[i]);
      }
      System.out.println(max - 1);
    }
  }

  private static int[] reverseInput(int n, int[] input) {
    int[] reverseInput = new int[n];
    for (int i = 0; i < n; i++) {
      reverseInput[i] = input[n - i - 1];
    }
    return reverseInput;
  }

  private static int[] getLisArray(int[] input) {
    int n = input.length;

    int[] lis = new int[n];
    int length = 0;
    int[] previousInputIndex = new int[n];
    int[] minInputIndexByLis = new int[n];
    previousInputIndex[0] = -1;
    minInputIndexByLis[0] = 0;
    lis[0] = 1;

    for (int i = 1; i < n; i++) {
      int number = input[i];
      if (number > input[minInputIndexByLis[length]]) {
        previousInputIndex[i] = minInputIndexByLis[length];

        length++;
        lis[i] = length + 1;
        minInputIndexByLis[length] = i;
      } else if (number <= input[minInputIndexByLis[0]]) {
        minInputIndexByLis[0] = i;
        previousInputIndex[i] = -1;
        lis[i] = 1;
      } else {
        int ceilIndex = ceilIndexByLis(number, input, minInputIndexByLis, 0, length);
        minInputIndexByLis[ceilIndex] = i;
        previousInputIndex[i] = minInputIndexByLis[ceilIndex - 1];
        lis[i] = ceilIndex + 1;
      }
    }

    //    StringJoiner stringJoiner = new StringJoiner(" ");
    //    dfs(stringJoiner, input, previousInputIndex, minInputIndexByLis[length]);
    //    System.out.println("Input: " +
    // IntStream.of(input).mapToObj(String::valueOf).collect(Collectors.joining(" ")));
    //    System.out.println("Solution: " + stringJoiner);
    //    System.out.println("Length: " + (length + 1));

    return lis;
  }

  private static int[] getLdsArray(int[] input) {
    int n = input.length;

    int[] lis = new int[n];
    int length = 0;
    int[] previousInputIndex = new int[n];
    int[] minInputIndexByLis = new int[n];
    previousInputIndex[0] = -1;
    minInputIndexByLis[0] = 0;
    lis[0] = 1;

    for (int i = 1; i < n; i++) {
      int number = input[i];
      if (number < input[minInputIndexByLis[length]]) {
        previousInputIndex[i] = minInputIndexByLis[length];

        length++;
        lis[i] = length + 1;
        minInputIndexByLis[length] = i;
      } else if (number >= input[minInputIndexByLis[0]]) {
        minInputIndexByLis[0] = i;
        previousInputIndex[i] = -1;
        lis[i] = 1;
      } else {
        int ceilIndex = ceilIndexByLds(number, input, minInputIndexByLis, 0, length);
        minInputIndexByLis[ceilIndex] = i;
        previousInputIndex[i] = minInputIndexByLis[ceilIndex - 1];
        lis[i] = ceilIndex + 1;
      }
    }

    //    StringJoiner stringJoiner = new StringJoiner(" ");
    //    dfs(stringJoiner, input, previousInputIndex, minInputIndexByLis[length]);
    //    System.out.println("Input: " +
    // IntStream.of(input).mapToObj(String::valueOf).collect(Collectors.joining(" ")));
    //    System.out.println("Solution: " + stringJoiner);
    //    System.out.println("Length: " + (length + 1));

    return lis;
  }

  private static void dfs(
      StringJoiner stringJoiner, int[] input, int[] previousInputIndex, int currentIndex) {
    if (currentIndex == -1) {
      return;
    }
    dfs(stringJoiner, input, previousInputIndex, previousInputIndex[currentIndex]);
    stringJoiner.add(String.valueOf(input[currentIndex]));
  }

  private static int ceilIndexByLds(
      int needle, int[] input, int[] minInputIndexByLis, int low, int high) {
    if (low == high) {
      return low;
    }
    int mid = (low + high) / 2;
    int middleValue = input[minInputIndexByLis[mid]];
    if (middleValue < needle) {
      return ceilIndexByLds(needle, input, minInputIndexByLis, low, mid);
    } else {
      return ceilIndexByLds(needle, input, minInputIndexByLis, mid + 1, high);
    }
  }

  private static int ceilIndexByLis(
      int needle, int[] input, int[] minInputIndexByLis, int low, int high) {
    if (low == high) {
      return low;
    }
    int mid = (low + high) / 2;
    int middleValue = input[minInputIndexByLis[mid]];
    if (middleValue > needle) {
      return ceilIndexByLis(needle, input, minInputIndexByLis, low, mid);
    } else {
      return ceilIndexByLis(needle, input, minInputIndexByLis, mid + 1, high);
    }
  }

  private static int[] readInput(Scanner scanner, int n) {
    int[] input = new int[n];
    for (int i = 0; i < n; i++) {
      input[i] = scanner.nextInt();
    }

    return input;
  }
}
