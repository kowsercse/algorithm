package com.kowsercse.uva.V4;

import java.util.Scanner;

public class P401 {
  private static char[] MIRRORED_CHARACTERS = initializeMirroredCharacters();

  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    while (scanner.hasNext()) {
      String string = scanner.next();

      int start = 0;
      int end = string.length() - 1;

      boolean palindrome = true;
      boolean mirrored = true;
      while (start < end) {
        char startCharacter = string.charAt(start);
        char endCharacter = string.charAt(end);

        if (startCharacter != endCharacter) {
          palindrome = false;
        }
        if (startCharacter != MIRRORED_CHARACTERS[endCharacter]) {
          mirrored = false;
        }

        start++;
        end--;
      }

      if (string.length() % 2 == 1) {
        char middleCharacter = string.charAt(string.length() / 2);
        if (middleCharacter != MIRRORED_CHARACTERS[middleCharacter]) {
          mirrored = false;
        }
      }

      if (palindrome && mirrored) {
        System.out.println(string + " -- is a mirrored palindrome.\n");
      } else if (!palindrome && mirrored) {
        System.out.println(string + " -- is a mirrored string.\n");
      } else if (palindrome && !mirrored) {
        System.out.println(string + " -- is a regular palindrome.\n");
      } else {
        System.out.println(string + " -- is not a palindrome.\n");
      }
    }
  }

  private static char[] initializeMirroredCharacters() {
    char[] mirroredCharacters = new char[100];
    setMirrorCharacter(mirroredCharacters, 'A');
    setMirrorCharacter(mirroredCharacters, 'E', '3');
    setMirrorCharacter(mirroredCharacters, 'H');
    setMirrorCharacter(mirroredCharacters, 'I');
    setMirrorCharacter(mirroredCharacters, 'J', 'L');
    setMirrorCharacter(mirroredCharacters, 'M');
    setMirrorCharacter(mirroredCharacters, 'O');
    setMirrorCharacter(mirroredCharacters, 'S', '2');
    setMirrorCharacter(mirroredCharacters, 'T');
    setMirrorCharacter(mirroredCharacters, 'U');
    setMirrorCharacter(mirroredCharacters, 'V');
    setMirrorCharacter(mirroredCharacters, 'W');
    setMirrorCharacter(mirroredCharacters, 'X');
    setMirrorCharacter(mirroredCharacters, 'Y');
    setMirrorCharacter(mirroredCharacters, 'Z', '5');
    setMirrorCharacter(mirroredCharacters, '1');
    setMirrorCharacter(mirroredCharacters, '8');
    return mirroredCharacters;
  }

  private static void setMirrorCharacter(char[] mirroredCharacters, char character, char mirror) {
    mirroredCharacters[character] = mirror;
    mirroredCharacters[mirror] = character;
  }

  private static void setMirrorCharacter(char[] mirroredCharacters, char character) {
    mirroredCharacters[character] = character;
  }
}
