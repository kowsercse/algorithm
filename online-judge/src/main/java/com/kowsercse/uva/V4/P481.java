package com.kowsercse.uva.V4;

import java.util.*;

public class P481 {
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    ArrayList<Integer> numbers = new ArrayList<>();
    ArrayList<Integer> minNumberIndexByLis = new ArrayList<>();
    ArrayList<Integer> previousIndex = new ArrayList<>();

    int length = 0;
    int number = scanner.nextInt();

    numbers.add(number);
    minNumberIndexByLis.add(0);
    previousIndex.add(-1);

    int index = 1;
    while (scanner.hasNext()) {
      number = scanner.nextInt();
      numbers.add(number);

      if (number > numbers.get(minNumberIndexByLis.get(length))) {
        previousIndex.add(minNumberIndexByLis.get(length));
        minNumberIndexByLis.add(index);
        length++;
      } else if (number <= numbers.get(minNumberIndexByLis.get(0))) {
        previousIndex.add(-1);
        minNumberIndexByLis.set(0, index);
      } else {
        int ceilIndex =
            ceilIndex(number, numbers, minNumberIndexByLis, 0, minNumberIndexByLis.size() - 1);
        previousIndex.add(minNumberIndexByLis.get(ceilIndex - 1));
        minNumberIndexByLis.set(ceilIndex, index);
      }
      index++;
    }

    System.out.println(length + 1);
    System.out.println('-');
    dfs(numbers, previousIndex, minNumberIndexByLis.get(length));
  }

  private static void dfs(
      ArrayList<Integer> numbers, ArrayList<Integer> previousIndex, Integer currentIndex) {
    if (currentIndex == -1) {
      return;
    }

    dfs(numbers, previousIndex, previousIndex.get(currentIndex));

    System.out.println(numbers.get(currentIndex));
  }

  private static int ceilIndex(
      int needle, ArrayList<Integer> values, ArrayList<Integer> indexes, int low, int high) {
    if (low == high) {
      return low;
    }

    int mid = (low + high) / 2;
    Integer middleValue = values.get(indexes.get(mid));
    if (middleValue == needle) {
      return mid;
    } else if (middleValue > needle) {
      return ceilIndex(needle, values, indexes, low, mid);
    } else {
      return ceilIndex(needle, values, indexes, mid + 1, high);
    }
  }
}

// Complexity = O(n^2)
class Solution1 {
  static void solution1() {
    Scanner scanner = new Scanner(System.in);
    ArrayList<Integer> numbers = new ArrayList<>();
    ArrayList<Integer> lisLength = new ArrayList<>();
    ArrayList<Integer> previousNumberIndexList = new ArrayList<>();

    numbers.add(Integer.MIN_VALUE);
    lisLength.add(0);
    previousNumberIndexList.add(0);

    int globalMax = 0;
    int globalMaxIndex = 0;
    int index = 0;
    while (scanner.hasNext()) {
      index++;
      int number = scanner.nextInt();
      int localMax = 0;
      int localMaxIndex = 0;

      for (int i = numbers.size() - 1; i >= 0; i--) {
        if (numbers.get(i) < number) {
          Integer lisAsOfIndex = lisLength.get(i);
          if (lisAsOfIndex >= localMax) {
            localMax = lisLength.get(i) + 1;
            localMaxIndex = i;
          }
        }
      }

      lisLength.add(localMax);
      numbers.add(number);
      previousNumberIndexList.add(localMaxIndex);

      if (globalMax <= localMax) {
        globalMax = localMax;
        globalMaxIndex = index;
      }
    }

    LinkedList<Integer> answers = new LinkedList<>();
    int iteratorIndex = globalMaxIndex;
    while (iteratorIndex != 0) {
      answers.addFirst(numbers.get(iteratorIndex));
      iteratorIndex = previousNumberIndexList.get(iteratorIndex);
    }

    System.out.println(answers.size());
    System.out.println('-');
    for (Integer answer : answers) {
      System.out.println(answer);
    }
  }
}
