package com.kowsercse.uva.V12;

import java.util.*;

public class P1203 {

  public static void main(String[] args) {
    final Scanner scanner = new Scanner(System.in);
    final LinkedList<String> instructions = readInstructions(scanner);
    final int queueSize = scanner.nextInt();

    final PriorityQueue<Period> initialPeriods = new PriorityQueue<>();
    for (String instruction : instructions) {
      addInstructionEventToQueue(queueSize, initialPeriods, instruction);
    }

    final Stack<Integer> stack = new Stack<>();
    while (!initialPeriods.isEmpty()) {
      stack.push(initialPeriods.poll().qNum);
    }

    while (!stack.isEmpty()) {
      System.out.println(stack.pop());
    }
  }

  private static void addInstructionEventToQueue(
      int queueSize, PriorityQueue<Period> initialPeriods, String instruction) {
    final StringTokenizer tokenizer = new StringTokenizer(instruction);
    tokenizer.nextToken();
    int qNum = Integer.parseInt(tokenizer.nextToken());
    int time = Integer.parseInt(tokenizer.nextToken());

    for (int i = time; i < 3000; i += time) {
      final Period period = new Period(qNum, i);
      if (initialPeriods.size() < queueSize) {
        initialPeriods.add(period);
      } else {
        final Period peek = initialPeriods.peek();
        final int comparisonResult = peek.compareTo(period);

        if (comparisonResult < 0) {
          initialPeriods.poll();
          initialPeriods.add(period);
        } else {
          return;
        }
      }
    }
  }

  private static LinkedList<String> readInstructions(Scanner scanner) {
    final LinkedList<String> instructions = new LinkedList<>();
    String instruction = scanner.nextLine();
    while (!"#".equals(instruction)) {
      instructions.add(instruction);
      instruction = scanner.nextLine();
    }
    return instructions;
  }
}

class Period implements Comparable<Period> {
  final int qNum;
  final int time;

  Period(int qNum, int time) {
    this.qNum = qNum;
    this.time = time;
  }

  @Override
  public int compareTo(Period o) {
    if (time != o.time) {
      return Integer.compare(o.time, time);
    }

    return Integer.compare(o.qNum, qNum);
  }
}
