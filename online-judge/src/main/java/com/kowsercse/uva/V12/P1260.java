package com.kowsercse.uva.V12;

import java.util.Scanner;

public class P1260 {
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    int testCase = scanner.nextInt();
    while (testCase-- > 0) {
      int totalSell = scanner.nextInt();

      int result = 0;
      int[] sellValues = new int[totalSell];

      sellValues[0] = scanner.nextInt();
      for (int i = 1; i < totalSell; i++) {
        int sellValue = scanner.nextInt();
        for (int previous = 0; previous < i; previous++) {
          if (sellValues[previous] <= sellValue) {
            result++;
          }
        }

        sellValues[i] = sellValue;
      }

      System.out.println(result);
    }
  }
}
