package com.kowsercse.uva.V12;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.StringTokenizer;

public class P1237 {
  public static void main(String[] args) throws IOException {
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    int testCase = Integer.parseInt(reader.readLine());
    while (testCase-- > 0) {
      ArrayList<Maker> makers = readMakers(reader);
      int totalQuery = Integer.parseInt(reader.readLine());
      for (int i = 0; i < totalQuery; i++) {
        int query = Integer.parseInt(reader.readLine());

        Maker foundMaker = null;
        for (Maker maker : makers) {
          if (maker.lowPrice > query) {
            break;
          } else if (maker.highPrice >= query) {
            if (foundMaker != null) {
              foundMaker = null;
              break;
            }
            foundMaker = maker;
          }
        }

        System.out.println(foundMaker == null ? "UNDETERMINED" : foundMaker.name);
      }

      if (testCase != 0) {
        System.out.println();
      }
    }
  }

  private static ArrayList<Maker> readMakers(BufferedReader reader) throws IOException {
    int totalMaker = Integer.parseInt(reader.readLine());
    ArrayList<Maker> makers = new ArrayList<>(totalMaker);
    for (int i = 0; i < totalMaker; i++) {
      makers.add(new Maker(reader));
    }
    makers.sort(Comparator.comparingInt(maker -> maker.lowPrice));
    return makers;
  }
}

class Maker {
  String name;
  int lowPrice;
  int highPrice;

  public Maker(BufferedReader reader) throws IOException {
    StringTokenizer tokenizer = new StringTokenizer(reader.readLine());
    name = tokenizer.nextToken();
    lowPrice = Integer.parseInt(tokenizer.nextToken());
    highPrice = Integer.parseInt(tokenizer.nextToken());
  }
}
