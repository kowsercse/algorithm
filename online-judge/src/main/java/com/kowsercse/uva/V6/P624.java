package com.kowsercse.uva.V6;

import java.util.*;

public class P624 {
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    while (scanner.hasNext()) {
      int durationSize = scanner.nextInt();
      int[] tracks = readTracks(scanner);
      boolean[][] table = createTable(durationSize, tracks);
      final int sum = getSum(durationSize, table);
      LinkedList<String> selectedTracks = getSelectedTracks(tracks, sum, table);

      System.out.println(String.join(" ", selectedTracks) + " sum:" + sum);
    }
  }

  private static boolean[][] createTable(int durationSize, int[] tracks) {
    boolean[][] table = new boolean[tracks.length + 1][durationSize + 1];
    table[0][0] = true;

    for (int r = 1; r <= tracks.length; r++) {
      int track = tracks[r - 1];
      if (track >= 0) {
        System.arraycopy(table[r - 1], 0, table[r], 0, track);
      }

      for (int c = track; c <= durationSize; c++) {
        table[r][c] = table[r - 1][c] || table[r - 1][c - track];
      }
      if (table[r][durationSize]) {
        for (int rr = r + 1; rr <= tracks.length; rr++) {
          table[rr][durationSize] = true;
        }
        return table;
      }
    }

    return table;
  }

  private static int getSum(int durationSize, boolean[][] table) {
    int lastRow = table.length - 1;
    int duration = durationSize;
    while (!table[lastRow][duration]) {
      duration--;
    }
    return duration;
  }

  private static LinkedList<String> getSelectedTracks(int[] tracks, int sum, boolean[][] table) {
    LinkedList<String> selectedTracks = new LinkedList<>();

    int trackIndex = tracks.length;
    int currentDuration = sum;
    while (currentDuration > 0) {
      while (table[trackIndex - 1][currentDuration]) {
        trackIndex--;
      }

      int nextDuration = currentDuration - tracks[trackIndex - 1];
      if (nextDuration >= 0 && table[trackIndex - 1][nextDuration]) {
        selectedTracks.addFirst(Integer.toString(tracks[trackIndex - 1]));
        currentDuration = nextDuration;
      }

      trackIndex--;
    }

    return selectedTracks;
  }

  private static int[] readTracks(Scanner scanner) {
    int totalTracks = scanner.nextInt();
    int[] tracks = new int[totalTracks];
    for (int i = 0; i < totalTracks; i++) {
      tracks[i] = scanner.nextInt();
    }
    return tracks;
  }
}
