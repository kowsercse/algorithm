package com.kowsercse.uva.V6;

import java.util.Scanner;
import java.util.Stack;

public class P673 {

  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    int testCase = scanner.nextInt();
    scanner.nextLine();
    for (int i = 0; i < testCase; i++) {
      String parentheses = scanner.nextLine();
      boolean balanced = isBalanced(parentheses);
      System.out.println(balanced ? "Yes" : "No");
    }
  }

  private static boolean isBalanced(String parentheses) {
    Stack<Character> stack = new Stack<>();
    for (int i = 0; i < parentheses.length(); i++) {
      char character = parentheses.charAt(i);
      if (character == '[' || character == '(') {
        stack.push(character);
      } else if (character == ']') {
        if (stack.isEmpty() || stack.peek() != '[') {
          return false;
        }
        stack.pop();
      } else if (character == ')') {
        if (stack.isEmpty() || stack.peek() != '(') {
          return false;
        }
        stack.pop();
      }
    }

    return stack.isEmpty();
  }
}
