package com.kowsercse.uva.V6;

import java.util.Scanner;

public class P674 {

  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    long[] totalPath = preCalculate();

    while (scanner.hasNext()) {
      System.out.println(totalPath[scanner.nextInt()]);
    }
  }

  private static long[] preCalculate() {
    int maximumTarget = 7489 + 1;
    long[] totalPath = new long[maximumTarget];
    totalPath[0] = 1;
    int[] coins = {50, 25, 10, 5, 1};

    for (int coin : coins) {
      for (int i = coin; i < maximumTarget; i++) {
        totalPath[i] += totalPath[i - coin];
      }
    }
    return totalPath;
  }
}
