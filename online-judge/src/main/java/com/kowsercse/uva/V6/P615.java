package com.kowsercse.uva.V6;

import java.util.*;

public class P615 {
  Scanner scanner = new Scanner(System.in);

  public static void main(String[] args) {
    new P615().solve();
  }

  void solve() {
    int testCase = 0;
    int source = scanner.nextInt();
    int destination = scanner.nextInt();

    while (!(source < 0 && destination < 0)) {
      Graph graph = new Graph();
      while (source != 0 && destination != 0) {
        graph.addEdge(source, destination);

        source = scanner.nextInt();
        destination = scanner.nextInt();
      }
      System.out.println(
          "Case " + ++testCase + " is" + (graph.isTree() ? "" : " not") + " a tree.");

      source = scanner.nextInt();
      destination = scanner.nextInt();
    }
  }
}

class Graph {
  private final Map<Integer, Integer> parentCountByNode = new HashMap<>();
  private final Map<Integer, List<Integer>> edgesBySourceNode = new HashMap<>();
  private final Map<Integer, Boolean> visited = new HashMap<>();

  boolean tree = true;
  private Integer parent;

  public void addEdge(int source, int destination) {
    if (!tree) {
      return;
    }

    parentCountByNode.compute(
        destination, (node, totalParent) -> totalParent == null ? 1 : totalParent + 1);
    parentCountByNode.putIfAbsent(source, 0);
    if (source == destination || parentCountByNode.get(destination) > 1) {
      tree = false;
    }

    edgesBySourceNode.computeIfAbsent(source, ignore -> new LinkedList<>()).add(destination);
    edgesBySourceNode.computeIfAbsent(destination, ignore -> new LinkedList<>());
  }

  public boolean isTree() {
    if (!tree) {
      return false;
    }
    if (parentCountByNode.isEmpty()) {
      return true;
    }
    boolean information = calculateParentLessNodeInformation();
    if (!information) {
      return false;
    }

    boolean dfs = dfs(parent);
    if (!dfs) {
      return false;
    }

    return visited.size() == parentCountByNode.size();
  }

  private boolean dfs(Integer node) {
    if (visited.get(node) != null) {
      return false;
    }
    visited.put(node, true);
    for (Integer destination : edgesBySourceNode.get(node)) {
      boolean dfs = dfs(destination);
      if (!dfs) {
        return false;
      }
    }

    return true;
  }

  private boolean calculateParentLessNodeInformation() {
    int parentLessNode = 0;
    for (Map.Entry<Integer, Integer> parentCount : parentCountByNode.entrySet()) {
      if (parentCount.getValue() == 0) {
        parentLessNode++;
        parent = parentCount.getKey();
        if (parentLessNode > 1) {
          return false;
        }
      }
    }

    return parentLessNode == 1;
  }
}
