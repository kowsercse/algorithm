package com.kowsercse.uva.V10;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Stack;

public class P1062 {
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    int testCase = 1;
    String containers = scanner.next();
    while (!"end".equals(containers)) {
      System.out.println("Case " + testCase++ + ": " + getMaximumStack(containers));

      containers = scanner.next();
    }
  }

  public static int getMaximumStack(String containers) {
    final List<Stack<Character>> stacks = new ArrayList<>();

    for (int i = 0; i < containers.length(); i++) {
      char container = containers.charAt(i);

      int minimumRankDifference = Integer.MAX_VALUE;
      int preferredStack = -1;

      for (int s = 0; s < stacks.size(); s++) {
        Stack<Character> stack = stacks.get(s);
        char topContainer = stack.peek();
        if (topContainer == container) {
          preferredStack = s;
          break;
        } else if (topContainer > container) {
          int rankDifference = topContainer - container;
          if (rankDifference < minimumRankDifference) {
            minimumRankDifference = rankDifference;
            preferredStack = s;
          }
        }
      }

      if (preferredStack == -1) {
        Stack<Character> newStack = new Stack<>();
        newStack.push(container);
        stacks.add(newStack);
      } else {
        stacks.get(preferredStack).push(container);
      }
    }
    return stacks.size();
  }
}
