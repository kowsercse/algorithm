package com.kowsercse.uva.V104;

import java.util.Scanner;
import java.util.TreeSet;

public class P10487 {
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    int totalNumbers = scanner.nextInt();

    int testCase = 1;
    while (totalNumbers > 0) {
      System.out.println("Case " + testCase++ + ":");

      TreeSet<Integer> allSum = readInputs(scanner, totalNumbers);
      int totalQuery = scanner.nextInt();
      for (int i = 0; i < totalQuery; i++) {
        int query = scanner.nextInt();
        Integer floorValue = allSum.floor(query);
        Integer ceilValue = allSum.ceiling(query);
        int answer = 0;

        if (floorValue != null && ceilValue != null) {
          answer = query - floorValue <= ceilValue - query ? floorValue : ceilValue;
        } else if (floorValue != null) {
          answer = floorValue;
        } else if (ceilValue != null) {
          answer = ceilValue;
        }

        System.out.println("Closest sum to " + query + " is " + answer + ".");
      }

      totalNumbers = scanner.nextInt();
    }
  }

  private static TreeSet<Integer> readInputs(Scanner scanner, int totalNumbers) {
    int[] numbers = new int[totalNumbers];
    for (int i = 0; i < totalNumbers; i++) {
      numbers[i] = scanner.nextInt();
    }
    TreeSet<Integer> allSum = new TreeSet<>();
    for (int first = 0; first < totalNumbers; first++) {
      for (int second = first + 1; second < totalNumbers; second++) {
        allSum.add(numbers[first] + numbers[second]);
      }
    }
    return allSum;
  }
}
