package com.kowsercse.uva.V100;

import java.math.BigInteger;
import java.util.Scanner;

public class P10018 {

  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    int testCase = scanner.nextInt();
    while (testCase-- > 0) {
      BigInteger number = BigInteger.valueOf(scanner.nextInt());

      int i = 0;
      while (!isPalindrome(number.toString())) {
        BigInteger reverse = reverse(number.toString());
        number = reverse.add(number);
        i++;
      }
      System.out.println(i + " " + number);
    }
  }

  private static BigInteger reverse(String string) {
    char[] chars = string.toCharArray();
    char[] reverse = new char[chars.length];

    for (int i = 0; i < chars.length; i++) {
      reverse[chars.length - 1 - i] = chars[i];
    }

    return new BigInteger(new String(reverse), 10);
  }

  private static boolean isPalindrome(String string) {
    int start = 0;
    int end = string.length() - 1;
    while (start < end) {
      if (string.charAt(start) != string.charAt(end)) {
        return false;
      }
      start++;
      end--;
    }
    return true;
  }
}
