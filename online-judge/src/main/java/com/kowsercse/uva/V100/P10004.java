package com.kowsercse.uva.V100;

import java.util.*;

public class P10004 {
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    int totalNode = scanner.nextInt();
    while (totalNode != 0) {
      int totalEdge = scanner.nextInt();
      List<Node> nodes = readInput(scanner, totalNode, totalEdge);

      boolean bipartiteGraph = isBipartiteGraph(nodes);
      System.out.println(bipartiteGraph ? "BICOLORABLE." : "NOT BICOLORABLE.");

      totalNode = scanner.nextInt();
    }
  }

  private static List<Node> readInput(Scanner scanner, int totalNode, int totalEdge) {
    List<Node> nodes = new ArrayList<>();
    for (int i = 0; i < totalNode; i++) {
      nodes.add(new Node(i));
    }

    for (int i = 0; i < totalEdge; i++) {
      int source = scanner.nextInt();
      int destination = scanner.nextInt();

      nodes.get(source).edges.add(nodes.get(destination));
      nodes.get(destination).edges.add(nodes.get(source));
    }
    return nodes;
  }

  private static boolean isBipartiteGraph(List<Node> nodes) {
    Node root = nodes.get(0);
    root.color = Boolean.TRUE;

    Queue<Node> queue = new LinkedList<>();
    queue.add(root);
    while (!queue.isEmpty()) {
      Node source = queue.poll();
      source.processed = true;

      boolean oppositeColor = !source.color;
      for (Node destination : source.edges) {
        if (!destination.processed) {
          if (destination.color == null) {
            destination.color = oppositeColor;
            queue.add(destination);
          } else if (destination.color == source.color) {
            return false;
          }
        }
      }
    }

    return true;
  }
}

class Node {
  final int id;
  final List<Node> edges = new LinkedList<>();
  public Boolean color;
  public boolean processed;

  Node(int id) {
    this.id = id;
  }

  @Override
  public String toString() {
    return "{" + id + '}';
  }
}
