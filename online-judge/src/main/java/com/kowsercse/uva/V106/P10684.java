package com.kowsercse.uva.V106;

import java.util.Scanner;

public class P10684 {
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    int totalNumber = scanner.nextInt();
    while (totalNumber > 0) {
      int globalMaximum = Integer.MIN_VALUE;
      int runningSum = 0;
      for (int i = 0; i < totalNumber; i++) {
        int number = scanner.nextInt();
        runningSum += number;
        if (runningSum < 0) {
          runningSum = 0;
        } else if (globalMaximum < runningSum) {
          globalMaximum = runningSum;
        }
      }

      if (globalMaximum > 0) {
        System.out.println("The maximum winning streak is " + globalMaximum + ".");
      } else {
        System.out.println("Losing streak.");
      }

      totalNumber = scanner.nextInt();
    }
  }
}
