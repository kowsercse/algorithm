package com.kowsercse.uva.V3;

import java.util.ArrayList;
import java.util.Scanner;

public class P357 {
  private static final int[] coins = {1, 5, 10, 25, 50};
  public static final int MAX_TARGET_SUM = 30000;

  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    ArrayList<Long> totalPath = new ArrayList<>(MAX_TARGET_SUM + 1);
    totalPath.add(1L);
    for (int i = 1; i <= MAX_TARGET_SUM; i++) {
      totalPath.add(1L);
    }
    for (int i = 1; i < coins.length; i++) {
      int coin = coins[i];
      for (int c = coin; c <= MAX_TARGET_SUM; c++) {
        totalPath.set(c, totalPath.get(c) + totalPath.get(c - coin));
      }
    }

    while (scanner.hasNext()) {
      int targetSum = scanner.nextInt();
      if (targetSum < 5) {
        System.out.println("There is only 1 way to produce " + targetSum + " cents change.");
      } else {
        System.out.println(
            "There are "
                + totalPath.get(targetSum)
                + " ways to produce "
                + targetSum
                + " cents change.");
      }
    }
  }
}
