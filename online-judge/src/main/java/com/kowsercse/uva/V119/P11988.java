package com.kowsercse.uva.V119;

import java.util.LinkedList;
import java.util.Scanner;
import java.util.Stack;

public class P11988 {
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    while (scanner.hasNext()) {
      Stack<LinkedList<Character>> stack = process(scanner.nextLine());
      StringBuilder stringBuilder = new StringBuilder();
      while (!stack.isEmpty()) {
        for (Character character : stack.pop()) {
          stringBuilder.append(character);
        }
      }
      System.out.println(stringBuilder.toString());
    }
  }

  private static Stack<LinkedList<Character>> process(String line) {
    LinkedList<Character> homeStringBuilder = new LinkedList<>();
    LinkedList<Character> endStringBuilder = new LinkedList<>();
    LinkedList<Character> currentStringBuilder = endStringBuilder;
    Stack<LinkedList<Character>> stack = new Stack<>();
    stack.push(endStringBuilder);

    boolean isHomeButtonPressed = false;
    for (int i = 0; i < line.length(); i++) {
      char currentCharacter = line.charAt(i);

      if (currentCharacter == '[') {
        isHomeButtonPressed = true;
        stack.push(homeStringBuilder);
        currentStringBuilder = homeStringBuilder = new LinkedList<>();
      } else if (currentCharacter == ']') {
        if (isHomeButtonPressed) {
          stack.push(homeStringBuilder);
          homeStringBuilder = new LinkedList<>();
          currentStringBuilder = endStringBuilder;
        }
      } else {
        currentStringBuilder.addLast(currentCharacter);
      }
    }
    stack.push(homeStringBuilder);
    return stack;
  }

  private static String _process(String line) {
    StringBuilder homeStringBuilder = new StringBuilder();
    StringBuilder endStringBuilder = new StringBuilder();
    StringBuilder currentStringBuilder = endStringBuilder;

    boolean isHomeButtonPressed = false;
    for (int i = 0; i < line.length(); i++) {
      char currentCharacter = line.charAt(i);

      if (currentCharacter == '[') {
        isHomeButtonPressed = true;
        endStringBuilder = homeStringBuilder.append(endStringBuilder);
        currentStringBuilder = homeStringBuilder = new StringBuilder();
      } else if (currentCharacter == ']') {
        if (isHomeButtonPressed) {
          endStringBuilder = homeStringBuilder.append(endStringBuilder);
          homeStringBuilder = new StringBuilder();
          currentStringBuilder = endStringBuilder;
        }
      } else {
        currentStringBuilder.append(currentCharacter);
      }
    }
    return homeStringBuilder.append(endStringBuilder).toString();
  }
}
