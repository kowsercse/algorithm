package com.kowsercse.uva.V119;

import java.util.*;

public class P11995 {
  private String[] possibleDataStructure = new String[] {"stack", "queue", "priority queue"};

  public static void main(String[] args) {
    new P11995().solve();
  }

  private void solve() {
    final Scanner scanner = new Scanner(System.in);
    while (scanner.hasNext()) {
      final Stack<Integer> stack = new Stack<>();
      final Queue<Integer> queue = new LinkedList<>();
      final PriorityQueue<Integer> priorityQueue = new PriorityQueue<>(Comparator.reverseOrder());

      boolean[] possible = new boolean[] {true, true, true};

      int totalEntry = scanner.nextInt();
      while (totalEntry-- > 0) {
        final int command = scanner.nextInt();
        final Integer value = scanner.nextInt();

        if (command == 1) {
          push(stack, queue, priorityQueue, possible, value);
        } else if (command == 2) {
          pop(stack, queue, priorityQueue, possible, value);
        }
      }

      if (isImpossible(possible)) {
        System.out.println("impossible");
      } else if (isAmbiguous(possible)) {
        System.out.println("not sure");
      } else {
        final int dataStructureIndex = getPossibleDataStructure(possible);
        System.out.println(possibleDataStructure[dataStructureIndex]);
      }
    }
  }

  private int getPossibleDataStructure(boolean[] possible) {
    for (int i = 0; i < possible.length; i++) {
      if (possible[i]) {
        return i;
      }
    }

    return -1;
  }

  private boolean isAmbiguous(boolean[] possible) {
    int possibleCount = 0;
    for (boolean isPossible : possible) {
      if (isPossible) {
        possibleCount++;
      }
    }
    return possibleCount > 1;
  }

  private boolean isImpossible(boolean[] possible) {
    return !possible[0] && !possible[1] && !possible[2];
  }

  private void push(
      Stack<Integer> stack,
      Queue<Integer> queue,
      PriorityQueue<Integer> priorityQueue,
      boolean[] possible,
      Integer value) {
    if (possible[0]) {
      stack.push(value);
    }
    if (possible[1]) {
      queue.add(value);
    }
    if (possible[2]) {
      priorityQueue.add(value);
    }
  }

  private void pop(
      Stack<Integer> stack,
      Queue<Integer> queue,
      PriorityQueue<Integer> priorityQueue,
      boolean[] possible,
      Integer value) {
    if (stack.isEmpty()) {
      possible[0] = false;
    }
    if (queue.isEmpty()) {
      possible[1] = false;
    }
    if (priorityQueue.isEmpty()) {
      possible[2] = false;
    }

    if (possible[0] && value.equals(stack.peek())) {
      stack.pop();
    } else {
      possible[0] = false;
    }
    if (possible[1] && value.equals(queue.peek())) {
      queue.poll();
    } else {
      possible[1] = false;
    }
    if (possible[2] && value.equals(priorityQueue.peek())) {
      priorityQueue.poll();
    } else {
      possible[2] = false;
    }
  }
}
