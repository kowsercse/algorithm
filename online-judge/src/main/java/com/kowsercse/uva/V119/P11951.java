package com.kowsercse.uva.V119;

import java.util.Scanner;

public class P11951 {
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);

    int testCase = scanner.nextInt();
    for (int i = 1; i <= testCase; i++) {
      int R = scanner.nextInt();
      int C = scanner.nextInt();
      int maxBudget = scanner.nextInt();

      long[][] input = readInput(scanner, R, C);
      long[][] prefixSum = calculatePrefixSum(R, C, input);

      long globalMinCost = Integer.MAX_VALUE;
      long globalMaxPlot = 0;
      for (int rowStart = 0; rowStart < R; rowStart++) {
        for (int rwoEnd = rowStart; rwoEnd < R; rwoEnd++) {
          for (int columnStart = 0; columnStart < C; columnStart++) {
            long localTotalCost = 0;

            for (int columnEnd = columnStart; columnEnd < C; columnEnd++) {
              localTotalCost +=
                  prefixSum[rwoEnd][columnEnd]
                      - prefixSum[rowStart][columnEnd]
                      + input[rowStart][columnEnd];
              if (localTotalCost > maxBudget) {
                break;
              }

              long localTotalPlot = (rwoEnd - rowStart + 1) * (columnEnd - columnStart + 1);
              if (globalMaxPlot < localTotalPlot) {
                globalMaxPlot = localTotalPlot;
                globalMinCost = localTotalCost;
              } else if (globalMaxPlot == localTotalPlot && localTotalCost < globalMinCost) {
                globalMinCost = localTotalCost;
              }
            }
          }
        }
      }

      if (globalMaxPlot == 0) {
        globalMinCost = 0;
      }
      System.out.println("Case #" + i + ": " + globalMaxPlot + " " + globalMinCost);
    }
  }

  private static long[][] calculatePrefixSum(int R, int C, long[][] input) {
    long[][] prefixSum = new long[R][C];
    if (C >= 0) {
      System.arraycopy(input[0], 0, prefixSum[0], 0, C);
    }
    for (int r = 1; r < R; r++) {
      for (int c = 0; c < C; c++) {
        prefixSum[r][c] = prefixSum[r - 1][c] + input[r][c];
      }
    }
    return prefixSum;
  }

  private static long[][] readInput(Scanner scanner, int R, int C) {
    long[][] input = new long[R][C];
    for (int r = 0; r < R; r++) {
      for (int c = 0; c < C; c++) {
        input[r][c] = scanner.nextInt();
      }
    }
    return input;
  }

  private static void printTable(final long[][] table) {
    for (final long[] row : table) {
      for (final long i : row) {
        System.out.print(i + "\t");
      }
      System.out.println();
    }
    System.out.println();
  }
}
