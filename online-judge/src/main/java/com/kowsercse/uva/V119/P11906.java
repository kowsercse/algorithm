package com.kowsercse.uva.V119;

import java.util.Scanner;

public class P11906 {
  public static void main(String[] args) {
    final Scanner scanner = new Scanner(System.in);
    final int testCase = scanner.nextInt();
    for (int i = 1; i <= testCase; i++) {
      final Graph graph = new Graph(scanner);
      graph.readWaterRegions(scanner);
      graph.solve();
      System.out.println(
          "Case :" + i + " " + graph.evenOddRegionCounter[0] + " " + graph.evenOddRegionCounter[1]);
    }
  }
}

class Graph {
  private static int[][] DIRECTION_DELTA = new int[][] {{-1, 0}, {1, 0}, {0, 1}, {0, -1}};

  private final int row;
  private final int column;
  private final boolean[][] hasWater;
  int[] evenOddRegionCounter = new int[2];
  private final int[] jump;

  public Graph(Scanner scanner) {
    row = scanner.nextInt();
    column = scanner.nextInt();
    int verticalJump = scanner.nextInt();
    int horizontalJump = scanner.nextInt();
    jump = new int[] {verticalJump, horizontalJump};
    hasWater = new boolean[row][column];
  }

  public void readWaterRegions(Scanner scanner) {
    final int totalWaterSpots = scanner.nextInt();
    for (int i = 0; i < totalWaterSpots; i++) {
      final int r = scanner.nextInt();
      final int c = scanner.nextInt();

      hasWater[r][c] = true;
    }
  }

  public void solve() {
    for (int r = 0; r < row; r++) {
      for (int c = 0; c < column; c++) {
        final int totalReachableRegion = countReachableRegion(r, c);
        evenOddRegionCounter[totalReachableRegion % 2]++;
      }
    }
  }

  private int countReachableRegion(int r, int c) {
    int totalReachableRegion = 0;
    for (int i = 0; i < DIRECTION_DELTA.length; i++) {
      totalReachableRegion += dfs(r, c, DIRECTION_DELTA[i], jump[i / 2]);
    }
    return totalReachableRegion;
  }

  private int dfs(int r, int c, int[] delta, int depth) {
    if (depth == 0 || r < 0 || r >= row || c < 0 || c >= column || hasWater[r][c]) {
      return 0;
    }
    return 1 + dfs(r + delta[0], c + delta[1], delta, depth - 1);
  }
}
