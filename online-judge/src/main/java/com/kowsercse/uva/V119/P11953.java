package com.kowsercse.uva.V119;

import java.util.Scanner;

public class P11953 {
  public static void main(String[] args) {
    final Scanner scanner = new Scanner(System.in);
    final int testCase = scanner.nextInt();
    for (int i = 1; i <= testCase; i++) {
      final BattleGround battleGround = new BattleGround(scanner);
      battleGround.solve();
      System.out.println("Case " + i + ": " + battleGround.getShipCount());
    }
  }
}

class BattleGround {
  private final String[] matrix;
  private final boolean[][] visited;
  private final int gridSize;

  private int shipCount;

  public BattleGround(Scanner scanner) {
    gridSize = scanner.nextInt();
    scanner.nextLine();

    matrix = new String[gridSize];
    visited = new boolean[gridSize][gridSize];
    for (int i = 0; i < gridSize; i++) {
      matrix[i] = scanner.nextLine();
    }
  }

  public void solve() {
    for (int r = 0; r < gridSize; r++) {
      for (int c = 0; c < gridSize; c++) {
        if (!visited[r][c] && isShip(r, c)) {
          shipCount++;
          dfs(r, c);
        }
      }
    }
  }

  private boolean isShip(int r, int c) {
    return matrix[r].charAt(c) == 'x';
  }

  private void dfs(int r, int c) {
    if (r < 0 || r >= gridSize || c < 0 || c >= gridSize) {
      return;
    }
    if (visited[r][c] || isWater(r, c)) {
      return;
    }
    visited[r][c] = true;

    dfs(r + 1, c);
    dfs(r - 1, c);
    dfs(r, c + 1);
    dfs(r, c - 1);
  }

  private boolean isWater(int r, int c) {
    return matrix[r].charAt(c) == '.';
  }

  public int getShipCount() {
    return shipCount;
  }
}
