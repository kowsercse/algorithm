package com.kowsercse.uva.V119;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

public class P11991 {

  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    while (scanner.hasNext()) {
      int totalNumber = scanner.nextInt();
      int totalTestCase = scanner.nextInt();
      HashMap<Integer, List<Integer>> map = new HashMap<>();

      int index = 1;
      while (totalNumber-- > 0) {
        int number = scanner.nextInt();
        List<Integer> indexByOccurrence = map.computeIfAbsent(number, ignore -> new ArrayList<>());
        indexByOccurrence.add(index++);
      }

      while (totalTestCase-- > 0) {
        int occurrence = scanner.nextInt();
        int number = scanner.nextInt();

        List<Integer> indexByOccurrence = map.get(number);
        if (occurrence <= indexByOccurrence.size()) {
          System.out.println(indexByOccurrence.get(occurrence - 1));
        } else {
          System.out.println(0);
        }
      }
    }
  }
}
