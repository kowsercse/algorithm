package com.kowsercse.uva.V101;

import java.util.ArrayList;
import java.util.Scanner;

public class P10147 {

  public void solve() {
    final Scanner scanner = new Scanner(System.in);
    int testCase = scanner.nextInt();
    while (testCase-- > 0) {
      final int totalTown = scanner.nextInt();
      final ArrayList<Point> vertices = new ArrayList<>(totalTown);
      for (int i = 0; i < totalTown; i++) {
        final Point point = new Point(scanner.nextInt(), scanner.nextInt(), i);
        vertices.set(i, point);
      }

      final Graph graph = new Graph();

      int totalRoads = scanner.nextInt();
      for (int i = 0; i < totalRoads; i++) {
        final Point start = vertices.get(scanner.nextInt());
        final Point end = vertices.get(scanner.nextInt());

        graph.join(start, end);
      }
    }
  }
}

class Graph {

  public void join(final Point left, final Point right) {
    left.root = right;
    if (right.root == null) {
      right.root = right;
    }
  }
}

class Point {

  final int x;
  final int y;
  final int position;

  Point root;

  Point(final int x, final int y, final int position) {
    this.x = x;
    this.y = y;
    this.position = position;
  }
}
