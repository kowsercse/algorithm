package com.kowsercse.uva.V113;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class P11340 {
  /** Buffer reader solves TLE */
  public static void main(String[] args) throws IOException {
    BufferedReader scanner = new BufferedReader(new InputStreamReader(System.in));
    int testCase = Integer.parseInt(scanner.readLine());
    while (testCase-- > 0) {
      int[] costByCharacter = getCharacterIntegerMap(scanner);
      int totalLine = Integer.parseInt(scanner.readLine());
      long totalCost = 0;
      while (totalLine-- > 0) {
        String line = scanner.readLine();
        for (int i = 0; i < line.length(); i++) {
          char c = line.charAt(i);
          if (c < 256 && costByCharacter[c] != 0) {
            totalCost += costByCharacter[c];
          }
        }
      }

      System.out.printf("%d.%02d$\n", totalCost / 100, totalCost % 100);
    }
  }

  private static int[] getCharacterIntegerMap(BufferedReader scanner) throws IOException {
    int totalCharacters = Integer.parseInt(scanner.readLine());
    int[] costByCharacter = new int[256];
    while (totalCharacters-- > 0) {
      StringTokenizer tokenizer = new StringTokenizer(scanner.readLine());

      costByCharacter[tokenizer.nextToken().charAt(0)] = Integer.parseInt(tokenizer.nextToken());
    }
    return costByCharacter;
  }
}
