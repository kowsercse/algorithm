package com.kowsercse.uva.V113;

import java.util.*;

public class P10305 {
  public static void main(String[] args) {
    final Scanner scanner = new Scanner(System.in);
    int totalNode = scanner.nextInt();
    int totalEdge = scanner.nextInt();

    while (totalNode != 0 || totalEdge != 0) {
      final Map<Integer, List<Integer>> graph = readGraph(scanner, totalEdge);
      final Stack<Integer> sortedNode = topologicalSort(graph, totalNode);

      final StringJoiner stringJoiner = new StringJoiner(" ");
      while (!sortedNode.isEmpty()) {
        stringJoiner.add(Integer.toString(sortedNode.pop()));
      }
      System.out.println(stringJoiner.toString());

      totalNode = scanner.nextInt();
      totalEdge = scanner.nextInt();
    }
  }

  private static Stack<Integer> topologicalSort(Map<Integer, List<Integer>> graph, int totalNode) {
    final Boolean[] nodeVisitStatus = new Boolean[totalNode + 1];
    final Stack<Integer> sortedNode = new Stack<>();

    for (int i = 1; i <= totalNode; i++) {
      dfs(i, graph, nodeVisitStatus, sortedNode);
    }
    return sortedNode;
  }

  private static void dfs(
      int node,
      Map<Integer, List<Integer>> graph,
      Boolean[] nodeVisitStatus,
      Stack<Integer> sortedNode) {
    if (nodeVisitStatus[node] != null) {
      return;
    }
    nodeVisitStatus[node] = false;
    for (int destination : graph.getOrDefault(node, new LinkedList<>())) {
      dfs(destination, graph, nodeVisitStatus, sortedNode);
    }

    nodeVisitStatus[node] = true;
    sortedNode.push(node);
  }

  private static Map<Integer, List<Integer>> readGraph(Scanner scanner, int totalEdge) {
    final Map<Integer, List<Integer>> graph = new HashMap<>();
    for (int i = 0; i < totalEdge; i++) {
      final int sourceNode = scanner.nextInt();
      final int destinationNode = scanner.nextInt();
      graph.computeIfAbsent(sourceNode, ignore -> new LinkedList<>()).add(destinationNode);
    }
    return graph;
  }
}
