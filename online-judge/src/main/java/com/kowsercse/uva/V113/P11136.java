package com.kowsercse.uva.V113;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;
import java.util.TreeMap;

public class P11136 {

  public static void main(String[] args) throws IOException {
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    int totalPromotionDay = Integer.parseInt(reader.readLine());
    while (totalPromotionDay != 0) {
      final TreeMap<Integer, Integer> frequencyByNumber = new TreeMap<>();

      long totalReward = 0;
      while (totalPromotionDay-- > 0) {
        StringTokenizer tokenizer = new StringTokenizer(reader.readLine());
        int totalBills = Integer.parseInt(tokenizer.nextToken());

        while (totalBills-- > 0) {
          int billAmount = Integer.parseInt(tokenizer.nextToken());
          frequencyByNumber.put(billAmount, frequencyByNumber.getOrDefault(billAmount, 0) + 1);
        }

        Integer min = frequencyByNumber.firstKey();
        Integer max = frequencyByNumber.lastKey();

        if (frequencyByNumber.get(min) == 1) {
          frequencyByNumber.remove(min);
        } else {
          frequencyByNumber.put(min, frequencyByNumber.get(min) - 1);
        }
        if (frequencyByNumber.get(max) == 1) {
          frequencyByNumber.remove(max);
        } else {
          frequencyByNumber.put(max, frequencyByNumber.get(max) - 1);
        }

        long reward = max - min;
        totalReward += reward;
      }

      System.out.println(totalReward);
      totalPromotionDay = Integer.parseInt(reader.readLine());
    }
  }
}
