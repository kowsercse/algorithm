package com.kowsercse.uva.V7;

import java.util.LinkedList;
import java.util.Scanner;
import java.util.Stack;

public class P732 {
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);

    while (scanner.hasNext()) {
      Solution solution = new Solution(scanner.next(), scanner.next());
      System.out.println('[');
      solution.anagramByStack(new Stack<>(), 0, 0, new LinkedList<>());
      System.out.println(']');
    }
  }
}

class Solution {
  private final String source;
  private final String destination;

  public Solution(String source, String destination) {
    this.source = source;
    this.destination = destination;
  }

  public void anagramByStack(
      Stack<Character> stack, int i, int j, LinkedList<String> selectedPath) {
    if (stack.isEmpty() && j == destination.length()) {
      System.out.println(String.join(" ", selectedPath));
      return;
    }

    if (i < source.length()) {
      selectedPath.addLast("i");
      stack.push(source.charAt(i));
      anagramByStack(stack, i + 1, j, selectedPath);
      stack.pop();
      selectedPath.removeLast();
    }

    if (!stack.isEmpty()
        && j < destination.length()
        && stack.peek().equals(destination.charAt(j))) {
      selectedPath.addLast("o");
      Character pop = stack.pop();
      anagramByStack(stack, i, j + 1, selectedPath);
      stack.push(pop);
      selectedPath.removeLast();
    }
  }
}
