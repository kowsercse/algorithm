package com.kowsercse.uva.V7;

import java.util.Scanner;
import java.util.StringTokenizer;

public class P793 {

  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);

    int testCase = Integer.parseInt(scanner.nextLine());
    scanner.nextLine();

    boolean eof = false;
    while (testCase-- > 0) {
      int totalSuccess = 0, totalFailure = 0;
      UnionFind forest = new UnionFind(Integer.parseInt(scanner.nextLine()));

      String line = scanner.nextLine();
      while (!line.isEmpty()) {
        StringTokenizer tokenizer = new StringTokenizer(line);
        if (tokenizer.nextToken().charAt(0) == 'c') {
          forest.connect(
              Integer.parseInt(tokenizer.nextToken()), Integer.parseInt(tokenizer.nextToken()));
        } else {
          if (forest.isConnected(
              Integer.parseInt(tokenizer.nextToken()), Integer.parseInt(tokenizer.nextToken()))) {
            totalSuccess++;
          } else {
            totalFailure++;
          }
        }

        if (!scanner.hasNext()) {
          eof = true;
          break;
        }
        line = scanner.nextLine();
      }
      System.out.println(totalSuccess + "," + totalFailure);
      if (!eof) {
        System.out.println();
      }
    }
  }
}

class UnionFind {

  private final int[] rootByNode;

  public UnionFind(int totalNode) {
    rootByNode = new int[totalNode + 1];
  }

  public void connect(int first, int second) {
    union(getRoot(first), getRoot(second));
  }

  public boolean isConnected(int first, int second) {
    return getRoot(first) == getRoot(second);
  }

  private void union(int node1, int node2) {
    int root1 = getRoot(node1);
    int root2 = getRoot(node2);
    if (root1 >= root2) {
      rootByNode[root1] = root2;
    } else {
      rootByNode[root2] = root1;
    }
  }

  private int getRoot(int node) {
    if (rootByNode[node] == 0) {
      rootByNode[node] = node;
      return node;
    }
    int root = node;
    while (rootByNode[root] != root) {
      root = rootByNode[root];
    }

    int parent = rootByNode[node];
    while (node != parent) {
      rootByNode[node] = root;
      node = parent;
      parent = rootByNode[parent];
    }

    return root;
  }
}
