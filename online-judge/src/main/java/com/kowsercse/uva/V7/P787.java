package com.kowsercse.uva.V7;

import java.math.BigInteger;
import java.util.Scanner;
import java.util.StringTokenizer;

public class P787 {
  private static final BigInteger END_INDICATOR = BigInteger.valueOf(-999999L);

  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    while (scanner.hasNext()) {

      StringTokenizer tokenizer = new StringTokenizer(scanner.nextLine());
      BigInteger[] numbers = new BigInteger[200];
      BigInteger input = new BigInteger(tokenizer.nextToken(), 10);
      int totalNumber = 0;

      while (!END_INDICATOR.equals(input)) {
        numbers[totalNumber++] = input;
        input = new BigInteger(tokenizer.nextToken(), 10);
      }

      BigInteger globalMaximum = null;
      BigInteger[][] table = new BigInteger[totalNumber][totalNumber];
      for (int r = 0; r < totalNumber; r++) {
        table[r][r] = numbers[r];
        if (globalMaximum == null || globalMaximum.compareTo(numbers[r]) < 0) {
          globalMaximum = numbers[r];
        }

        for (int c = 0; c < r; c++) {
          table[r][c] =
              table[r - 1][c].compareTo(BigInteger.ZERO) == 0
                  ? numbers[r]
                  : table[r - 1][c].multiply(numbers[r]);
          if (globalMaximum.compareTo(table[r][c]) < 0) {
            globalMaximum = table[r][c];
          }
        }
      }

      System.out.println(globalMaximum);
    }
  }
}
