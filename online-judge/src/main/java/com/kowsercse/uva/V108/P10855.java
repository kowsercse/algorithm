package com.kowsercse.uva.V108;

import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class P10855 {

  public static void main(String[] args) {
    new P10855().solve();
  }

  private Scanner scanner = new Scanner(System.in);

  public static void printTable(final int[][] table) {
    for (final int[] row : table) {
      for (final int i : row) {
        System.err.print(i + "\t");
      }
      System.err.println();
    }
  }

  void solve() {
    StringTokenizer tokenizer = new StringTokenizer(scanner.nextLine());
    int biggerMatrixSize = Integer.parseInt(tokenizer.nextToken());
    int smallerMatrixSize = Integer.parseInt(tokenizer.nextToken());
    while (biggerMatrixSize != 0 && smallerMatrixSize != 0) {
      solve(biggerMatrixSize, smallerMatrixSize);

      tokenizer = new StringTokenizer(scanner.nextLine());
      biggerMatrixSize = Integer.parseInt(tokenizer.nextToken());
      smallerMatrixSize = Integer.parseInt(tokenizer.nextToken());
    }
  }

  private void solve(int biggerMatrixSize, int smallerMatrixSize) {
    Matrix biggerMatrix = new Matrix(biggerMatrixSize, scanner);

    Matrix[] smallerMatrix = new Matrix[4];
    smallerMatrix[0] = new Matrix(smallerMatrixSize, scanner);
    smallerMatrix[1] = new Matrix90(smallerMatrix[0].matrix, smallerMatrixSize);
    smallerMatrix[2] = new Matrix180(smallerMatrix[0].matrix, smallerMatrixSize);
    smallerMatrix[3] = new Matrix270(smallerMatrix[0].matrix, smallerMatrixSize);

    int[][] hash = biggerMatrix.preCalculateHash(smallerMatrixSize);
    //    printTable(hash);

    int firstHash = smallerMatrix[0].calculateHash(0, smallerMatrixSize);
    int[] contains = new int[4];
    for (int r = 0; r < hash.length; r++) {
      for (int c = 0; c < hash.length; c++) {
        if (firstHash == hash[r][c]) {
          for (int i = 0; i < 4; i++) {
            if (biggerMatrix.contains(smallerMatrix[i], r, c)) {
              contains[i]++;
            }
          }
        }
      }
    }

    System.out.println(
        IntStream.of(contains).mapToObj(String::valueOf).collect(Collectors.joining(" ")));
  }
}

class Matrix270 extends Matrix {

  public Matrix270(char[][] matrix, int size) {
    super(matrix, size);
  }

  char get(int r, int c) {
    return matrix[c][size - r - 1];
  }
}

class Matrix180 extends Matrix {
  public Matrix180(char[][] matrix, int size) {
    super(matrix, size);
  }

  char get(int r, int c) {
    return matrix[size - r - 1][size - c - 1];
  }
}

class Matrix90 extends Matrix {

  public Matrix90(char[][] matrix, int size) {
    super(matrix, size);
  }

  char get(int r, int c) {
    return matrix[size - 1 - c][r];
  }
}

class Matrix {

  final char[][] matrix;
  final int size;

  public Matrix(char[][] matrix, int size) {
    this.matrix = matrix;
    this.size = size;
  }

  Matrix(int size, Scanner scanner) {
    this.size = size;
    matrix = new char[size][];
    for (int row = 0; row < size; row++) {
      matrix[row] = scanner.nextLine().toCharArray();
    }
  }

  int calculateHash(int startingColumn, int size) {
    int hash = 0;
    for (int r = 0; r < size; r++) {
      for (int c = startingColumn; c < startingColumn + size; c++) {
        hash += matrix[r][c];
      }
    }
    return hash;
  }

  int[][] preCalculateHash(int smallerMatrixSize) {
    int hashSize = size - smallerMatrixSize + 1;
    int[][] hash = new int[hashSize][hashSize];
    for (int c = 0; c < hash.length; c++) {
      hash[0][c] = calculateHash(c, smallerMatrixSize);
    }

    for (int r = 1; r < hash.length; r++) {
      for (int c = 0; c < hash.length; c++) {
        int old = getRowHash(matrix[r - 1], c, c + smallerMatrixSize);
        int current = getRowHash(matrix[r + smallerMatrixSize - 1], c, c + smallerMatrixSize);
        hash[r][c] = hash[r - 1][c] - old + current;
      }
    }
    return hash;
  }

  private int getRowHash(char[] string, int start, int end) {
    int sum = 0;
    for (int column = start; column < end; column++) {
      sum += string[column];
    }
    return sum;
  }

  public boolean contains(Matrix smallerMatrix, int startRow, int startColumn) {
    for (int r = startRow; r < startRow + smallerMatrix.size; r++) {
      for (int c = startColumn; c < startColumn + smallerMatrix.size; c++) {
        if (matrix[r][c] != smallerMatrix.get(r - startRow, c - startColumn)) {
          return false;
        }
      }
    }
    return true;
  }

  char get(int r, int c) {
    return matrix[r][c];
  }
}
