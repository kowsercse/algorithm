package com.kowsercse.uva.V108;

import java.util.*;

/**
 * @author kowsercse@gmail.com
 */
public class P10819 {

  private int evaluateZeroOneKnapsack(int w, final List<Item> items) {
    final int size = items.size();
    int W = w + 200;
    int table[][] = new int[size + 1][W + 1];

    for (int i = 1; i <= size; i++) {
      for (int j = 1; j <= W; j++) {
        final Item item = items.get(i - 1);
        table[i][j] =
            j < item.weight
                ? table[i - 1][j]
                : Math.max(table[i - 1][j - item.weight] + item.value, table[i - 1][j]);
      }
    }

    if (w > 1800) {
      int i = size;
      while (table[i][W] == table[i][2000] && table[size][W] == table[i][2000] && i > 0) {
        i--;
      }
      return Math.max(table[size][w], table[i][W]);
    } else {
      return table[size][w];
    }
  }

  public void solve() {
    final Scanner scanner = new Scanner(System.in);
    while (scanner.hasNextInt()) {
      final int m = scanner.nextInt();
      final int n = scanner.nextInt();
      final List<Item> items = new ArrayList<>();

      for (int i = 0; i < n; i++) {
        final Item item = new Item();
        item.weight = scanner.nextInt();
        item.value = scanner.nextInt();
        items.add(item);
      }
      Collections.sort(items);
      final int outcome = evaluateZeroOneKnapsack(m, items);

      System.out.println(outcome);
    }
  }

  static class Item implements Comparable<Item> {
    int weight;
    int value;

    @Override
    public int compareTo(final Item o) {
      return value - o.value;
    }

    @Override
    public String toString() {
      return "Item{" + "weight=" + weight + ", value=" + value + '}';
    }
  }

  public static void main(String[] args) {
    new P10819().solve();
  }
}
