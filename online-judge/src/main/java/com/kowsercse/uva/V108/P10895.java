package com.kowsercse.uva.V108;

import java.util.Collections;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.StringJoiner;
import java.util.StringTokenizer;
import java.util.TreeMap;

public class P10895 {

  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);

    while (scanner.hasNext()) {
      Map<Integer, Map<Integer, Integer>> sparseMatrix = new TreeMap<>();
      StringTokenizer tokenizer = new StringTokenizer(scanner.nextLine());
      int m = Integer.parseInt(tokenizer.nextToken());
      int n = Integer.parseInt(tokenizer.nextToken());

      for (int row = 1; row <= m; row++) {
        StringTokenizer columnIndexTokenizer = new StringTokenizer(scanner.nextLine());
        StringTokenizer rowValues = new StringTokenizer(scanner.nextLine());
        int totalColumnIndex = Integer.parseInt(columnIndexTokenizer.nextToken());
        for (int i = 0; i < totalColumnIndex; i++) {
          int columnIndex = Integer.parseInt(columnIndexTokenizer.nextToken());
          int value = Integer.parseInt(rowValues.nextToken());
          sparseMatrix.computeIfAbsent(columnIndex, ignore -> new TreeMap<>()).put(row, value);
        }
      }

      System.out.println(n + " " + m);
      for (int row = 1; row <= n; row++) {
        StringJoiner columnIndexJoiner = new StringJoiner(" ");
        StringJoiner rowValueJoiner = new StringJoiner(" ");
        Map<Integer, Integer> rowValueByColumnIndex =
            sparseMatrix.computeIfAbsent(row, ignore -> Collections.emptyMap());
        columnIndexJoiner.add(String.valueOf(rowValueByColumnIndex.size()));
        for (Entry<Integer, Integer> entry : rowValueByColumnIndex.entrySet()) {
          columnIndexJoiner.add(String.valueOf(entry.getKey()));
          rowValueJoiner.add(String.valueOf(entry.getValue()));
        }

        System.out.println(columnIndexJoiner.toString());
        System.out.println(rowValueJoiner.toString());
      }
    }
  }
}
