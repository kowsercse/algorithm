package com.kowsercse.uva.V123;

import static java.lang.Integer.max;
import static java.lang.Integer.min;

import java.util.Scanner;
import java.util.TreeSet;

public class P12356 {
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);

    int totalSoldiers = scanner.nextInt();
    int totalReport = scanner.nextInt();
    while (totalSoldiers != 0 || totalReport != 0) {
      Solution3 solution = new Solution3(totalSoldiers);
      for (int i = 0; i < totalReport; i++) {
        solution.printReport(scanner.nextInt(), scanner.nextInt());
      }

      System.out.println("-");
      totalSoldiers = scanner.nextInt();
      totalReport = scanner.nextInt();
    }
  }
}

class Solution3 {
  Buddy[] buddies;

  public Solution3(int totalSoldiers) {
    buddies = new Buddy[totalSoldiers + 1];
    buddies[totalSoldiers] = new Buddy(totalSoldiers - 1, 0);
  }

  public void printReport(final int left, final int right) {
    Buddy leftBuddy = getBuddy(left);
    Buddy rightBuddy = getBuddy(right);

    getBuddy(leftBuddy.left).right = rightBuddy.right;
    getBuddy(rightBuddy.right).left = leftBuddy.left;

    System.out.printf(
        "%s %s%n",
        leftBuddy.left == 0 ? "*" : leftBuddy.left, rightBuddy.right == 0 ? "*" : rightBuddy.right);
  }

  private Buddy getBuddy(int id) {
    if (buddies[id] == null) {
      buddies[id] = new Buddy(id - 1, id + 1);
    }
    return buddies[id];
  }
}

class Buddy {
  int left;
  int right;

  public Buddy(int left, int right) {
    this.left = left;
    this.right = right;
  }
}

class Solution2 {
  Integer[] leftBuddy;
  Integer[] rightBuddy;

  public Solution2(int totalSoldiers) {
    leftBuddy = new Integer[totalSoldiers + 1];
    rightBuddy = new Integer[totalSoldiers + 1];
    for (int i = 1; i <= totalSoldiers; i++) {
      leftBuddy[i] = i - 1;
      rightBuddy[i] = i + 1;
    }
    rightBuddy[totalSoldiers] = 0;
  }

  public void printReport(final int left, final int right) {
    rightBuddy[leftBuddy[left]] = rightBuddy[right];
    leftBuddy[rightBuddy[right]] = leftBuddy[left];

    Integer start = leftBuddy[left];
    Integer end = rightBuddy[right];
    System.out.printf("%s %s%n", start == 0 ? "*" : start, end == 0 ? "*" : end);
  }
}

class Solution {
  TreeSet<Integer> soldiers = new TreeSet<>();

  public Solution(int totalSoldiers) {
    for (int i = 1; i <= totalSoldiers; i++) {
      soldiers.add(i);
    }
  }

  public void printReport(final int left, final int right) {
    if (soldiers.isEmpty()) {
      System.out.println("* *");
      return;
    }

    soldiers.subSet(max(left, soldiers.first()), true, min(right, soldiers.last()), true).clear();

    Integer start = soldiers.lower(left);
    Integer end = soldiers.higher(right);
    System.out.printf("%s %s%n", start == null ? "*" : start, end == null ? "*" : end);
  }
}
