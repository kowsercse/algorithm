package com.kowsercse.uva.V102;

import java.util.*;

public class P10258 {
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    int testCase = Integer.parseInt(scanner.nextLine());
    scanner.nextLine();
    while (testCase-- > 0) {
      HashMap<Integer, Participant> participantsById = new HashMap<>();

      String submissionRecord = scanner.nextLine();
      while (!submissionRecord.isEmpty()) {
        StringTokenizer tokenizer = new StringTokenizer(submissionRecord);
        participantsById
            .computeIfAbsent(Integer.parseInt(tokenizer.nextToken()), Participant::new)
            .addSubmission(tokenizer);

        submissionRecord = scanner.hasNext() ? scanner.nextLine() : "";
      }

      List<Participant> participants = new ArrayList<>(participantsById.values());
      for (Participant participant : participants) {
        participant.calculateScore();
      }
      Collections.sort(participants);

      for (Participant participant : participants) {
        System.out.println(participant.recordAsString());
      }

      if (testCase != 0) {
        System.out.println();
      }
    }
  }
}

class Problem {

  final Integer problemId;
  int incorrectSubmissionCount;
  Integer penalty;

  public Problem(Integer problemId) {
    this.problemId = problemId;
  }

  public void addSubmission(StringTokenizer tokenizer) {
    int time = Integer.parseInt(tokenizer.nextToken());
    String submissionType = tokenizer.nextToken();
    if ("I".equals(submissionType)) {
      incorrectSubmissionCount++;
    } else if ("C".equals(submissionType) && penalty == null) {
      penalty = incorrectSubmissionCount * 20 + time;
    }
  }
}

class Participant implements Comparable<Participant> {

  final int id;
  final Map<Integer, Problem> problemsById = new HashMap<>();
  int penalty;
  int correctSubmission;

  public Participant(Integer id) {
    this.id = id;
  }

  public void addSubmission(StringTokenizer tokenizer) {
    problemsById
        .computeIfAbsent(Integer.parseInt(tokenizer.nextToken()), Problem::new)
        .addSubmission(tokenizer);
  }

  @Override
  public int compareTo(Participant o) {
    if (correctSubmission != o.correctSubmission) {
      return Integer.compare(o.correctSubmission, correctSubmission);
    }
    if (penalty != o.penalty) {
      return Integer.compare(penalty, o.penalty);
    }
    return Integer.compare(id, o.id);
  }

  public void calculateScore() {
    penalty = 0;
    correctSubmission = 0;
    for (Problem problem : problemsById.values()) {
      if (problem.penalty != null) {
        correctSubmission++;
        penalty += problem.penalty;
      }
    }
  }

  public String recordAsString() {
    return String.format("%d %d %d", id, correctSubmission, penalty);
  }
}
