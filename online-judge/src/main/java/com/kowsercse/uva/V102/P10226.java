package com.kowsercse.uva.V102;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.TreeMap;

public class P10226 {
  public static void main(String[] args) throws IOException {
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    int testCase = Integer.parseInt(reader.readLine());
    reader.readLine();

    while (testCase-- > 0) {
      TreeMap<String, Integer> countByTree = new TreeMap<>();

      int totalTree = 0;
      String tree = reader.readLine();
      while (tree != null && !tree.isEmpty()) {
        countByTree.put(tree, countByTree.getOrDefault(tree, 0) + 1);
        totalTree++;

        tree = reader.readLine();
      }

      double total = totalTree;
      countByTree.forEach(
          (key, value) -> System.out.printf("%s %1.4f%n", key, (100 * value) / total));

      if (testCase != 0) {
        System.out.println();
      }
    }
  }
}
