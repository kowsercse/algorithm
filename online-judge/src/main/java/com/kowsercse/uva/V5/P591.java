package com.kowsercse.uva.V5;

import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class P591 {

  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    int totalStack = scanner.nextInt();

    int iteration = 0;
    while (totalStack != 0) {
      List<Integer> stackSize = new LinkedList<>();
      for (int i = 0; i < totalStack; i++) {
        stackSize.add(scanner.nextInt());
      }

      int average = stackSize.stream().mapToInt(Integer::intValue).sum() / totalStack;
      int move =
          stackSize.stream().mapToInt(Integer::intValue).map(a -> Math.abs(a - average)).sum() / 2;

      System.out.println("Set #" + ++iteration);
      System.out.println("The minimum number of moves is " + move + ".");
      System.out.println();

      totalStack = scanner.nextInt();
    }
  }
}
