package com.kowsercse.uva.V5;

import java.util.Scanner;
import java.util.Stack;

public class P514 {
  private final Scanner scanner = new Scanner(System.in);

  void solve() {
    int totalCoaches = scanner.nextInt();
    while (totalCoaches != 0) {

      int firstCoach = scanner.nextInt();
      while (firstCoach != 0) {
        int[] expectedCoachOrder = readExpectedCoachOrder(totalCoaches, firstCoach);

        boolean requiredOrderPossible = isRequiredOrderPossible(expectedCoachOrder);
        System.out.println(requiredOrderPossible ? "Yes" : "No");

        firstCoach = scanner.nextInt();
      }

      System.out.println();
      totalCoaches = scanner.nextInt();
    }
  }

  private boolean isRequiredOrderPossible(int[] expectedCoachOrder) {
    int processedCoachId = 0;
    Stack<Integer> stack = new Stack<>();
    for (int expectedCoachId : expectedCoachOrder) {
      if (processedCoachId < expectedCoachId) {
        for (int coachId = processedCoachId + 1; coachId <= expectedCoachId; coachId++) {
          stack.push(coachId);
        }
        processedCoachId = expectedCoachId;
      }

      if (stack.peek() != expectedCoachId) {
        return false;
      }
      stack.pop();
    }
    return true;
  }

  private int[] readExpectedCoachOrder(int totalCoaches, int firstCoach) {
    int[] expectedOrder = new int[totalCoaches];
    expectedOrder[0] = firstCoach;
    for (int i = 1; i < totalCoaches; i++) {
      expectedOrder[i] = scanner.nextInt();
    }
    return expectedOrder;
  }

  public static void main(String[] args) {
    new P514().solve();
  }
}
