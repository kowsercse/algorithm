package com.kowsercse.uva.V1;

import java.util.Scanner;
import java.util.StringJoiner;

public class P116 {
  Scanner scanner = new Scanner(System.in);

  void solve() {
    while (scanner.hasNext()) {
      int row = scanner.nextInt();
      int column = scanner.nextInt();

      int[][] table = new int[row][column];
      for (int r = 0; r < row; r++) {
        for (int c = 0; c < column; c++) {
          table[r][c] = scanner.nextInt();
        }
      }

      int[][] tspTable = createTspTable(row, column, table);
      //      printTable(table);
      //      printTable(tspTable);

      int minCost = Integer.MAX_VALUE;
      int tsp[] = new int[column];
      for (int r = 0; r < row; r++) {
        if (tspTable[r][0] < minCost) {
          minCost = tspTable[r][0];
          tsp[0] = r;
        }
      }

      for (int c = 1; c < column; c++) {
        int r = tsp[c - 1];
        if (tspTable[(r - 1 + row) % row][c] < tspTable[r][c]) {
          tsp[c] = (r - 1 + row) % row;
        } else {
          tsp[c] = r;
        }
        if (tspTable[(r + 1) % row][c] < tspTable[tsp[c]][c]) {
          tsp[c] = (r + 1) % row;
        }

        if (tspTable[tsp[c]][c] == tspTable[(r - 1 + row) % row][c]) {
          tsp[c] = Math.min(tsp[c], (r - 1 + row) % row);
        }
        if (tspTable[tsp[c]][c] == tspTable[r][c]) {
          tsp[c] = Math.min(tsp[c], r);
        }
        if (tspTable[tsp[c]][c] == tspTable[(r + 1) % row][c]) {
          tsp[c] = Math.min(tsp[c], (r + 1) % row);
        }
      }

      StringJoiner stringJoiner = new StringJoiner(" ");
      for (int i : tsp) {
        stringJoiner.add(Integer.toString(i + 1));
      }
      System.out.println(stringJoiner.toString());
      System.out.println(minCost);
    }
  }

  private int[][] createTspTable(int row, int column, int[][] table) {
    int lastColumn = column - 1;
    int tsp[][] = new int[row][column];
    for (int r = 0; r < row; r++) {
      tsp[r][lastColumn] = table[r][lastColumn];
    }

    for (int c = lastColumn - 1; c >= 0; c--) {
      for (int r = 0; r < row; r++) {
        tsp[r][c] =
            table[r][c]
                + min(tsp[(r - 1 + row) % row][c + 1], tsp[r][c + 1], tsp[(r + 1) % row][c + 1]);
      }
    }
    return tsp;
  }

  public static void printTable(final int[][] table) {
    System.err.println();
    for (final int[] row : table) {
      for (final int i : row) {
        System.err.print(i + "\t");
      }
      System.err.println();
    }
  }

  private int min(int a, int b, int c) {
    return Math.min(Math.min(a, b), c);
  }

  public static void main(String[] args) {
    new P116().solve();
  }
}
