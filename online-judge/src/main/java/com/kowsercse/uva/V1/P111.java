package com.kowsercse.uva.V1;

import java.util.*;

public class P111 {

  public void solve() {
    final Scanner scanner = new Scanner(System.in);
    final int n = scanner.nextInt();
    final List<Event> actualChronology = readChronology(scanner, n);

    while (scanner.hasNextInt()) {
      final List<Event> chronology = readChronology(scanner, n);
      System.out.println(chronology);
    }
    actualChronology.sort(Comparator.comparingInt(event -> event.rank));

    System.out.println(actualChronology);
  }

  private List<Event> readChronology(final Scanner scanner, final int n) {
    final List<Event> chronology = new ArrayList<>();

    for (int i = 0; i < n; i++) {
      final int rank = scanner.nextInt();
      chronology.add(new Event(i + 1, rank));
    }
    return chronology;
  }

  public static void main(String[] args) {
    final P111 p111 = new P111();
    p111.solve();
  }
}

class Event {
  final int event;
  final int rank;

  Event(final int event, final int rank) {
    this.event = event;
    this.rank = rank;
  }

  @Override
  public String toString() {
    return "Event{event=" + event + ", rank=" + rank + '}';
  }
}
