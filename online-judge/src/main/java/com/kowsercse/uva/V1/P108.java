package com.kowsercse.uva.V1;

import static java.lang.Integer.max;

import java.util.Scanner;

public class P108 {
  public static void main(String[] args) {
    final Scanner scanner = new Scanner(System.in);

    while (scanner.hasNextInt()) {
      final int dimension = scanner.nextInt();
      final int[][] matrix = readInput(scanner, dimension);
      final int[][] prefixSum = calculatePrefixSum(dimension, matrix);

      System.out.println(calculateMaximumSum(dimension, prefixSum, matrix));
    }
  }

  private static int calculateMaximumSum(int dimension, int[][] prefixSum, int[][] matrix) {
    int globalMaximumSum = Integer.MIN_VALUE;

    for (int start = 0; start < dimension; start++) {
      for (int end = start; end < dimension; end++) {
        int rowMaximumSum = prefixSum[end][0] - prefixSum[start][0] + matrix[start][0];
        globalMaximumSum = max(globalMaximumSum, rowMaximumSum);
        for (int c = 1; c < dimension; c++) {
          final int value = prefixSum[end][c] - prefixSum[start][c] + matrix[start][c];
          rowMaximumSum = max(value, rowMaximumSum + value);
          globalMaximumSum = max(globalMaximumSum, rowMaximumSum);
        }
      }
    }

    return globalMaximumSum;
  }

  private static int[][] calculatePrefixSum(int dimension, int[][] matrix) {
    final int[][] prefixSum = new int[dimension][dimension];
    prefixSum[0] = matrix[0];
    for (int r = 1; r < dimension; r++) {
      for (int c = 0; c < dimension; c++) {
        prefixSum[r][c] = matrix[r][c] + prefixSum[r - 1][c];
      }
    }
    return prefixSum;
  }

  private static int[][] readInput(Scanner scanner, int dimension) {
    final int[][] matrix = new int[dimension][dimension];
    for (int r = 0; r < dimension; r++) {
      for (int c = 0; c < dimension; c++) {
        matrix[r][c] = scanner.nextInt();
      }
    }
    return matrix;
  }
}
