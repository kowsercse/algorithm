package com.kowsercse.uva.V1;

import java.util.*;

public class P193 {
  Scanner scanner = new Scanner(System.in);

  public static void main(String[] args) {
    new P193().solve();
  }

  void solve() {
    int totalGraphs = scanner.nextInt();
    while (totalGraphs-- > 0) {
      Graph graph = new Graph(scanner);
      graph.solve2();
    }
  }
}

class Graph {
  private final int totalNode;
  private final int totalEdge;
  private final boolean[][] graph;
  private final List<List<Integer>> edges;

  public Graph(Scanner scanner) {
    totalNode = scanner.nextInt();
    totalEdge = scanner.nextInt();
    graph = new boolean[totalNode + 1][totalNode + 1];
    edges = new ArrayList<>();

    edges.add(null);
    for (int i = 1; i <= totalNode; i++) {
      edges.add(new LinkedList<>());
    }

    for (int i = 0; i < totalEdge; i++) {
      int source = scanner.nextInt();
      int destination = scanner.nextInt();

      graph[source][destination] = true;
      graph[destination][source] = true;

      edges.get(source).add(destination);
      edges.get(destination).add(source);
    }
  }

  public void solve2() {
    Boolean[] color = new Boolean[totalNode + 1];
    boolean[] visited = new boolean[totalNode + 1];
    ArrayList<Integer> nodes = new ArrayList<>();
    for (int i = 0; i < totalNode; i++) {
      nodes.add(i + 1);
    }

    for (int i = 0; i < totalNode; i++) {
      nodes.sort(
          Comparator.<Integer, Boolean>comparing(node -> visited[node])
              .thenComparingInt(node -> availableEdges(node, visited, color))
              .thenComparing(node -> -node));
      Integer currentNode = nodes.get(0);
      if (color[currentNode] == null) {
        visited[currentNode] = true;
        color[currentNode] = true;
        for (Integer nextNode : edges.get(currentNode)) {
          visited[nextNode] = true;
          color[nextNode] = false;
        }
      }
    }

    int x = 0;
    StringJoiner joiner = new StringJoiner(" ");
    for (int node = 1; node <= totalNode; node++) {
      if (color[node]) {
        x++;
        joiner.add(Integer.toString(node));
      }
    }
    System.out.println(x);
    System.out.println(joiner);
  }

  private int availableEdges(Integer node, boolean[] visited, Boolean[] color) {
    int unvisitedNodes = 0;
    for (Integer nextNode : edges.get(node)) {
      if (!visited[nextNode]) {
        unvisitedNodes++;
      }
    }
    return unvisitedNodes;
  }

  public void solve() {
    int globalMax = 0;
    Boolean[] maxSet = null;
    int humm = 0;
    for (int node = 1; node <= totalNode; node++) {
      Boolean[] color = new Boolean[totalNode + 1];
      color[node] = Boolean.TRUE;

      boolean hasSolution = dfs(node, color);
      if (hasSolution) {
        int localMax = getLocalMax(color);
        if (globalMax < localMax) {
          globalMax = localMax;
          maxSet = color;
          humm = node;
        }
      }
    }

    StringJoiner stringJoiner = new StringJoiner(" ");
    if (maxSet != null) {
      for (int node = 1; node <= totalNode; node++) {
        if (maxSet[node] == null || maxSet[node]) {
          stringJoiner.add(Integer.toString(node));
        }
      }
    }

    System.out.println("=> " + humm);
    System.out.println(globalMax);
    System.out.println(stringJoiner.toString());
  }

  private int getLocalMax(Boolean[] color) {
    int max = 0;
    for (int node = 1; node <= totalNode; node++) {
      if (color[node] == null || color[node]) {
        max++;
      }
    }
    return max;
  }

  private boolean dfs(int node, Boolean[] color) {
    boolean oppositeColor = !color[node];
    for (Integer destination : edges.get(node)) {
      if (color[destination] == null) {
        color[destination] = oppositeColor;
        boolean hasSolution = dfs(destination, color);
        if (!hasSolution) {
          return false;
        }
      } else if (color[node] && color[destination]) {
        return false;
      }
    }
    return true;
  }
}
