package com.kowsercse.uva.V1;

import java.util.*;

public class P105 {

  void solve() {
    PriorityQueue<Building.Strip> stripQueue = readInput();
    HeightQueue heightQueue = new HeightQueue();
    StringJoiner stringJoiner = new StringJoiner(" ");

    int height = 0;
    while (!stripQueue.isEmpty()) {
      int currentCoordinate = stripQueue.peek().getCoordinate();
      while (!stripQueue.isEmpty() && stripQueue.peek().getCoordinate() == currentCoordinate) {
        Building.Strip strip = stripQueue.poll();
        if (strip.isEndStrip) {
          heightQueue.popProcessed(currentCoordinate);
        } else {
          heightQueue.push(strip.building());
        }
      }

      if (heightQueue.height() != height) {
        height = heightQueue.height();
        stringJoiner.add(Integer.toString(currentCoordinate)).add(Integer.toString(height));
      }
    }

    System.out.println(stringJoiner.toString());
  }

  private PriorityQueue<Building.Strip> readInput() {
    PriorityQueue<Building.Strip> stripQueue = new PriorityQueue<>();
    Scanner scanner = new Scanner(System.in);
    while (scanner.hasNext()) {
      Building building = new Building(scanner.nextInt(), scanner.nextInt(), scanner.nextInt());
      stripQueue.add(building.start);
      stripQueue.add(building.end);
    }
    return stripQueue;
  }

  public static void main(String[] args) {
    new P105().solve();
  }
}

class HeightQueue {
  PriorityQueue<Building> queue =
      new PriorityQueue<>(Comparator.comparingInt(Building::getHeight).reversed());

  public void push(Building building) {
    queue.add(building);
  }

  int height() {
    return queue.isEmpty() ? 0 : queue.peek().getHeight();
  }

  void popProcessed(int currentCoordinate) {
    while (!queue.isEmpty() && queue.peek().end.getCoordinate() <= currentCoordinate) {
      queue.poll();
    }
  }
}

class Building {
  final Strip start;
  final Strip end;
  private final Building building;
  private boolean processed = false;

  Building(int start, int height, int end) {
    this.start = new Strip(start, height);
    this.end = new Strip(end, 0);
    building = this;
  }

  @Override
  public String toString() {
    return "("
        + start.coordinate
        + ","
        + start.height
        + ","
        + end.coordinate
        + ","
        + processed
        + ')';
  }

  public int getHeight() {
    return start.height;
  }

  class Strip implements Comparable<Strip> {
    private final int coordinate;
    private final int height;
    final boolean isEndStrip;

    public int getCoordinate() {
      return coordinate;
    }

    Strip(int coordinate, int height) {
      this.coordinate = coordinate;
      this.height = height;
      this.isEndStrip = height == 0;
    }

    @Override
    public String toString() {
      return "Strip{coordinate="
          + coordinate
          + ", height="
          + height
          + ", isEndStrip="
          + isEndStrip
          + '}';
    }

    @Override
    public int compareTo(Strip other) {
      if (coordinate != other.coordinate) {
        return Integer.compare(coordinate, other.coordinate);
      }
      if (other.isEndStrip != isEndStrip) {
        return Boolean.compare(other.isEndStrip, isEndStrip);
      }
      return Integer.compare(other.height, height);
    }

    public Building building() {
      return building;
    }
  }
}
