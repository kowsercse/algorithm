package com.kowsercse.uva.V1;

import java.util.*;
import java.util.stream.Collectors;

public class P103 {
  Scanner scanner = new Scanner(System.in);

  public static void main(String[] args) {
    new P103().solve();
  }

  void solve() {
    while (scanner.hasNext()) {
      int totalBox = scanner.nextInt();
      List<Box> boxes = readInput(totalBox);
      boxes.sort(Comparator.naturalOrder());
      int maxStackSize = 0;
      Box candidateBox = null;

      for (int i = 0; i < totalBox; i++) {
        Box biggerBox = boxes.get(i);
        for (int j = i - 1; j >= 0; j--) {
          biggerBox.accept(boxes.get(j));
        }
        if (maxStackSize < biggerBox.stackSize()) {
          maxStackSize = biggerBox.stackSize();
          candidateBox = biggerBox;
        }
      }

      Box temp = candidateBox;
      Stack<String> stack = new Stack<>();
      do {
        stack.push(Integer.toString(temp.id));
        temp = temp.nextBox;
      } while (temp != null);
      StringJoiner stringJoiner = new StringJoiner(" ");
      while (!stack.isEmpty()) {
        stringJoiner.add(stack.pop());
      }
      System.out.println(maxStackSize);
      System.out.println(stringJoiner.toString());
    }
  }

  private List<Box> readInput(int totalBox) {
    int dimension = scanner.nextInt();
    List<Box> boxes = new ArrayList<>();
    for (int i = 0; i < totalBox; i++) {
      boxes.add(new Box(i + 1, dimension));
    }
    return boxes;
  }

  class Box implements Comparable<Box> {
    private int id;
    private int dimension;
    private final List<Integer> lengths;
    private final Set<Box> coveredBox = new HashSet<>();
    private int maxCoveredByChild = 0;
    private Box nextBox;

    public Box(int id, int dimension) {
      this.id = id;
      this.dimension = dimension;
      lengths = new ArrayList<>(dimension);
      for (int i = 0; i < dimension; i++) {
        lengths.add(scanner.nextInt());
      }
      lengths.sort(Comparator.comparingInt(Integer::intValue).reversed());
    }

    @Override
    public int compareTo(Box other) {
      return lengths.get(0).compareTo(other.lengths.get(0));
    }

    public void accept(Box smallerBox) {
      if (isCovered(smallerBox) || !isSmaller(smallerBox)) {
        return;
      }

      coveredBox.add(smallerBox);
      if (maxCoveredByChild < smallerBox.stackSize()) {
        maxCoveredByChild = smallerBox.stackSize();
        nextBox = smallerBox;
      } else if (maxCoveredByChild == smallerBox.stackSize() && id > smallerBox.id) {
        nextBox = smallerBox;
      }

      for (Box box : smallerBox.coveredBox) {
        if (maxCoveredByChild < box.stackSize()) {
          maxCoveredByChild = box.stackSize();
          nextBox = box;
        } else if (maxCoveredByChild == box.stackSize() && id > box.id) {
          nextBox = box;
        }
        coveredBox.add(box);
      }
    }

    private int stackSize() {
      return 1 + maxCoveredByChild;
    }

    public boolean isSmaller(Box smallerBox) {
      if (isCovered(smallerBox)) {
        return true;
      }
      for (int i = 0; i < dimension; i++) {
        if (lengths.get(i) <= smallerBox.lengths.get(i)) {
          return false;
        }
      }
      return true;
    }

    private boolean isCovered(Box smallerBox) {
      return coveredBox.contains(smallerBox);
    }

    @Override
    public String toString() {
      return String.format(
          "%3d => %s | %2d:%3s | %s",
          id,
          lengths.stream()
              .map(integer -> String.format("%3d", integer))
              .collect(Collectors.joining(", ")),
          stackSize(),
          nextBoxId(),
          coveredBox.stream()
              .map(box -> Integer.toString(box.id))
              .collect(Collectors.joining(", ")));
    }

    private String nextBoxId() {
      return nextBox == null ? " " : Integer.toString(nextBox.id);
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) {
        return true;
      }
      if (o == null || getClass() != o.getClass()) {
        return false;
      }
      Box box = (Box) o;
      return id == box.id;
    }

    @Override
    public int hashCode() {
      return Objects.hash(id);
    }
  }
}
