package com.kowsercse.uva.V125;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class P12532 {
  public static void main(String[] args) throws IOException {
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    while (reader.ready()) {
      StringTokenizer tokenizer = new StringTokenizer(reader.readLine());
      int totalEntries = Integer.parseInt(tokenizer.nextToken());
      int totalQueries = Integer.parseInt(tokenizer.nextToken());

      SegmentTree segmentTree = new SegmentTree(totalEntries);
      segmentTree.constructTree(new StringTokenizer(reader.readLine()));

      System.out.println(segmentTree.processQueries(totalQueries, reader));
    }
  }
}

class SegmentTree {
  private int totalEntries;
  private int[][] tree;

  public SegmentTree(int totalEntries) {
    this.totalEntries = totalEntries;
    this.tree = new int[calculateHeight(totalEntries) + 1][];
  }

  private int getAnswer(int height, int index, int start, int end) {
    int totalChildNodes = 1 << height;
    int thisStart = totalChildNodes * index;
    int thisEnd = totalChildNodes * index + totalChildNodes - 1;

    if (height == 0) {
      return end < thisStart || start > thisEnd ? 1 : tree[height][index];
    } else if (index >= tree[height].length || end < thisStart || start > thisEnd) {
      return 1;
    } else if (start <= thisStart && thisEnd <= end) {
      return tree[height][index];
    }

    int leftAnswer = getAnswer(height - 1, 2 * index, start, end);
    int rightAnswer = getAnswer(height - 1, 2 * index + 1, start, end);
    return leftAnswer * rightAnswer;
  }

  private void update(int i, int value) {
    int index = i;
    if (value == 0) {
      tree[0][index] = 0;
    } else {
      tree[0][index] = value > 0 ? 1 : -1;
    }

    for (int depth = 1; depth < tree.length; depth++) {
      index /= 2;
      tree[depth][index] =
          2 * index + 1 < tree[depth - 1].length
              ? tree[depth - 1][index * 2] * tree[depth - 1][2 * index + 1]
              : tree[depth - 1][2 * index];
    }
  }

  public void constructTree(StringTokenizer tokenizer) {
    int[] previousEntries = readEntries(tokenizer);
    tree[0] = previousEntries;

    for (int depth = 1; depth < tree.length; depth++) {
      int size = (previousEntries.length + 1) / 2;
      int[] currentEntries = new int[size];

      for (int i = 0; i < size; i++) {
        currentEntries[i] =
            2 * i + 1 < previousEntries.length
                ? previousEntries[2 * i] * previousEntries[2 * i + 1]
                : previousEntries[2 * i];
      }

      tree[depth] = currentEntries;
      previousEntries = currentEntries;
    }
  }

  public String processQueries(int totalQueries, BufferedReader reader) throws IOException {
    StringBuilder stringBuilder = new StringBuilder();
    for (int i = 0; i < totalQueries; i++) {
      StringTokenizer tokenizer = new StringTokenizer(reader.readLine());
      String command = tokenizer.nextToken();
      if ("C".equals(command)) {
        update(
            Integer.parseInt(tokenizer.nextToken()) - 1, Integer.parseInt(tokenizer.nextToken()));
      } else if ("P".equals(command)) {
        int start = Integer.parseInt(tokenizer.nextToken()) - 1;
        int end = Integer.parseInt(tokenizer.nextToken()) - 1;
        stringBuilder.append(getChar(getAnswer(tree.length - 1, 0, start, end)));
      }
    }

    return stringBuilder.toString();
  }

  private char getChar(int value) {
    if (value == 0) {
      return '0';
    }

    return value < 0 ? '-' : '+';
  }

  private int[] readEntries(StringTokenizer tokenizer) {
    int[] entries = new int[totalEntries];
    for (int i = 0; i < totalEntries; i++) {
      int value = Integer.parseInt(tokenizer.nextToken());
      if (value != 0) {
        entries[i] = value > 0 ? 1 : -1;
      }
    }

    return entries;
  }

  private int calculateHeight(int totalEntries) {
    int depth = 0;
    while (totalEntries > 0) {
      totalEntries >>= 1;
      depth++;
    }
    return depth;
  }
}
