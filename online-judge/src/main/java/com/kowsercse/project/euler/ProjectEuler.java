package com.kowsercse.project.euler;

import java.math.BigInteger;
import java.util.*;

public class ProjectEuler {

  public static void main(String... args) {}

  private static void euler28() {
    long total = 1;
    for (int i = 1; i <= 500; i++) {
      final int number = 2 * i + 1;
      final int square = number * number;
      total +=
          square + (square - number + 1) + (square - 2 * number + 2) + (square - 3 * number + 3);
    }

    System.out.println(total);
  }

  private static void euler25AlterNate() {
    final double ceil =
        Math.ceil((1000 - 1 + Math.log10(Math.sqrt(5))) / Math.log10((1 + Math.sqrt(5)) / 2));
    System.out.println(ceil);
  }

  private static void euler25() {
    final ArrayList<BigInteger> fibs = new ArrayList<>();
    fibs.add(new BigInteger("0"));
    fibs.add(new BigInteger("1"));
    int i = 1;
    while (fibs.get(i).toString().length() != 1000) {
      i++;
      fibs.add(fibs.get(i - 1).add(fibs.get(i - 2)));
    }
    System.out.println(i + ": " + fibs.get(i));
  }

  private static List<Integer> primes() {
    final int range = 1000000;
    final BitSet bitSet = new BitSet(range);
    final List<Integer> primes = new ArrayList<>();
    primes.add(2);

    for (int i = 3; i <= range; i += 2) {
      if (!bitSet.get(i / 2)) {
        primes.add(i);
        for (int j = 3 * i; j < range; j += i) {
          bitSet.set(j / 2);
        }
      }
    }

    int sum = 0;
    int totalTerm = 0;
    for (Integer prime : primes) {
      totalTerm++;
      sum += prime;
      if (sum > 1000000) {
        break;
      }

      System.out.println(prime + ": " + sum + " --> " + totalTerm);
      if (sum % 2 == 1 && !bitSet.get(sum / 2)) {
        System.out.println("---> " + sum);
      }
    }

    return primes;
  }

  private static void initPrime() {
    final BitSet bitSet = new BitSet(Integer.MAX_VALUE / 2);

    for (int i = 3; i <= 1000000; i += 2) {
      if (!bitSet.get(i / 2)) {
        for (int j = 3 * i; j < Integer.MAX_VALUE - 2 * i; j += 2 * i) {
          bitSet.set(j / 2);
        }
      }
    }
  }

  private static void euler22() {
    final Scanner scanner = new Scanner(ProjectEuler.class.getResourceAsStream("/euler22.txt"));
    final String content = scanner.nextLine();
    final List<String> names = Arrays.asList(content.split(","));
    for (int i = 0; i < names.size(); i++) {
      names.set(i, names.get(i).replace("\"", ""));
    }
    Collections.sort(names);

    long score = 0;
    for (int i = 0; i < names.size(); i++) {
      final String name = names.get(i);
      int stringWeight = 0;
      for (int j = 0; j < name.length(); j++) {
        stringWeight += name.charAt(j) - 'A' + 1;
      }

      score += stringWeight * (i + 1);
    }
    System.out.println(score);
  }

  private static void euler21() {
    final Map<Integer, Integer> amicablePairs = new HashMap<>();
    final int totalNumber = 10000;
    for (int i = 2; i < totalNumber; i++) {
      int sum = calculateSumOfProperFactors(i);
      amicablePairs.put(i, sum);
    }

    int sum = 0;
    for (int i = 2; i < totalNumber; i++) {
      final Integer sumOfProperFactors = amicablePairs.get(i);
      final Integer integer = amicablePairs.get(sumOfProperFactors);
      if (sumOfProperFactors != i && integer != null && integer == i) {
        System.out.println(i + ": " + sumOfProperFactors);
        sum += i;
      }
    }

    System.out.println(sum);
  }

  private static int calculateSumOfProperFactors(final int number) {
    int sum = 1;
    final int root = (int) Math.sqrt(number);
    for (int i = 2; i <= root; i++) {
      if (number % i == 0) {
        sum += i;
        sum += number / i;
      }
    }

    if (root * root == number) {
      sum -= root;
    }

    return sum;
  }

  private static void euler20() {
    BigInteger result = new BigInteger("1");
    for (long i = 1; i <= 100; i++) {
      result = result.multiply(new BigInteger(String.valueOf(i)));
      System.out.println(i + ": " + result);
    }

    int total = stringWeight(result.toString());
    System.out.println(total);
  }

  private static int stringWeight(final String string) {
    int total = 0;
    for (int i = 0; i < string.length(); i++) {
      total += string.charAt(i) - '0';
    }
    return total;
  }

  private static void euler14() {
    Map<Long, Long> chainLength = new HashMap<>();
    chainLength.put(1L, 1L);

    long maxLength = 0;
    long maxNumber = 0;

    for (long i = 2; i < 10; i++) {
      long length = calculateChain(i, chainLength);
      chainLength.put(i, length);

      System.out.println(i + ": " + length);

      if (length > maxLength) {
        maxLength = length;
        maxNumber = i;
      }
    }

    System.out.println(maxNumber + ": " + maxLength);
  }

  private static long calculateChain(final long number, final Map<Long, Long> chainLength) {
    long n = number;
    long length = 0;
    while (n != 1) {
      if (n % 2 == 0) {
        n = n / 2;
      } else {
        n = 3 * n + 1;
      }
      length++;

      if (chainLength.containsKey(n)) {
        length += chainLength.get(n);
        break;
      }
    }

    chainLength.put(number, length);
    return length;
  }

  private static void euler16() {
    final BigInteger two = new BigInteger("2");
    final BigInteger pow = two.pow(1000);
    final String result = pow.toString();

    int sum = stringWeight(result);
    System.out.println(sum);
  }

  private static void euler12() {
    for (int i = 7; ; i++) {
      final int triangleNumber = (i * (i + 1)) / 2;
      int totalFactor = countFactors(triangleNumber);
      if (i % 100 == 0) {
        System.out.println(i + ": " + totalFactor);
      }
      if (totalFactor >= 500) {
        System.out.println(i + ": " + totalFactor);
        break;
      }
    }
  }

  private static int countFactors(final int number) {
    int totalFactor = 2;
    for (int i = 2; i <= Math.sqrt(number); i++) {
      if (number % i == 0) {
        totalFactor += 2;
      }
    }

    int root = (int) Math.sqrt(number);
    if (root * root == number) {
      totalFactor--;
    }

    return totalFactor;
  }

  private static void euler13() {
    final Scanner scanner = new Scanner(ProjectEuler.class.getResourceAsStream("/euler13.txt"));
    BigInteger sumInteger = new BigInteger("0");
    while (scanner.hasNextLine()) {
      final String line = scanner.nextLine();
      final BigInteger currentInteger = new BigInteger(line);
      sumInteger = sumInteger.add(currentInteger);
    }

    System.out.println(sumInteger);
  }

  private static void eulerX() {
    final int[][] matrix = read();

    for (int i = 0; i < matrix.length; i++) {
      for (int j = 0; j < matrix[0].length; j++) {}
    }
  }

  private static int[][] read() {
    final ArrayList<int[]> lists = new ArrayList<>();
    final Scanner scanner = new Scanner(ProjectEuler.class.getResourceAsStream("/euler11.txt"));
    while (scanner.hasNextLine()) {
      final String line = scanner.nextLine();
      final String[] strings = line.split(" ");
      final int[] integers = new int[strings.length];
      for (int j = 0; j < strings.length; j++) {
        integers[j] = Integer.parseInt(strings[j]);
      }
      lists.add(integers);
    }

    final int[][] matrix = new int[lists.size()][];
    for (int i = 0; i < lists.size(); i++) {
      matrix[i] = lists.get(i);
    }

    return matrix;
  }

  private static void projectEuler10() {
    final int max = 2000000;
    final BitSet bitSet = new BitSet(max / 2);

    long primeSum = 2;
    for (int i = 3; i < max; i += 2) {
      if (!bitSet.get(i / 2)) {
        primeSum += i;
        for (int j = 3 * i; j < Integer.MAX_VALUE - 2 * i; j += 2 * i) {
          bitSet.set(j / 2);
        }
      }
    }

    System.out.println(primeSum);
  }

  private static void projectEuler9() {
    final int limit = 1000;
    boolean found = false;
    for (int c = limit - 2; c > 0; c--) {
      final int cSquare = c * c;
      for (int a = 1; a + c < limit; a++) {
        int b = limit - (a + c);

        final double bSqrt = Math.sqrt(cSquare - a * a);
        if (b - bSqrt == 0.0000000) {
          System.out.println(a + " " + b + " " + c);
          System.out.println(a * b * c);
          found = true;
          break;
        } else if (bSqrt < a) {
          break;
        }
      }

      if (found) {
        break;
      }
    }
  }

  private static void projectEuler8() {
    System.out.println(number);
    long product = 1;
    final int length = 13;
    int totalZero = 0;
    for (int i = 0; i < length; i++) {
      if (number.charAt(i) == '0') {
        totalZero++;
      }
      product += (number.charAt(i) - '0');
    }
    long maxProduct = product;
    int maxEndIndex = length - 1;

    long currentProduct = product;
    for (int i = length; i < number.length(); i++) {
      if (number.charAt(i - length) == '0') {
        totalZero--;
      }
      if (number.charAt(i) == '0') {
        totalZero++;
      }

      if (totalZero > 0) {
        currentProduct = 0;
      } else if (currentProduct == 0) {
        currentProduct = 1;
        for (int j = i - length + 1; j <= i; j++) {
          currentProduct *= (number.charAt(j) - '0');
        }
      } else {
        currentProduct =
            (currentProduct * (number.charAt(i) - '0')) / (number.charAt(i - length) - '0');
      }

      if (currentProduct > maxProduct) {
        maxProduct = currentProduct;
        maxEndIndex = i;
      }
    }

    System.out.println(maxProduct);
    System.out.println(number.substring(maxEndIndex - length, maxEndIndex + 1));
  }

  private static void projectEuler7() {
    int totalPrime = 1;
    final BitSet bitSet = new BitSet(Integer.MAX_VALUE / 2);

    for (int i = 3; totalPrime < 10001; i += 2) {
      if (!bitSet.get(i / 2)) {
        totalPrime++;
        if (totalPrime == 10001) {
          System.out.println(i);
        }
        //        System.out.printf("%4d: %d\n", totalPrime, i);

        for (int j = 3 * i; j < Integer.MAX_VALUE - 2 * i; j += 2 * i) {
          bitSet.set(j / 2);
        }
      }
    }
  }

  private static String number =
      "73167176531330624919225119674426574742355349194934"
          + "96983520312774506326239578318016984801869478851843"
          + "85861560789112949495459501737958331952853208805511"
          + "12540698747158523863050715693290963295227443043557"
          + "66896648950445244523161731856403098711121722383113"
          + "62229893423380308135336276614282806444486645238749"
          + "30358907296290491560440772390713810515859307960866"
          + "70172427121883998797908792274921901699720888093776"
          + "65727333001053367881220235421809751254540594752243"
          + "52584907711670556013604839586446706324415722155397"
          + "53697817977846174064955149290862569321978468622482"
          + "83972241375657056057490261407972968652414535100474"
          + "82166370484403199890008895243450658541227588666881"
          + "16427171479924442928230863465674813919123162824586"
          + "17866458359124566529476545682848912883142607690042"
          + "24219022671055626321111109370544217506941658960408"
          + "07198403850962455444362981230987879927244284909188"
          + "84580156166097919133875499200524063689912560717606"
          + "05886116467109405077541002256983155200055935729725"
          + "71636269561882670428252483600823257530420752963450";
}
