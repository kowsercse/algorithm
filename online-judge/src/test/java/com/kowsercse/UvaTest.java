package com.kowsercse;

import static org.junit.jupiter.api.DynamicContainer.dynamicContainer;

import com.kowsercse.test.InputOutputTestBuilder;
import com.kowsercse.util.SkeletonBuilder;
import com.kowsercse.uva.V1.*;
import com.kowsercse.uva.V10.*;
import com.kowsercse.uva.V100.*;
import com.kowsercse.uva.V101.*;
import com.kowsercse.uva.V102.*;
import com.kowsercse.uva.V104.P10487;
import com.kowsercse.uva.V105.P10507;
import com.kowsercse.uva.V106.P10684;
import com.kowsercse.uva.V108.*;
import com.kowsercse.uva.V109.*;
import com.kowsercse.uva.V110.*;
import com.kowsercse.uva.V112.P11235;
import com.kowsercse.uva.V112.P11286;
import com.kowsercse.uva.V113.*;
import com.kowsercse.uva.V114.*;
import com.kowsercse.uva.V114.P11402;
import com.kowsercse.uva.V115.*;
import com.kowsercse.uva.V118.*;
import com.kowsercse.uva.V119.*;
import com.kowsercse.uva.V12.*;
import com.kowsercse.uva.V123.P12356;
import com.kowsercse.uva.V124.P12442;
import com.kowsercse.uva.V125.P12532;
import com.kowsercse.uva.V3.P357;
import com.kowsercse.uva.V4.*;
import com.kowsercse.uva.V5.*;
import com.kowsercse.uva.V6.*;
import com.kowsercse.uva.V7.P732;
import com.kowsercse.uva.V7.P787;
import com.kowsercse.uva.V7.P793;
import com.kowsercse.uva.V9.*;
import java.util.List;
import java.util.stream.Stream;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DynamicContainer;
import org.junit.jupiter.api.DynamicNode;
import org.junit.jupiter.api.TestFactory;

public class UvaTest {

  public static final List<Class<?>> accepted =
      List.of(
          P103.class,
          P105.class,
          P108.class,
          P116.class,
          P357.class,
          P401.class,
          P481.class,
          P514.class,
          P591.class,
          P615.class,
          P624.class,
          P673.class,
          P674.class,
          P787.class,
          P793.class,
          P732.class,
          P948.class,
          P993.class,
          P1062.class,
          P1203.class,
          P1237.class,
          P1260.class,
          P10004.class,
          P10018.class,
          P10258.class,
          P10305.class,
          P10487.class,
          P10507.class,
          P10684.class,
          P10819.class,
          P10819.class,
          P10855.class,
          P10895.class,
          P10901.class,
          P10954.class,
          P11057.class,
          P11034.class,
          P11085.class,
          P11094.class,
          P11136.class,
          P11235.class,
          P11286.class,
          P11340.class,
          P11456.class,
          P11503.class,
          P11517.class,
          P11572.class,
          P11805.class,
          P11831.class,
          P11849.class,
          P11875.class,
          P11953.class,
          P11988.class,
          P11995.class,
          P12532.class);

  public static final List<Class<?>> wrongAnswer = List.of(P193.class);
  public static final List<Class<?>> pending =
      List.of(P10147.class, P11503.class, P11906.class, P111.class);
  public static final List<Class<?>> timeLimitExceeded = List.of();
  public static final List<Class<?>> tleInJavaAcceptedInCpp =
      List.of(P978.class, P10226.class, P11402.class, P11951.class, P12356.class, P12442.class);
  public static final List<Class<?>> rteInJavaAcceptedInCpp = List.of(P11991.class);
  private static final List<Integer> cppSolutionsOnly = List.of(599, 10189);
  public static final List<Class<?>> inProgress = List.of();

  @TestFactory
  Stream<DynamicNode> inProgress() {
    final SkeletonBuilder skeletonBuilder = new SkeletonBuilder();
    inProgress.stream()
        .filter(clazz -> !skeletonBuilder.isCreated(clazz))
        .forEach(skeletonBuilder::build);
    return inProgress.stream().map(UvaTest::createDynamicContainer);
  }

  @TestFactory
  Stream<DynamicNode> accepted() {
    return accepted.stream().map(UvaTest::createDynamicContainer);
  }

  @Disabled
  @TestFactory
  Stream<DynamicNode> wrongAnswers() {
    return wrongAnswer.stream().map(UvaTest::createDynamicContainer);
  }

  @Disabled
  @TestFactory
  Stream<DynamicNode> pending() {
    return pending.stream().map(UvaTest::createDynamicContainer);
  }

  private static DynamicContainer createDynamicContainer(Class<?> clazz) {
    return dynamicContainer(clazz.getSimpleName(), InputOutputTestBuilder.of(clazz, "-"));
  }
}
