package com.kowsercse.kickstarter;

import com.kowsercse.test.InputOutputTestBuilder;
import java.util.stream.Stream;
import org.junit.jupiter.api.DynamicNode;
import org.junit.jupiter.api.TestFactory;

public class HIndexCalculatorTest {

  @TestFactory
  public Stream<DynamicNode> xyz() {
    return InputOutputTestBuilder.of(HIndexCalculator.class, "-");
  }
}
