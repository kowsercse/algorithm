package com.kowsercse.codeforces.round.r321;

import java.util.Scanner;
import org.junit.Ignore;
import org.junit.Test;

public class ProbTestCase {

  @Test
  @Ignore
  public void testProblemA() {
    new ProbA(new Scanner(getClass().getResourceAsStream("/com/kowsercse/R321/A.txt")), System.out)
        .solve();
  }

  @Test
  public void testProblemB() {
    new ProbB(new Scanner(getClass().getResourceAsStream("/com/kowsercse/R321/B.txt")), System.out)
        .solve();
  }
}
