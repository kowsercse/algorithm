package com.kowsercse.leetcode;

import com.kowsercse.leetcode.P15.Solution;
import java.util.List;
import org.junit.jupiter.api.Test;

class P15Test {

  private final Solution solution = new Solution();

  @Test
  void test() {
    List<List<Integer>> lists = solution.threeSum(new int[]{-1, 0, 1, 2, -1, -4});
    lists.forEach(System.out::println);
  }
}
