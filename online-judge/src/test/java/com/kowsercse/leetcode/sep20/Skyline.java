package com.kowsercse.leetcode.sep20;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

class Skyline {
  public List<List<Integer>> getSkyline(int[][] buildings) {
    if (buildings == null || buildings.length == 0) {
      return Collections.emptyList();
    }

    return solve(buildings, 0, buildings.length - 1);
  }

  LinkedList<List<Integer>> solve(int[][] towers, int start, int end) {
    if (start > end) {
      return new LinkedList<>();
    } else if (start == end) {
      LinkedList<List<Integer>> result = new LinkedList<>();
      result.add(Arrays.asList(towers[start][0], towers[start][2]));
      result.add(Arrays.asList(towers[start][1], 0));
      return result;
    }

    return merge(
        solve(towers, start, (start + end) / 2), solve(towers, (start + end) / 2 + 1, end));
  }

  LinkedList<List<Integer>> merge(LinkedList<List<Integer>> left, LinkedList<List<Integer>> right) {
    int hLeft = 0, hRight = 0, hCurr = 0;
    LinkedList<List<Integer>> merged = new LinkedList<>();
    while (!left.isEmpty() && !right.isEmpty()) {
      int xCurr;
      if (left.getFirst().get(0) < right.getFirst().get(0)) {
        List<Integer> l = left.pollFirst();
        hLeft = l.get(1);
        xCurr = l.get(0);
      } else {
        List<Integer> r = right.pollFirst();
        hRight = r.get(1);
        xCurr = r.get(0);
      }

      int hMax = Math.max(hLeft, hRight);
      if (hCurr != hMax) {
        addOrUpdate(merged, xCurr, hMax);
        hCurr = hMax;
      }
    }

    addRemaining(merged, left, hCurr);
    addRemaining(merged, right, hCurr);

    return merged;
  }

  private void addRemaining(
      LinkedList<List<Integer>> merged, LinkedList<List<Integer>> remaining, int hCurr) {
    while (!remaining.isEmpty()) {
      List<Integer> point = remaining.pollFirst();
      int x = point.get(0), h = point.get(1);
      if (hCurr != h) {
        addOrUpdate(merged, x, h);
        hCurr = h;
      }
    }
  }

  private void addOrUpdate(LinkedList<List<Integer>> merged, int xCurr, int hMax) {
    if (merged.isEmpty() || merged.getLast().get(0) != xCurr) {
      merged.add(Arrays.asList(xCurr, hMax));
    } else {
      merged.getLast().set(1, hMax);
    }
  }
}
