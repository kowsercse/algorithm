package com.kowsercse.leetcode.sep20;

import java.util.List;
import org.junit.jupiter.api.Test;

public class SkylineTest {
  @Test
  void name() {
    Skyline skyline = new Skyline();
    List<List<Integer>> result =
        skyline.getSkyline(
            new int[][] {{2, 9, 10}, {3, 7, 15}, {5, 12, 12}, {15, 20, 10}, {19, 24, 8}});
    System.out.println(result);
  }

  @Test
  void name2() {
    Skyline skyline = new Skyline();
    List<List<Integer>> result = skyline.getSkyline(new int[][] {{1, 2, 1}, {1, 2, 2}, {1, 2, 3}});
    System.out.println(result);
  }

  @Test
  void leetcode() {
    Solution solution = new Solution();
    List<List<Integer>> result = solution.getSkyline(new int[][] {{1, 2, 1}, {1, 2, 2}, {1, 2, 3}});
    System.out.println(result);
  }
}
