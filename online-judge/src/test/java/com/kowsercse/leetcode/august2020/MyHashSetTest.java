package com.kowsercse.leetcode.august2020;

import static org.junit.Assert.assertEquals;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import org.junit.Test;

public class MyHashSetTest {
  private String[] methodCalls = {
    "add",
    "contains",
    "add",
    "contains",
    "remove",
    "add",
    "contains",
    "add",
    "add",
    "add",
    "add",
    "add",
    "add",
    "contains",
    "add",
    "add",
    "add",
    "contains",
    "remove",
    "contains",
    "contains",
    "add",
    "remove",
    "add",
    "remove",
    "add",
    "remove",
    "add",
    "contains",
    "add",
    "add",
    "contains",
    "add",
    "add",
    "add",
    "add",
    "remove",
    "contains",
    "add",
    "contains",
    "add",
    "add",
    "add",
    "remove",
    "remove",
    "add",
    "contains",
    "add",
    "add",
    "contains",
    "remove",
    "add",
    "contains",
    "add",
    "remove",
    "remove",
    "add",
    "contains",
    "add",
    "contains",
    "contains",
    "add",
    "add",
    "remove",
    "remove",
    "add",
    "remove",
    "add",
    "add",
    "add",
    "add",
    "add",
    "add",
    "remove",
    "remove",
    "add",
    "remove",
    "add",
    "add",
    "add",
    "add",
    "contains",
    "add",
    "remove",
    "remove",
    "remove",
    "remove",
    "add",
    "add",
    "add",
    "add",
    "contains",
    "add",
    "add",
    "add",
    "add",
    "add",
    "add",
    "add",
    "add"
  };
  private String[] arguments =
      "[[58],[0],[14],[58],[91],[6],[58],[66],[51],[16],[40],[52],[48],[40],[42],[85],[36],[16],[0],[43],[6],[3],[25],[99],[66],[60],[58],[97],[3],[35],[65],[40],[41],[10],[37],[65],[37],[40],[28],[60],[30],[63],[76],[90],[3],[43],[81],[61],[39],[75],[10],[55],[92],[71],[2],[20],[7],[55],[88],[39],[97],[44],[1],[51],[89],[37],[19],[3],[13],[11],[68],[18],[17],[41],[87],[48],[43],[68],[80],[35],[2],[17],[71],[90],[83],[42],[88],[16],[37],[33],[66],[59],[6],[79],[77],[14],[69],[36],[21],[40]]"
          .replace("[", "")
          .replace("]", "")
          .split(",");
  //  private String[] actual =
  // "null,false,null,true,null,null,true,null,null,null,null,null,null,true,null,null,null,true,null,false,true,null,null,null,null,null,null,null,true,null,null,true,null,null,null,null,null,true,null,false,null,null,null,null,null,null,false,null,null,false,null,null,false,null,null,null,null,true,null,true,true,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,true,null,null,null,null,null,null,null,null,null,false,null,null,null,null,null,null,null,null".split(",");
  private String[] expected =
      "null,false,null,true,null,null,true,null,null,null,null,null,null,true,null,null,null,true,null,false,true,null,null,null,null,null,null,null,true,null,null,true,null,null,null,null,null,true,null,true,null,null,null,null,null,null,false,null,null,false,null,null,false,null,null,null,null,true,null,true,true,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,true,null,null,null,null,null,null,null,null,null,false,null,null,null,null,null,null,null,null"
          .split(",");

  @Test
  public void test() throws InvocationTargetException, IllegalAccessException {
    MyHashSet set = new MyHashSet();

    Map<String, Method> methodByName = new HashMap<>();
    for (Method method : MyHashSet.class.getMethods()) {
      methodByName.put(method.getName(), method);
    }

    for (int i = 0; i < methodCalls.length; i++) {
      String methodCall = methodCalls[i];
      int value = Integer.parseInt(arguments[i]);
      Object result = methodByName.get(methodCall).invoke(set, value);
      System.out.println(methodCall + " -> " + value);
      set.inorder();
      System.out.println();

      if ("contains".equals(methodCall)) {
        assertEquals(
            "Failed for " + i + " -> " + arguments[i], Boolean.parseBoolean(expected[i]), result);
      }
    }
  }
}
