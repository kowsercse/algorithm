package com.kowsercse.leetcode.august2020;

import org.junit.jupiter.api.Test;

public class SolutionTest {
  private final Solution solution = new Solution();

  @Test
  public void test() {
    System.out.println(solution.containsNearbyAlmostDuplicate(new int[] {0, 4}, 2, 3));
  }
}
