package com.kowsercse.leetcode.august2020;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoggerTest {

  @Test
  public void testSample() {
    Logger logger = new Logger();
    Object[][] input =
        new Object[][] {{1, "foo"}, {2, "bar"}, {3, "foo"}, {8, "bar"}, {10, "foo"}, {11, "foo"}};

    assertEquals(true, logger.shouldPrintMessage(1, "foo"));
    assertEquals(true, logger.shouldPrintMessage(2, "bar"));
    assertEquals(false, logger.shouldPrintMessage(3, "foo"));
    assertEquals(false, logger.shouldPrintMessage(8, "bar"));
    assertEquals(false, logger.shouldPrintMessage(10, "foo"));
    assertEquals(true, logger.shouldPrintMessage(11, "foo"));
  }

  @Test
  public void testDetectCapitalUse() {
    Logger logger = new Logger();
    assertTrue(logger.detectCapitalUse("USA"));
    assertTrue(logger.detectCapitalUse("Usa"));
    assertFalse(logger.detectCapitalUse("UsA"));
    assertTrue(logger.detectCapitalUse("US"));
    assertTrue(logger.detectCapitalUse("U"));
    assertTrue(logger.detectCapitalUse("u"));
  }
}
