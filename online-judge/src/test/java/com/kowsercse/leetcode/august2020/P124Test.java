package com.kowsercse.leetcode.august2020;

import com.kowsercse.leetcode.august2020.P124.TreeNode;
import org.junit.jupiter.api.Test;

public class P124Test {

  @Test
  public void test() {

    P124 p124 = new P124();
    int maxPathSum =
        p124.maxPathSum(
            new TreeNode(
                1,
                new TreeNode(-2, new TreeNode(1), new TreeNode(3)),
                new TreeNode(-3, new TreeNode(-2), null)));
    System.out.println(maxPathSum);
  }
}
