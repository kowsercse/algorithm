package com.kowsercse.leetcode.august2020;

import static org.junit.Assert.*;

import org.junit.Test;

public class P269Test {

  @Test
  public void test() {
    P269.Solution solution = new P269.Solution();
    solution.alienOrder(new String[] {"wrt", "wrf", "er", "ett", "rftt"});
  }

  @Test
  public void test1() {
    P269.Solution solution = new P269.Solution();
    solution.alienOrder(new String[] {"z", "z", "z"});
  }

  @Test
  public void test2() {
    P269.Solution solution = new P269.Solution();
    solution.alienOrder(new String[] {"z", "x", "z"});
  }

  @Test
  public void test3() {
    P269.Solution solution = new P269.Solution();
    solution.alienOrder(new String[] {"zy", "zx"});
  }
}
