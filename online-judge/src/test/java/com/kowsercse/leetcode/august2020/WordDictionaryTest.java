package com.kowsercse.leetcode.august2020;

import static org.junit.Assert.*;

import org.junit.jupiter.api.Test;

public class WordDictionaryTest {

  @Test
  void test() {
    WordDictionary wordDictionary = new WordDictionary();
    wordDictionary.addWord("bad");
    wordDictionary.addWord("dad");
    wordDictionary.addWord("mad");
    wordDictionary.search("pad");
    System.out.println(wordDictionary.search("bad"));
    System.out.println(wordDictionary.search(".ad"));
    System.out.println(wordDictionary.search("b.."));
  }
}
