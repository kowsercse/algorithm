package com.kowsercse.hackerrank;

import com.kowsercse.IOTestCase;
import org.junit.Test;

public class FlippingTheMatrixTest extends IOTestCase {

  private FlipTheMatrix flippingTheMatrix;

  @Test
  public void test() {
    setInput("/com/kowsercse/hackerrank/FlippingTheMatrix.txt");

    flippingTheMatrix = new FlipTheMatrix();
    flippingTheMatrix.solve();
  }
}
