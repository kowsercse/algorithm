#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <sstream>

using namespace std;

template<typename Out>
void split(const std::string &s, char delim, Out result) {
  std::istringstream iss(s);
  std::string item;
  while (std::getline(iss, item, delim)) {
    *result++ = item[0];
  }
}

class Solution {
private:
    vector<char> nodes;
    map<char, bool> visited;
    map<char, vector<char>> graph;

public:
    Solution() {
      char source, destination;
      string line;

      getline(cin, line);
      while (line[0] != '*') {
        source = line[1];
        destination = line[3];

        graph[source].push_back(destination);
        graph[destination].push_back(source);
        getline(cin, line);
      }

      getline(cin, line);
      split(line, ',', back_inserter(nodes));
    }

    int dfs(char node) {
      visited[node] = true;

      int totalNode = 1;
      for (auto destination: graph[node]) {
        if (!visited[destination]) {
          totalNode += dfs(destination);
        }
      }

      return totalNode;
    }

    void solve() {
      int totalAcorn = 0, totalTree = 0;
      for (auto source: nodes) {
        if (!visited[source]) {
          int totalNode = dfs(source);
          if (totalNode == 1) {
            totalAcorn++;
          } else {
            totalTree++;
          }
        }
      }

      cout << "There are " << totalTree << " tree(s) and " << totalAcorn << " acorn(s)." << endl;
    }
};

int main() {
  freopen("../src/test/resources/com/kowsercse/uva/V5/599-2.in", "r", stdin);
  freopen("../src/test/resources/com/kowsercse/uva/V5/599-2.out", "w", stdout);

  string line;
  getline(cin, line);

  int testCase = stoi(line);
  while (testCase-- > 0) {
    Solution solution;
    solution.solve();
  }

  return 0;
}
