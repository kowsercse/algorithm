#include <iostream>
#include <vector>

using namespace std;

const char UNSET = 'U';
const char YES = '1';
const char NO = '0';
const char MIXED = 'M';

class Node {
private:
    int yesCount;
    char flag, lazyValue = UNSET;
    int start, end;

    Node *left = nullptr, *right = nullptr;

    void setLazyValue(char value) {
      if (this->start == this->end) {
        this->flag = value;
        this->yesCount = flag == YES ? 1 : 0;
      } else {
        this->lazyValue = value;
      }
    }

    bool isCompleteOverlap(int start, int end) const { return start <= Node::start && Node::end <= end; }

    bool isNoOverlap(int start, int end) const { return end < Node::start || start > Node::end; }

    int getYesCount() {
      sinkLazyValueIfRequired();
      return yesCount;
    }

public:
    Node(Node *left, Node *right) {
      this->flag = left->flag == right->flag ? left->flag : MIXED;
      this->yesCount = left->yesCount + right->yesCount;

      this->start = left->start;
      this->end = right->end;

      this->left = left;
      this->right = right;
    }

    Node(int index, char flag) {
      this->flag = flag;
      this->yesCount = flag == YES ? 1 : 0;

      this->start = index;
      this->end = index;

      this->left = nullptr;
      this->right = nullptr;
    }

    void sinkLazyValueIfRequired() {
      if (lazyValue == UNSET) {
        return;
      }

      if (start != end) {
        left->setLazyValue(lazyValue);
        right->setLazyValue(lazyValue);
      }

      flag = lazyValue;
      yesCount = flag == YES ? end - start + 1 : 0;
      lazyValue = UNSET;
    }

    void setValue(int start, int end, char value) {
      sinkLazyValueIfRequired();
      if (isNoOverlap(start, end) || (this->lazyValue == UNSET && this->flag == value)) {
        return;
      }

      if (isCompleteOverlap(start, end)) {
        if (this->start != this->end) {
          left->setLazyValue(value);
          right->setLazyValue(value);
        }

        this->flag = value;
        this->yesCount = this->flag == YES ? this->end - this->start + 1 : 0;
      } else {
        left->setValue(start, end, value);
        right->setValue(start, end, value);

        this->flag = left->flag == right->flag ? left->flag : MIXED;
        this->yesCount = left->getYesCount() + right->getYesCount();
      }
    }

    void inverseValue(int start, int end) {
      sinkLazyValueIfRequired();
      if (isNoOverlap(start, end)) {
        return;
      }

      if (isCompleteOverlap(start, end)) {
        if (this->flag == MIXED) {
          if (this->start != this->end) {
            left->inverseValue(start, end);
            right->inverseValue(start, end);
          }

          this->flag = left->flag == right->flag ? left->flag : MIXED;
          this->yesCount = left->getYesCount() + right->getYesCount();
        } else {
          char inverseValue = this->flag == YES ? NO : YES;
          if (this->start != this->end) {
            left->setLazyValue(inverseValue);
            right->setLazyValue(inverseValue);
          }

          this->flag = inverseValue;
          this->yesCount = this->flag == YES ? this->end - this->start + 1 : 0;
        }
      } else {
        left->inverseValue(start, end);
        right->inverseValue(start, end);

        this->flag = left->flag == right->flag ? left->flag : MIXED;
        this->yesCount = left->getYesCount() + right->getYesCount();
      }
    }

    int calculateTotalYes(int start, int end) {
      sinkLazyValueIfRequired();
      if (isNoOverlap(start, end)) {
        return 0;
      }

      if (isCompleteOverlap(start, end)) {
        return this->yesCount;
      } else {
        return left->calculateTotalYes(start, end) + right->calculateTotalYes(start, end);
      }
    }
};

class SegmentTree {
private:
    Node *root = nullptr;
    vector<Node> input;
public:
    void constructTree() {
      vector<Node> *previousEntries = &input;
      while (previousEntries->size() != 1) {
        unsigned long summaryLength = (previousEntries->size() + 1) / 2;

        auto summaryEntries = new vector<Node>();
        summaryEntries->reserve(summaryLength);

        for (int i = 0; i < summaryLength; i++) {
          if (2 * i + 1 < previousEntries->size()) {
            Node &left = previousEntries->at(2 * i);
            Node &right = previousEntries->at(2 * i + 1);
            Node node(&left, &right);
            summaryEntries->push_back(node);
          } else {
            summaryEntries->push_back(previousEntries->at(2 * i));
          }
        }

        previousEntries = summaryEntries;
      }

      this->root = &previousEntries->at(0);
    }

    void readInput() {
      input.reserve(1024000);

      string line;
      int totalInput, inputFrequency;
      int index = 0;

      cin >> totalInput;
      while (totalInput-- > 0) {
        cin >> inputFrequency;
        getline(cin, line);
        getline(cin, line);
        char lastCharacter = line[line.length() - 1];
        if (lastCharacter != YES && lastCharacter != NO) {
          line = line.substr(0, line.length() - 1);
        }

        while (inputFrequency-- > 0) {
          for (char c : line) {
            Node node(index++, c);
            input.push_back(node);
          }
        }
      }
    }

    void answerQueries() {
      char command;
      int start, end;

      int totalQuery, printQuery = 1;
      cin >> totalQuery;
      while (totalQuery-- > 0) {
        cin >> command >> start >> end;

        if (command == 'F') {
          root->setValue(start, end, YES);
        } else if (command == 'E') {
          root->setValue(start, end, NO);
        } else if (command == 'I') {
          root->inverseValue(start, end);
        } else if (command == 'S') {
          cout << "Q" << printQuery++ << ": " << root->calculateTotalYes(start, end) << endl;
        }
      }
    }
};

int main() {
  freopen("../src/test/resources/com/kowsercse/uva/V114/11402.in", "r", stdin);
  freopen("../src/test/resources/com/kowsercse/uva/V114/11402.out", "w", stdout);
  int testCase;
  cin >> testCase;
  for (int i = 1; i <= testCase; ++i) {
    SegmentTree segmentTree;
    segmentTree.readInput();
    segmentTree.constructTree();

    cout << "Case " << i << ":" << endl;
    segmentTree.answerQueries();
  }

  return 0;
}
