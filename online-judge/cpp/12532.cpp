// wrong answer, accepted with java

#include <iostream>
#include <vector>

using namespace std;


int getNextMsb(int totalEntries) {
  int msb = 0, entry = 2 * totalEntries - 1;
  while (entry != 0) {
    entry = entry / 2;
    msb++;
  }
  return msb - 1;
}

class SegmentTree {
private:
    vector<int> result;
    int dataStart;
    int dataEnd;
    int treeHeight;

    int constructTree(int index) {
      if (index >= dataEnd) {
        return 1;
      } else if (index >= dataStart) {
        return result[index];
      }

      int leftResult = constructTree(2 * index);
      int rightResult = constructTree(2 * index + 1);

      return result[index] = leftResult * rightResult;
    }

    void update(int index, int value) {
      int actualIndex = index;
      if (value == 0) {
        result[actualIndex] = 0;
      } else {
        result[actualIndex] = value > 0 ? 1 : -1;
      }

      actualIndex /= 2;
      while (actualIndex != 0) {
        if (2 * actualIndex + 1 >= dataEnd) {
          result[actualIndex] = result[2 * actualIndex];
        } else {
          result[actualIndex] = result[2 * actualIndex] * result[2 * actualIndex + 1];
        }
        actualIndex /= 2;
      }
    }

public:
    SegmentTree(int treeHeight, int dataStart, int totalEntries) : result(dataStart + totalEntries, 1) {
      this->dataStart = dataStart;
      this->dataEnd = result.size();
      this->treeHeight = treeHeight;
    }

    void constructTree() {
      readEntries();
      constructTree(1);
    }

    void readEntries() {
      int value;
      for (int i = dataStart; i < dataEnd; ++i) {
        std::cin >> value;
        if (value <= 0) {
          result[i] = value == 0 ? 0 : -1;
        }
      }
    }

    int getMultiplication(int height, int index, int start, int end) {
      int totalChildNodes = 1 << height;
      int thisStart = totalChildNodes * index;
      int thisEnd = totalChildNodes * index + totalChildNodes - 1;

      if (index >= dataEnd || end < thisStart || start > thisEnd) {
        return 1;
      } else if (start <= thisStart && thisEnd <= end) {
        return result[index];
      }

      int leftMultiplication = getMultiplication(height - 1, 2 * index, start, end);
      int rightMultiplication = getMultiplication(height - 1, 2 * index + 1, start, end);

      return leftMultiplication * rightMultiplication;
    }

    void processQueries(int totalQuery) {
      string printMessage;
      char command;
      int firstArgument, secondArgument;
      for (int i = 0; i < totalQuery; ++i) {
        std::cin >> command >> firstArgument >> secondArgument;
        if ('C' == command) {
          update(dataStart + firstArgument - 1, secondArgument);
        } else if ('P' == command) {
          int multiplication =
              getMultiplication(treeHeight, 1, dataStart + firstArgument - 1, dataStart + secondArgument - 1);
          if (multiplication == 0) {
            printMessage.push_back('0');
          } else {
            printMessage.push_back(multiplication > 0 ? '+' : '-');
          }
        }
      }

      cout << printMessage << endl;
    }
};

int main() {
  freopen("../src/test/resources/com/kowsercse/uva/V125/12532-4.in", "r", stdin);
  freopen("../src/test/resources/com/kowsercse/uva/V125/12532-4.out", "w", stdout);

  int totalEntries, totalQuery;
  while (cin >> totalEntries >> totalQuery) {
    int msb = getNextMsb(totalEntries);
    int dataStart = 1 << msb;
    SegmentTree segmentTree(msb, dataStart, totalEntries);
    segmentTree.constructTree();
    segmentTree.processQueries(totalQuery);
  }

  return 0;
}
