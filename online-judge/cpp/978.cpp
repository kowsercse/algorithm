#include <bits/stdc++.h>
#include <iostream>
#include <queue>

using namespace std;

int minimum(int a, int b, int c) {
  return min(a, min(b, c));
}

int main() {
  ifstream cin("../src/test/resources/com/kowsercse/uva/V9/978-2.in");
  ofstream cout("../src/test/resources/com/kowsercse/uva/V9/978-2.out"); 

  int testCase; cin >> testCase;

  while(testCase-- > 0) {
    int totalBattleFields, greenTotal, blueTotal;
    cin >> totalBattleFields >> greenTotal >> blueTotal;
    priority_queue <int> greenSoldiers, blueSoldiers;

    int greenSoldier, blueSoldier;
    for(int i=0; i < greenTotal; i++) {
      cin >> greenSoldier;
      greenSoldiers.push(greenSoldier);
    }
    for(int i=0; i < blueTotal; i++) {
      cin >> blueSoldier;
      blueSoldiers.push(blueSoldier);
    }


    while(!greenSoldiers.empty() && !blueSoldiers.empty()) {
      int actualBattleFields = minimum(totalBattleFields, greenSoldiers.size(), blueSoldiers.size());
      int greenSelected[actualBattleFields], blueSelected[actualBattleFields];

      for(int i = 0; i < actualBattleFields; i++) {
        greenSelected[i] = greenSoldiers.top();
        blueSelected[i] = blueSoldiers.top();

        greenSoldiers.pop();
        blueSoldiers.pop();
      }

      for (int i = 0; i < actualBattleFields; i++) {
        int green = greenSelected[i];
        int blue = blueSelected[i];

        if (green > blue) {
          greenSoldiers.push(green - blue);
        }
        else if (green < blue) {
          blueSoldiers.push(blue - green);
        }
      }
    }

    if (blueSoldiers.empty() && greenSoldiers.empty()) {
      cout << "green and blue died" << endl;
    }
    else if (!blueSoldiers.empty()) {
      cout << "blue wins" << endl;
      while(!blueSoldiers.empty()) {
        cout << blueSoldiers.top() << endl;
        blueSoldiers.pop();
      }
    }
    else if (!greenSoldiers.empty()) {
      cout << "green wins" << endl;
      while(!greenSoldiers.empty()) {
        cout << greenSoldiers.top() << endl;
        greenSoldiers.pop();
      }
    }
    
    if(testCase != 0) {
      cout << endl;
    }
  }

  return 0;
}

