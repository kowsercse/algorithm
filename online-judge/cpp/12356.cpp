#include <bits/stdc++.h>
#include <iostream>

using namespace std;

int main() {
  ifstream cin("../src/test/resources/com/kowsercse/uva/V123/12356-1.in");
  ofstream cout("../src/test/resources/com/kowsercse/uva/V123/12356-1.out");

  int totalSoldiers, totalReport;
  cin >> totalSoldiers >> totalReport;
  while (totalSoldiers != 0 || totalReport != 0) {
    int leftBuddy[totalSoldiers + 1], rightBuddy[totalSoldiers + 1];
    for (int i = 1; i <= totalSoldiers; i++) {
      leftBuddy[i] = i - 1;
      rightBuddy[i] = i + 1;
    }
    rightBuddy[totalSoldiers] = 0;

    int left, right;
    for (int i = 0; i < totalReport; i++) {
      cin >> left >> right;
      rightBuddy[leftBuddy[left]] = rightBuddy[right];
      leftBuddy[rightBuddy[right]] = leftBuddy[left];

      if (leftBuddy[left] == 0) {
        cout << "*";
      } else {
        cout << leftBuddy[left];
      }
      cout << " ";
      if (rightBuddy[right] == 0) {
        cout << "*";
      } else {
        cout << rightBuddy[right];
      }
      cout << endl;
    }

    cout << "-" << endl;
    cin >> totalSoldiers >> totalReport;
  }

  return 0;
}
