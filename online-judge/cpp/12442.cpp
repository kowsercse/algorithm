#include <bits/stdc++.h>
#include <iostream>
#include <queue>

using namespace std;

class Graph {
  public:
    int maxNode = -1;
    map<int, int> destinations;
    map<int, bool> visited;
    map<int, bool> isCycle;
    map<int, int> totalReachableNode;
    int totalNode;
    
    Graph(int totalNode) {
      this->totalNode = totalNode;
    }

    void solve() {
      for(map<int, int>::iterator i = destinations.begin(); i != destinations.end(); i++) {
        calculateReachableNodeCount(i->first);
      }
    }

    void calculateReachableNodeCount(int startNode) {
      if (totalReachableNode[startNode] != 0) {
        return;
      }

      int totalReachable[totalNode];
      int maximumReachable = 0;
      int node = startNode;
      while (!visited[node]) {
        totalReachable[node] = ++maximumReachable;
        visited[node] = true;

        node = destinations[node];
      }

      if (totalReachableNode[node] == 0) {
        int cycleLength =
            isCycle[node]
                ? totalReachableNode[node]
                : maximumReachable - totalReachable[node] + 1;

        fillCycleLength(node, cycleLength);
        fillPrefixLength(startNode, node, maximumReachable);
      }
      else {
        fillPrefixLength(startNode, node, maximumReachable + totalReachableNode[node]);
      }
    }

    void fillCycleLength(int cycleStart, int cycleLength) {
      if (cycleStart == destinations[cycleStart]) {
        setTotalReachableNode(cycleStart, cycleLength);
        isCycle[cycleStart] = true;
        return;
      }

      int node = cycleStart;
      do {
        setTotalReachableNode(node, cycleLength);
        isCycle[node] = true;

        node = destinations[node];
      } while (node != cycleStart);
    }

    void fillPrefixLength(int startNode, int cycleStart, int maximumReachable) {
      int node = startNode;
      while (node != cycleStart) {
        setTotalReachableNode(node, maximumReachable--);
        node = destinations[node];
      }
    }

    void setTotalReachableNode(int node, int totalReachable) {
      totalReachableNode[node] = totalReachable;
      if (maxNode == -1) {
        maxNode = node;
      }
      else if (totalReachableNode[maxNode] < totalReachableNode[node]) {
        maxNode = node;
      }
      else if (totalReachableNode[maxNode] == totalReachableNode[node] && maxNode > node) {
        maxNode = node;
      }
    }

};


int main() {
  ifstream cin("../src/test/resources/com/kowsercse/uva/V124/12442-7.in");
  ofstream cout("../src/test/resources/com/kowsercse/uva/V124/12442-7.out");

  int testCase;
  cin >> testCase;
  for(int t = 1; t <= testCase; t++) {
    int totalNode;
    cin >> totalNode;
    Graph graph(totalNode);
    
    int source, destination;
    for (int i = 0; i < totalNode; i++) {
        cin >> source >> destination;
        graph.destinations[source -1] = destination - 1;
    }

    graph.solve();
    cout << "Case " << t << ": " << graph.maxNode + 1 << endl;
  }

  return 0;
}
