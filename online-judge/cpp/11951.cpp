#include <iostream>
#include <climits>

using namespace std;

int main() {
  freopen("../src/test/resources/com/kowsercse/uva/V119/11951.in", "r", stdin);
  freopen("../src/test/resources/com/kowsercse/uva/V119/11951.out", "w", stdout);

  int testCase;
  cin >> testCase;

  for (int i = 1; i <= testCase; i++) {
    int R, C, maxBudget;
    cin >> R >> C >> maxBudget;

    long input[R][C];
    for (int r = 0; r < R; r++) {
      for (int c = 0; c < C; c++) {
        cin >> input[r][c];
      }
    }
    long prefixSum[R][C];
    for (int c = 0; c < C; ++c) {
      prefixSum[0][c] = input[0][c];
    }

    for (int r = 1; r < R; r++) {
      for (int c = 0; c < C; c++) {
        prefixSum[r][c] = prefixSum[r - 1][c] + input[r][c];
      }
    }

    long globalMinCost = INT_MAX;
    long globalMaxPlot = 0;
    for (int rowStart = 0; rowStart < R; rowStart++) {
      for (int rwoEnd = rowStart; rwoEnd < R; rwoEnd++) {
        for (int columnStart = 0; columnStart < C; columnStart++) {
          long localTotalCost = 0;

          for (int columnEnd = columnStart; columnEnd < C; columnEnd++) {
            localTotalCost +=
                prefixSum[rwoEnd][columnEnd] - prefixSum[rowStart][columnEnd] + input[rowStart][columnEnd];
            if (localTotalCost > maxBudget) {
              break;
            }

            long localTotalPlot = (rwoEnd - rowStart + 1) * (columnEnd - columnStart + 1);
            if (globalMaxPlot < localTotalPlot) {
              globalMaxPlot = localTotalPlot;
              globalMinCost = localTotalCost;
            } else if (globalMaxPlot == localTotalPlot && localTotalCost < globalMinCost) {
              globalMinCost = localTotalCost;
            }
          }
        }
      }
    }

    if (globalMaxPlot == 0) {
      globalMinCost = 0;
    }
    cout << "Case #" << i << ": " << globalMaxPlot << " " << globalMinCost << endl;
  }
}
