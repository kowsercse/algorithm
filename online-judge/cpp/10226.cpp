#include <bits/stdc++.h>
#include <iostream>
#include <set>

using namespace std;

int main() {
  ifstream cin("../src/test/resources/com/kowsercse/uva/V102/10226-1.in");
  ofstream cout("../src/test/resources/com/kowsercse/uva/V102/10226-1.out");
  //ofstream cout("output.txt");

  string dummy;

  int testCase;
  cin >> testCase;
  getline(cin, dummy);
  getline(cin, dummy);

  while(testCase-- > 0) {
    map<string, int> countByTree;
    int totalTree = 0;

    string tree;
    getline(cin, tree);
    while(!tree.empty()) {
      countByTree[tree]++;
      totalTree++;
      //cout << totalTree << " " << tree << "" << tree.empty() << endl;

      getline(cin, tree);
    }

    for(map<string, int>::iterator i = countByTree.begin(); i != countByTree.end(); i++) {
      double percent = 100.0 * i->second / totalTree;
      cout << i->first << " " << fixed << setprecision(4) << percent << endl;
    }

    if(testCase != 0) {
      cout << endl;
    }

  }

  return 0;
}
