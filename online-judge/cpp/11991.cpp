#include <iostream>
#include <vector>
#include <map>

using namespace std;

int main() {
  freopen("../src/test/resources/com/kowsercse/uva/V119/11991.in", "r", stdin);
  freopen("../src/test/resources/com/kowsercse/uva/V119/11991.out", "w", stdout);

  int totalNumber, testCase;

  while (cin >> totalNumber >> testCase) {
    map<int, vector<int>> occurrenceIndexByNumber;
    int index = 0;
    int number, occurrence;
    while (totalNumber-- > 0) {
      cin >> number;
      vector<int> &occurrenceIndex = occurrenceIndexByNumber[number];
      occurrenceIndex.push_back(++index);
    }

    while (testCase-- > 0) {
      cin >> occurrence >> number;
      vector<int> &occurrenceIndex = occurrenceIndexByNumber[number];

      if (occurrence <= occurrenceIndex.size()) {
        cout << occurrenceIndex[occurrence - 1] << endl;
      } else {
        cout << 0 << endl;
      }
    }
  }

}
