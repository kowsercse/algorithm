#include <iostream>
#include <vector>

using namespace std;

void dfs(vector<int> &numbers, vector<int> &previousIndex, int currentIndex) {
  if (currentIndex == -1) {
    return;
  }

  dfs(numbers, previousIndex, previousIndex[currentIndex]);
  cout << numbers[currentIndex] << endl;
}


int getCeilIndex(int needle, vector<int> &values, vector<int> &indexes, int low, int high) {
  if (low == high) {
    return low;
  }

  int mid = (low + high) / 2;
  int middleValue = values[indexes[mid]];
  if (middleValue == needle) {
    return mid;
  } else if (middleValue > needle) {
    return getCeilIndex(needle, values, indexes, low, mid);
  } else {
    return getCeilIndex(needle, values, indexes, mid + 1, high);
  }
}

int main() {
  freopen("../src/test/resources/com/kowsercse/uva/V4/481-2.in", "r", stdin);
  freopen("../src/test/resources/com/kowsercse/uva/V4/481-2.out", "w", stdout);

  vector<int> numbers;
  vector<int> minNumberIndexByLis;
  vector<int> previousIndex;

  int length = 0;
  int index = 1, number;

  cin >> number;

  numbers.push_back(number);
  minNumberIndexByLis.push_back(0);
  previousIndex.push_back(-1);

  while (cin >> number) {
    numbers.push_back(number);

    if (number > numbers[minNumberIndexByLis[length]]) {
      previousIndex.push_back(minNumberIndexByLis[length]);
      minNumberIndexByLis.push_back(index);
      length++;
    } else if (number <= numbers[minNumberIndexByLis[0]]) {
      previousIndex.push_back(-1);
      minNumberIndexByLis[0] = index;
    } else {
      int ceilIndex = getCeilIndex(number, numbers, minNumberIndexByLis, 0, minNumberIndexByLis.size() - 1);
      previousIndex.push_back(minNumberIndexByLis[ceilIndex - 1]);
      minNumberIndexByLis[ceilIndex] = index;
    }
    index++;
  }

  cout << length + 1 << endl << '-' << endl;
  dfs(numbers, previousIndex, minNumberIndexByLis[length]);
}
