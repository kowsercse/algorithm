#include<bits/stdc++.h>
#include<iostream>

using namespace std;

int direction[][2] = {
    {-1, -1}, {-1, 0}, {-1, 1},
    {0,  -1},          {0,  1},
    {1,  -1}, {1,  0}, {1,  1},
};

int main() {
  int m, n, t = 0;

  while (true) {
    cin >> m >> n;

    if (m == 0 && n == 0) break;
    if (t > 0) cout << endl;

    char output[m][n + 1];
    memset(output, '0', sizeof output);

    for (int r = 0; r < m; ++r) {
      string line;
      cin >> line;

      output[r][n] = '\n';
      for (int c = 0; c < n; ++c) {
        if (line[c] != '*') continue;

        output[r][c] = '*';
        for (auto &dir : direction) {
          int R = r + dir[0];
          int C = c + dir[1];
          if (R >= 0 && C >= 0 && R < m && C < n && output[R][C] != '*') {
            output[R][C]++;
          }
        }
      }
    }
    output[m - 1][n] = 0;

    t++;
    cout << "Field #" << t << ":" << endl;
    cout << *output << endl;
  }
}
